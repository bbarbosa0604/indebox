<?php
function __autoload($class){ 
    require_once("class/".strtolower($class).".php");
}
function removeSimplesQuotes($val){
	return str_replace("'", "", $val);
}
function request($key){
	return removeSimplesQuotes($_REQUEST[$key]);
}
function post($key){
	return removeSimplesQuotes($_POST[$key]);
}
function get($key){
	return removeSimplesQuotes($_GET[$key]);
}

function in($t, $arrays = true, $keyEncode=''){
	if(is_array($t)){
		if($arrays){
			$b = array();
			foreach($t as $i){
				$n = array();
				foreach($i as $k=>$v){
					$n[chgName($k)] = $v;
				}
				$n['_'.chgName($keyEncode)] = in($i[$keyEncode]);
				array_push($b, $n);
			}
			return $b;
		}else{
			$n = array();
			foreach($t as $k=>$v){
				$n[chgName($k)] = $v;
			}
			$n['_'.chgName($keyEncode)] = in($t[$keyEncode]);
			return $n;
		}
	}else{
		$encode = base64_encode(time());
		return base64_encode(str_pad(strlen($encode), 3, '0', STR_PAD_LEFT).$encode.base64_encode($t));
	}
}
function out($t){
	$t = base64_decode($t);	
	$len = substr($t, 0, 3);
	return requestFIT(base64_decode(substr($t, ($len+3), strlen($t)-($len+3))));
}
function pre(){
	echo "<pre>";
	foreach(func_get_args() as $arg){
		print_r($arg);
	}
	echo "</pre>";
}
function formatdatetime($data, $tipo = 0)
{
	
	if(!is_numeric(left($data,1))){
		
		$data = replace($data,'Jan','1');
		$data = replace($data,'Feb','2');
		$data = replace($data,'Mar','3');
		$data = replace($data,'Apr','4');
		$data = replace($data,'May','5');
		$data = replace($data,'Jun','6');
		$data = replace($data,'Jul','7');
		$data = replace($data,'Aug','8');
		$data = replace($data,'Sep','9');
		$data = replace($data,'Oct','10');
		$data = replace($data,'Nov','11');
		$data = replace($data,'Dec','12');
		
		$ampm = right($data,2);
		
		$data = str_replace('  ',' ',left($data,strlen($data)-2));
		$data = explode(' ',$data);
		$hora = explode(':',$data[3]);
		
		if($ampm=='PM'){
			$hora[0] += 12;
		}
		
		if($hora[2]==''){
			$hora[2] = '00';
		}
		
		if($hora[1]==''){
			$hora[1] = '00';
		}
		
		$data = $data[2].'-'.$data[0].'-'.$data[1].' '.$hora[0].':'.$hora[1].':'.$hora[2];
		
	}else{
	
		$data = replace($data,' jan ','/01/');
		$data = replace($data,' fev ','/02/');
		$data = replace($data,' mar ','/03/');
		$data = replace($data,' abr ','/04/');
		$data = replace($data,' mai ','/05/');
		$data = replace($data,' jun ','/06/');
		$data = replace($data,' jul ','/07/');
		$data = replace($data,' ago ','/08/');
		$data = replace($data,' set ','/09/');
		$data = replace($data,' out ','/10/');
		$data = replace($data,' nov ','/11/');
		$data = replace($data,' dez ','/12/');
		
		$data = replace($data,' 1:',' 01:');
		$data = replace($data,' 2:',' 02:');
		$data = replace($data,' 3:',' 03:');
		$data = replace($data,' 4:',' 04:');
		$data = replace($data,' 5:',' 05:');
		$data = replace($data,' 6:',' 06:');
		$data = replace($data,' 7:',' 07:');
		$data = replace($data,' 8:',' 08:');
		$data = replace($data,' 9:',' 09:');
	
	}
	
	$array_data = explode(" ", $data);
	
	if($array_data[1] == '')
	{
		$hora_min_sec = trim(substr($data,strlen($array_data[0]),strlen($data)-strlen($array_data[0])));
	}
	else
	{
		$hora_min_sec = $array_data[1];	
	}
	
	if(instr($array_data[0],'/') > 0)
	{
		$array_dma = explode("/", $array_data[0]);
	}
	elseif(instr($array_data[0],'-') > 0)
	{
		$array_dma = explode("-", $array_data[0]);
		
		$new_data = $array_dma[2].'/'.$array_dma[1].'/'.$array_dma[0].' '.$hora_min_sec;
		
		return formatdatetime($new_data,$tipo);
	}
	
	$array_time = explode(":", $hora_min_sec);
	
	switch ($tipo)
	{
		case 0:
			$data_return = $array_dma[0] . "/" . $array_dma[1] . "/" . $array_dma[2] . " " . $hora_min_sec;
			break;
		case 1:
			$data_return = $array_dma[0] . " de " . monthname($array_dma[1]) . " de " . $array_dma[2] . utf8_encode(" às ") . $hora_min_sec;
			break;
		case 2:
			$data_return = right('0'.$array_dma[0],2) . "/" . right('0'.$array_dma[1],2) . "/" . $array_dma[2];
			break;
		case 3:
			$data_return = $hora_min_sec;
			break;
		case 4:
			$data_return = right('0'.$array_time[0],2) . ":" . right('0'.$array_time[1],2);
			break;
		case 5:
			$data_return = right('0'.$array_dma[0],2) . "/" . right('0'.$array_dma[1],2) . "/" . $array_dma[2];
			break;
		case 6:
			$data_return = datasql(right('0'.$array_dma[0],2) . "/" . right('0'.$array_dma[1],2) . "/" . $array_dma[2]);
			break;
		case 7:
			$data_return = right('0'.$array_dma[1],2) . "/" . right('0'.$array_dma[0],2) . "/" . $array_dma[2] ." ". $hora_min_sec;
			break;
		case 8:
			$data_return = datasql(right('0'.$array_dma[0],2) . "/" . right('0'.$array_dma[1],2) . "/" . $array_dma[2]).' '.$hora_min_sec;
			break;
		case 9:
			$data_return = right('0'.$array_dma[1],2) . "/" . right('0'.$array_dma[0],2) . "/" . $array_dma[2];
			break;
		case 10:
			$data_return = $array_dma[0] . " de " . monthname($array_dma[1]) . " de " . $array_dma[2];
			break;
		case 11:
			$data_return = $array_dma[0];
			break;
		default:
			$data_return = $array_dma[0] . "/" . $array_dma[1] . "/" . $array_dma[2] . " " . $hora_min_sec;
			break;
	}
	
	if(right(trim($data_return),5) == ' 0:00'){$data_return = replace($data_return," 0:00","");}
	
	if(trim($data_return) == '0/0/' || $data_return == '//' || $data_return == '-0-0' || trim($data_return) == '0-/0/-:00:' || trim($data_return) == '-:00:-0-0- :00:00')
	{
		return '';	
	}
	else
	{	
		return replace(replace($data_return,'//',''),'er/Em/-:00:','Em Aberto');
	}
	
}
//////////////////////////////////////////////////////////////////////////////////////////////
function left($string,$count)
{
	$string = substr($string,0,$count);
	return $string;
}
//////////////////////////////////////////////////////////////////////////////////////////////
function right($string,$count)
{
	$string = substr($string, -$count, $count);
	return $string;
}
//////////////////////////////////////////////////////////////////////////////////////////////
function replace($string,$isso,$porisso)
{
		$string = str_replace($isso, $porisso, $string);
		return $string;
}
//////////////////////////////////////////////////////////////////////////////////////////////
function instr($String,$Find,$CaseSensitive = false)
{
	$i=0;
	while (strlen($String)>=$i)
	{
		unset($substring);
		if ($CaseSensitive)
		{
			$Find=strtolower($Find);
			$String=strtolower($String);
		}
		$substring=substr($String,$i,strlen($Find));
		if ($substring==$Find) return true;
		$i++;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////////////////////////
function datasql($data) {
	
	$array_data = explode(" ", $data);
	$array_dma = explode("/", $array_data[0]);
	
	return $array_dma[2] . "-" . $array_dma[1] . "-" . $array_dma[0];// . " " . $array_data[1];
	
}
//////////////////////////////////////////////////////////////////////////////////////////////
function monthname($x)
{
	
	switch ($x)
	{
		case 1:
			return "Janeiro";
			break;
		case 2:
			return "Fevereiro";
			break;
		case 3:
			return "Marco";
			break;
		case 4:
			return "Abril";
			break;
		case 5:
			return "Maio";
			break;
		case 6:
			return "Junho";
			break;
		case 7:
			return "Julho";
			break;
		case 8:
			return "Agosto";
			break;
		case 9:
			return "Setembro";
			break;
		case 10:
			return "Outubro";
			break;
		case 11:
			return "Novembro";
			break;
		case 12:
			return "Dezembro";
			break;
		
	}
	
}
//////////////////////////////////////////////////////////////////////////////////////////////
function replaceAccent($text){
	return str_replace( array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $text); 
}
//////////////////////////////////////////////////////////////////////////////////////////////
function isExistsFolder($folder){

    if(file_exists($folder)){
        return true;
    } else {
        return false;
    }
}
function isExistsFile($folder,$name){
    if(file_exists($folder.$name)){
        return true;
    } else {
        return false;
    }
}

function createFolder($folder){
    //
    mkdir($folder);
    //
    return true;
}
function createFile($folder,$name){
    //
    $file = fopen($folder.'/'.$name, "a");
    fclose($file);
    //
    return true;
}
function success($status,$msg = ''){
    return json_encode(array("success"=>$status, "msg"=>$msg));
}
?>
<?php 
class Page { 
    
    public $options = array( 
        "data"=>array( 
            "title"=>"Sistema Indebox",
            "meta_description"=>"", 
            "meta_author"=>"SystemCode.com.br"
        ) 
    ); 
  
    public function __construct($options = array()){ 
  
        if( isset($options["data"]) ){
            $opt = array_merge($options["data"], $this->options["data"]);
            $options = array(
                "data"=>$opt
            );
        }


        $tpl = $this->getTpl(); 
        
        array_push($this->options, $options);
        
        //$this->options = $options; 
        
        if(gettype($this->options['data'])=='array'){ 
            foreach($this->options['data'] as $key=>$val){ 
                if($key!="js") $tpl->assign($key, $val); 
            } 
        } 
  
        if( isset($this->options['reflectJS']) === true ){ 
            array_push($this->options["js"], array( 
                "src"=>"res/js/".$this->getReflectPath().'.js'
                ) 
            ); 
        } 
  
        $tpl->draw("header", false); 
  
        if( isset($this->options['reflectTPL']) === true ){ 
  
            $this->setTpl($this->getReflectPath(), $this->options['data']); 
  
        } 
  
    } 
  
    public function __destruct(){ 
  
        $tpl = $this->getTpl(); 
  
        if( isset($this->options["js"]) == true && count($this->options["js"])){ 
            $tpl->assign("js", $this->options["js"]); 
        } 
          
        if(gettype($this->options['data'])=='array'){ 
            foreach($this->options['data'] as $key=>$val){ 
                $tpl->assign($key, $val); 
            } 
        } 
  
        $tpl->draw("footer", false); 
  
    } 
  
    private function getReflectPath(){ 
  
        return "reflect/".str_replace(".php", "", str_replace(PATH, "", $_SERVER["SCRIPT_FILENAME"])); 
  
    } 
      
    public static function setTpl($name, $data = array(), $returnHTML = false){ 
  
        $tpl = new RainTPL; 
  
        foreach ($data as $key => $val) { 
              
            $tpl->assign($key, $val); 
  
        } 
  
        if($returnHTML){ 
            return $tpl->draw($name, $returnHTML); 
        }else{ 
            $tpl->draw($name, $returnHTML); 
        }        
  
    } 
  
    public function getTpl(){ 
        return ( isset($this->Tpl) )?$this->Tpl:$this->Tpl = new RainTPL();   
    } 
  
} 
?>
<?php

class Usuario {

    public $desusuario;
    public $desemail;

    public function __construct($data = array()){
        if(gettype($data) == "numeric" || gettype($data) == "string"){            
            $this->idusuario = $data;
            $this->load();
        } else if(gettype($data) == "array") {
            
            if($this->hasLogin($data["desusuario"],$data["dessenha"])){
                $this->load();
            }
        } else {
            return false;
        }
    }
    public function load(){
        $sql = new Sql();
        $a = $sql->arrays("select * from tb_usuario where idusuario = ".$this->idusuario,false);
        //
        $this->desusuario = $a["desusuario"];
        $this->dessenha = $a["dessenha"];
    }

    public function getUsuarios(){
        $sql = new Sql();
        return $sql->arrays("select idusuario, desusuario, desemail, dessenha from tb_usuario where instatus = 1 order by desusuario");
    }

    public function hasUsuario(){
        $sql = new Sql();
        $a = $sql->arrays("select * from tb_usuario where idusuario = ".$this->idusuario);
        //
        if(count($a)>0){
            return true;
        } else {
            return false;
        }
    }

    public function hasLogin($desusuario, $dessenha = null){
        $sql = new Sql();
        if($dessenha){
            $a = $sql->arrays("select * from tb_usuario where desusuario = '".$desusuario."' and dessenha = '$dessenha'",true);
        } else {
            $a = $sql->arrays("select * from tb_usuario where desusuario = '".$desusuario."'",true);
        }
        //
        if(count($a)>0){
            $this->idusuario = $a['idusuario'];
            return true;
        } else {
            return false;
        }
    }

    public function editUsuario($desemail){
        $sql = new Sql();
        $sql->query("update tb_usuario set desemail = '$desemail' where idusuario =".$this->idusuario);
        //
        return true;
    }

    public function addUsuario($desusuario,$desemail,$dessenha){
        if(!$this->hasLogin($desusuario)){
            $sql = new Sql();
            $sql->query("insert tb_usuario(desusuario, desemail, dessenha, dtcadastro) values('$desusuario','$desemail','$dessenha',now())");
            return true;
        } else{
            return false;
        }
        //
    }

    public function removeUsuario(){
        $sql = new Sql();
        $sql->query("update tb_usuario set instatus = 0 where idusuario = ".$this->idusuario);
        //
        return true;
    }

    public function editPassword($dessenha){
        $sql = new Sql();
        $sql->query("update tb_usuario set dessenha = '$dessenha' where idusuario = ".$this->idusuario);
        //
        return true;
    }

} 
<?php
/**
 * Date: 20/04/14
 * Time: 21:44
*/
class Grupo {

    private $sql;

    public function __construct($data = array()){
        if(gettype($data) == "numeric" || gettype($data) == "string"){
            $this->idgrupo = $data;
            $this->load();
        } else {
            return false;
        }
    }

    public function load(){
        $sql = new Sql();
        $a = $sql->arrays("select * from tb_grupo where idgrupo = ".$this->idgrupo,false);
        //
        $this->desgrupo = $a["desgrupo"];
    }

    public function getGrupo(){
        $sql = new Sql();
        return $sql->arrays("select idgrupo, desgrupo from tb_grupo where instatus = 1 order by desgrupo");
    }

    public function addGrupo($desgrupo){
        $sql = new Sql();
        $sql->query("insert tb_grupo (desgrupo,dtcadastro) values('$desgrupo',now())");
        //
        return true;
    }

    public function removeGrupo(){
        $sql = new Sql();
        $sql->query("update tb_grupo set instatus = 0 where idgrupo = ".$this->idgrupo);
        //
        return true;
    }

    public function getGrupoUsuarios(){
        $sql = new Sql();
        return $sql->arrays("select b.idusuario, b.desusuario from tb_relacao_grupousuario a inner join tb_usuario b on a.idusuario = b.idusuario where a.idgrupo = ".$this->idgrupo);
    }

    public function hasGrupoUsuario($idusuario){
        $sql = new Sql();
        $a = $sql->arrays("select * from tb_relacao_grupousuario where idusuario = ".$idusuario." and idgrupo = ".$this->idgrupo);
        //
        if(count($a)>0){
            return true;
        } else {
            return false;
        }
    }

    public function hasGrupoMenu($idmenu){
        $sql = new Sql();
        $a = $sql->arrays("select * from tb_relacao_grupomenu where idmenu = ".$idmenu." and idgrupo = ".$this->idgrupo);
        //
        if(count($a)>0){
            return true;
        } else {
            return false;
        }
    }

    public function addGrupoUsuario($idusuario){
        if(!$this->hasGrupoUsuario($idusuario)){
            $sql = new Sql();
            $sql->query("insert tb_relacao_grupousuario (idusuario,idgrupo,dtcadastro) values($idusuario,".$this->idgrupo.",'".date("Y-m-d")."')");
        }
        //
        return true;
    }

    public function removeGrupoUsuario($idusuario){
        $sql = new Sql();
        $sql->query("delete from tb_relacao_grupousuario where idgrupo = ".$this->idgrupo." and idusuario = ".$idusuario);
        //
        return true;
    }

    public function addGrupoMenu($idmenu){
        if(!$this->hasGrupoMenu($idmenu)){
            $sql = new Sql();
            $sql->query("insert tb_relacao_grupomenu (idmenu,idgrupo,dtcadastro) values($idmenu,".$this->idgrupo.",'".date("Y-m-d")."')");
        }
        //
        return true;
    }

    public function removeGrupoMenu(){
        $sql = new Sql();
        $sql->query("delete from tb_relacao_grupomenu where idgrupo = ".$this->idgrupo);
        //
        return true;
    }

    public function getGrupoMenu(){
        $sql = new Sql();
        return $sql->arrays("select idmenu from tb_relacao_grupomenu where idgrupo = ".$this->idgrupo);
    }

}
?>
<?php

class Menu {

    public $sql;

    public function __construct($data = array()){
        if(gettype($data) == "numeric" || gettype($data) == "string"){
            $this->idmenu = $data;
            $this->load();
        } else {
            return false;
        }
    }

    public function load(){
        //
        $sql = new Sql();
        $a = $sql->arrays("select desicon, desmenu, desurl, idmenupai from tb_menu where idmenu = ".$this->idmenu,false);
        //
        $this->desmenu = $a["desmenu"];
        $this->desicon = $a["desicon"];
        $this->desurl = $a["desurl"];
        $this->idmenupai = $a["idmenupai"];
        //
        return true;

    }

    public function getMenu($parent = 0, $tree = false, $home = false, $idusuario = 0){
        //
        $sql = new Sql();
        if($idusuario==0){
            $a = $sql->arrays("select idmenu, desicon, desmenu, desurl from tb_menu where instatus = 1 and idmenupai = ".$parent." order by nrordem");
        } else {
            $a = $sql->arrays("select a.idmenu, desicon, desmenu, desurl from tb_menu a inner join tb_permissao b on a.idmenu = b.idmenu where a.instatus = 1 and idmenupai = ".$parent." and idusuario = $idusuario order by nrordem");
        }

        //
        $menu = array();
        //
        foreach($a as $a1){
            //
            if($home && (strpos($a1['desurl'],"function()")===0 || strpos($a1['desurl'],"openWindowDefault")===0)){
                $item = array(
                    "text"=>$a1['desmenu'],
                    "tooltip"=>self::toolTip($a1['idmenu'],$a1['desmenu']),
                    "iconCls"=>$a1['desicon'],
                    "id"=>"menu-".$a1['idmenu'],
                    "handler"=>"%".$a1['desurl'].'%'
                );
            } else {
                $item = array(
                    "text"=>$a1['desmenu'],
                    "tooltip"=>self::toolTip($a1['idmenu'],$a1['desmenu']),
                    "iconCls"=>$a1['desicon'],
                    "id"=>"menu-".$a1['idmenu'],
                );
            }

            if($this->hasChild($a1["idmenu"])){
                if($tree){
                    $item['children'] = $this->getMenu($a1["idmenu"], $tree, $home, $idusuario);
                }else{
                    $item['menu'] = $this->getMenu($a1["idmenu"], $tree, $home, $idusuario);
                }
            }else{
                if($tree) $item['leaf'] = true;
            }

            array_push($menu, $item);

        }

        return $menu;

    }

    public function hasChild($idmenu){
        $sql = new Sql();
        $result = $sql->arrays("select * from tb_menu where idmenupai = ".$idmenu);
        if(count($result)){
            return true;
        }else{
            return false;
        }
    }

    public function hasMenu($idmenu){
        $sql = new Sql();
        $result = $sql->arrays("select * from tb_menu where idmenu = ".$idmenu);
        if(count($result)){
            return true;
        }else{
            return false;
        }
    }

    public function addMenu($desmenu, $desicon, $desurl, $idmenupai){
        $nrordem = $this->getOrdem($idmenupai);
        //
        $sql = new Sql();
        $sql->query("insert into tb_menu(desmenu, desicon, desurl, idmenupai, nrordem, dtcadastro) values('$desmenu','$desicon','$desurl',$idmenupai,$nrordem,now())");
        //
        return true;
    }

    public function editMenu($desmenu, $desicon, $desurl){
        $sql = new Sql();
        $sql->query("update tb_menu set desmenu = '$desmenu', desicon = '$desicon', desurl = '$desurl' where idmenu = ".$this->idmenu);
        //
        return true;
    }

    public function removeMenu(){
        $sql = new Sql();
        $sql->query("update tb_menu set instatus = 0 where idmenu = ".$this->idmenu);
        //
        return true;
    }

    public function editOrder($nrordem,$idmenupai){
        $sql = new Sql();
        $sql->query("update tb_menu set nrordem = $nrordem, idmenupai = $idmenupai where idmenu = ".$this->idmenu);
        //
        return true;
    }

    public function getOrdem($idmenupai){
        $sql = new Sql();
        $result = $sql->arrays("select * from tb_menu where idmenupai = $idmenupai order by idmenu desc limit 1");
        if(count($result)>0){
            return (int)$result['nrordem']+1;
        } else {
            return 0;
        }
    }

    public function getMenuGrupoCheck($parent = 0, $idgrupo){
        //
        $grupo = new Grupo($idgrupo);
        //
        $sql = new Sql();
        $a = $sql->arrays("select idmenu, desicon, desmenu, desurl from tb_menu where instatus = 1 and idmenupai = ".$parent." order by nrordem");
        //
        $menu = array();
        //
        foreach($a as $a1){
            $item = array(
                "text"=>$a1['desmenu'],
                "iconCls"=>$a1['desicon'],
                "id"=>"menu-".$a1['idmenu'],
                "checked"=>($grupo->hasGrupoMenu($a1["idmenu"]))?true:false
            );
            //
            if($this->hasChild($a1["idmenu"])){
                $item['children'] = $this->getMenuGrupoCheck($a1["idmenu"], $idgrupo);
            }else{
                $item['leaf'] = true;
            }
            array_push($menu, $item);
        }
        return $menu;
    }

    public function getMenuUsuarioCheck($parent = 0, $idusuario){
        //
        $permissao = new Permissao();
        //
        $sql = new Sql();
        $a = $sql->arrays("select idmenu, desicon, desmenu, desurl from tb_menu where instatus = 1 and idmenupai = ".$parent." order by nrordem");
        //
        $menu = array();
        //
        foreach($a as $a1){
            $item = array(
                "text"=>$a1['desmenu'],
                "iconCls"=>$a1['desicon'],
                "id"=>"menu-".$a1['idmenu'],
                "checked"=>($permissao->hasPermissao($a1["idmenu"],$idusuario))?true:false
            );
            //
            if($this->hasChild($a1["idmenu"])){
                $item['children'] = $this->getMenuUsuarioCheck($a1["idmenu"], $idusuario);
            }else{
                $item['leaf'] = true;
            }
            array_push($menu, $item);
        }
        return $menu;
    }

    public function toolTip($idmenu, $text){
        if(strlen($text) > 0){
            return $text;
        } else {
            if($idmenu==36){
                return "Home";
            } else if($idmenu == 25){
                return "Atualizar Permissões";
            } else if($idmenu == 35){
                return "Alterar senha";
            }
        }
    }
} 
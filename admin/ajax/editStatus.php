<?php
require_once("../inc/configuration.php");

try{
    if(post("idlocacao")){
        $locacao = new Locacao(array(
            "idlocacao"=>post("idlocacao"),
            "idstatus"=>post("idstatus")
        ));
        //
        $locacao->editStatus();
        //
        $imovel = new Imovel(array(
            "idimovel"=>post("idimovel"),
            "idstatus"=>1
        ));
        $imovel->editStatus();
    }
    //
    echo success(true);
} catch (Exception $e){
    echo success(false,$e->getMessage());
}
?>
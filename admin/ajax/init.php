<?php
/* AUTHOR: BRUNO BARBOSA */
$GLOBALS["login"]=true;
require_once("../inc/configuration.php");

function createMenu($folder, $name){
    if(!isExistsFolder($folder)) createFolder($folder);
    if(!isExistsFile($folder,$name)) createFile($folder,$name);
    //Carregar permissoes
    $menu = new Menu();
    //
    //Escrevendo no arquivo o menu do usuario
    unlink($folder."/".$name);
    //
    $file = fopen($folder."/".$name, "a");
    $writeFile = fwrite($file, json_encode($menu->getMenu(0,false,true,$_SESSION['idusuario'])));
    fclose($file);
    //
    unset($writeFile,$arrayMenu,$file);
}

//Criando menu
createMenu(PATH."inc/users/".$_SESSION["idusuario"]."/","menu.js");

//Redirect menu
//header("Pragma: no-cache");
//header("Cache: no-cache");
//header("Cache-Control: no-cache, must-revalidate");
//header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("location: ".URL."default.php");

//
?>
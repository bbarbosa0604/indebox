<?php
/* AUTHOR: BRUNO BARBOSA */
require_once("../inc/configuration.php");

if(post("idusuario")==1){
     echo success(false,"Nao é possivel remover o usuario administrador");
} else {
    $usuario = new Usuario(post("idusuario"));

    $usuario->removeUsuario();

    echo success(true);
}

?> 
<?php
/* AUTHOR: BRUNO BARBOSA */
require_once("../inc/configuration.php");

$idmenu = post("idmenu");
$idusuario = post("idusuario");
$inacao = post("inacao");

$permissao = new Permissao();

if($inacao=="true"){
    $permissao->addPermissao($idmenu, $idusuario);
} else {
    $permissao->removePermissao($idmenu, $idusuario);
}

echo success(true);
?> 
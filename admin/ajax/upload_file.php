<?php 
require_once("../inc/configuration.php");
// Pasta onde o arquivo vai ser salvo
//$_UP['pasta'] = $_SERVER["DOCUMENT_ROOT"]."\\admin\\res\\img\\galeria\\";
$_UP['pasta'] = "../../res/img/galeria/";

$idpagina = (post('idpagina'))? post('idpagina'): 0;
$idcategoria = (post('idcategoria'))? post('idcategoria'): 0;
$descategoria = (post('descategoria'))? utf8_encode(post('descategoria')) : 0;

$tipoUpload = post('tipoupload');

$sql = new Sql();

function retira_acentos($texto) { 	
	$array1 = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç" , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", "\\", "/"); 
	$array2 = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "", "" ); 
	
	return str_replace( $array1, $array2, $texto); 
} 

// Tamanho máximo do arquivo (em Bytes)
$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
// Array com as extensões permitidas
$_UP['extensoes'] = array('jpg', 'png', 'jpeg');
// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP['renomeia'] = false;
// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'Não houve erro';
$_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'Não foi feito o upload do arquivo';

$i=0;

foreach ($_FILES['arquivo']['name'] as $key => $arquivoname) {
	$i++;
	// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
	if ($_FILES['arquivo']['error'][$key] != 0) {
		die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
		exit; // Para a execução do script
	}

	$arquivoname = retira_acentos($arquivoname);
		
	// Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
	// Faz a verificação da extensão do arquivo
	$extensao = strtolower(end(explode('.', $arquivoname)));
	if (array_search($extensao, $_UP['extensoes']) === false) {
		echo success(false, "Por favor, envie arquivos com as seguintes extensões: jpg ou png");
	  	exit;
	}
	// Faz a verificação do tamanho do arquivo
	if ($_UP['tamanho'] < $_FILES['arquivo']['size'][$key]) {
		echo success(false, "O arquivo enviado é muito grande, envie arquivos de até 2Mb.");
	  	exit;
	}

	//se a imagem já existir, coloca um nome inexistente
	if ( file_exists($_UP['pasta'] . $arquivoname) ) {
		$_UP['renomeia'] = true;
	}

	// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
	// Primeiro verifica se deve trocar o nome do arquivo

	$nome_final = md5(time()).$i.'.'.$extensao;

	// if ($_UP['renomeia'] == true) {
	// 	// Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
	// 	$nome_final = md5(time()).'.'.$extensao;
	// } else {
	// 	// Mantém o nome original do arquivo
	// 	$nome_final = utf8_encode($arquivoname);
	// }
	  
	// Depois verifica se é possível mover o arquivo para a pasta escolhida
	if (move_uploaded_file($_FILES['arquivo']['tmp_name'][$key], $_UP['pasta'] . $nome_final)) {
		// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
		if ( $tipoUpload == 'banner' ) {
			$sql->arrays("call sp_banner_save(0, ".$idpagina.", '".str_replace("../../", "", $_UP['pasta']).$nome_final."', 1);");		
		}
		elseif ( $tipoUpload == 'categoria' ) {
			$idcategoria = $sql->arrays("call sp_categoria_save($idcategoria, '".$descategoria."', '".str_replace("../../", "", $_UP['pasta']).$nome_final."', 1);", true);
			$sql->arrays("call sp_foto_save(0, $idcategoria[idcategoria], '".str_replace("../../", "", $_UP['pasta']).$nome_final."', 1);");
		}
		elseif ( $tipoUpload == 'foto' ) {
			$sql->arrays("call sp_foto_save(0, $idcategoria, '".str_replace("../../", "", $_UP['pasta']).$nome_final."', 1);");		
		}

	} else {
		// Não foi possível fazer o upload, provavelmente a pasta está incorreta
		echo success(false, "Não foi possível enviar o arquivo $arquivoname, tente novamente");

		exit;
	}

}

echo success(true);

?>
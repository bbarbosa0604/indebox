<?php
/* AUTHOR: BRUNO BARBOSA */
require_once("../inc/configuration.php");

$idmenu = explode(",",post("idmenu"));
$grupo = new Grupo(post("idgrupo"));
//
$grupo->removeGrupoMenu();
//
for($i=0;$i<count($idmenu);$i++){
    //
    $grupo->addGrupoMenu($idmenu[$i]);
}
//
$permissao = new Permissao();
//
foreach($grupo->getGrupoUsuarios() as $val){
    for($i=0;$i<count($idmenu);$i++){
        //
        $permissao->addPermissao($idmenu[$i],$val["idusuario"]);
    }
}
echo success(true);

?> 
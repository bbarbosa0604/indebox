<?php
require_once("../inc/configuration.php");

try{
	$content = file_get_contents("../../res/tpl/".str_replace(" ","-",post("despagina")));

	$classe  = (post('classe'))? post('classe'): '.container-body';

	$pagina = phpQuery::newDocumentHTML($content);
	
	$conteudoSub = phpQuery::newDocumentHTML(post("desconteudo"));

	$container = pq('', $conteudoSub)->html();
	$pagina[$classe]->html($container);
	
	$desconteudo = $pagina->htmlOuter();

	if (isset($desconteudo) && $desconteudo !== '') {
		//abre o arquivo
	    $arquivo = fopen("../../res/tpl/".post("despagina"),"w+");
	    //escreve no arquivo
	    fwrite($arquivo, $desconteudo);
	    //fecha o arquivo
	    fclose($arquivo);
	}
    
    echo success(true);
} catch (Exception $e){
    echo success(false,$e->getMessage());
}
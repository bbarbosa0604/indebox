function getIdade(nascimento, hoje) {
    var diferencaAnos = hoje.getFullYear() - nascimento.getFullYear();
    if ( new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate()) <
        new Date(hoje.getFullYear(), nascimento.getMonth(), nascimento.getDate()) )
        diferencaAnos--;
    return diferencaAnos;
}
//
Ext.util.Format.comboRenderer = function(combo){
    return function(value){
        var record = combo.findRecord(combo.valueField, value);
        return record ? record.get(combo.displayField) : value;
    }
}
//
function filterStoreByAllFields (value, store, vetor){
    store.filterBy(function(rec){
        if (value != ""){
            var filtro = value.toLowerCase();
            var content = "";
            filtro = filtro.split(" ");
            if (vetor){
                $.each(vetor, function(index, v){
                    content += eval('rec.json.'+v)+' ';
                });
                content = content.toLowerCase();
            } else {
                $.each(store.fields.keys, function(index, v){
                    content += eval('rec.json.'+v)+' ';
                });
                content = content.toLowerCase();
            }
            var isValid = true;
            for (i = 0; i < filtro.length; i++){
                if (content.indexOf(filtro[i]) == -1){
                    isValid = false;
                }
            }
            if (isValid){
                return rec;
            }
        }
        else {
            return rec;
        }
    });
}
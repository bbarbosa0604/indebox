<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE HTML>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="res/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="res/css/fileuploadfield.css" />
    <link rel="stylesheet" type="text/css" href="res/css/icons.css" />
    <link rel="stylesheet" type="text/css" href="res/css/default.css" />
    <link rel="shortcut icon" href="res/img/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="res/js/ext-base.js"></script>
    <script type="text/javascript" src="res/js/ext-all.js"></script>
    <script type="text/javascript" src="res/js/ext-lang-pt_BR.js"></script>
    <!--<script type="text/javascript" src="res/js/fileupload.js"></script>-->
    <script type="text/javascript" src="res/js/FileUploadField.js"></script>
    <script type="text/javascript" src="res/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="res/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="res/js/configuration.js"></script>
    <script type="text/javascript" src="res/js/function.js"></script>
    <title><?php echo $title;?></title>
</head>
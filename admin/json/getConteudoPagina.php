<?php
require_once("../inc/configuration.php");

$content = file_get_contents("../../res/tpl/".str_replace(" ","-",get("despagina")).".html");

$classe  = (get('classe'))? get('classe'): '.container-body';

phpQuery::newDocument($content);
$container = pq($classe)->html();

echo json_encode($container);

?>
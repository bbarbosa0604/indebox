<?php
require_once("../inc/configuration.php");

$sql = new Sql();

$s = $sql->arrays("select idservico, desservico, desdescricao, desurl, desicon, instatus from tb_servicos;");

$servicos = array();

foreach ($s as $servico) {

	array_push($servicos, array(
		"idservico" => $servico['idservico'],
		"desservico" => utf8_encode($servico['desservico']),
		"desdescricao" => utf8_encode($servico['desdescricao']),
		"desurl" => utf8_encode($servico['desurl']),
		"desicon" => utf8_encode($servico['desicon']),
		"instatus" => $servico['instatus']
	));
}

echo json_encode(array("myData"=>$servicos));

?>
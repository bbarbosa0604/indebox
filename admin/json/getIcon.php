<?php
require_once("../inc/configuration.php");
//
$diretorio = dir("../res/img/icons/16/");
$arrIcons = array();
//
while($arquivo = $diretorio -> read()){
    if(strpos($arquivo,".png")){
        array_push($arrIcons,array(
            "name"=>"ico_".str_replace(".png","",$arquivo),
            "url"=>"res/img/icons/16/".$arquivo
        ));
    }
}
$diretorio -> close();

echo json_encode(array("myData"=>$arrIcons));

?>
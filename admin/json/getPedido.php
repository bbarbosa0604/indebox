<?php
require_once("../inc/configuration.php");

$sql = new Sql();

$query = $sql->arrays("select 
							pes.despessoa,
							cont3.descontato as email,
							cont1.descontato as telefone,
							cont2.descontato as celular,
							doc.desdocumento as cpf,
							doc2.desdocumento as rg,
							endco.desendereco,
							endco.desnumero,
							endco.descomplemento,
							endco.descep,
							endco.desbairro,
							endco.descidade,
							endco.desestado,
							ped.idpedido,
							ped.vlpedido,
							ped.dtcadastro,
							plan.desplano,
							(select max(dtcadastro) from tb_pagamentos where idpedido = ped.idpedido  order by idpagamento desc limit 1) as dtpagamento,
							(select max(idstatus) from tb_pagamentos where idpedido = ped.idpedido  order by idpagamento desc limit 1) as idstatus,
							(select desstatus from tb_pagamentos a1 inner join tb_status a2 on a1.idstatus = a2.idstatus where a1.idpedido = ped.idpedido  order by idpagamento desc limit 1) as desstatus,
							(select max(idpagamento) from tb_pagamentos where idpedido = ped.idpedido  order by idpagamento desc limit 1) as idpagamento,
							(select max(desformapagamento) from tb_pagamentos a1 inner join tb_formapagamento a2 on a1.idformapagamento = a2.idformapagamento where a1.idpedido = ped.idpedido  order by idpagamento desc limit 1) as desformapagamento
						from tb_pedidos ped
						inner join tb_pessoas pes on ped.idpessoa = pes.idpessoa
						left join tb_documentos doc on pes.idpessoa = doc.idpessoa and doc.iddocumentotipo = 1
						left join tb_documentos doc2 on pes.idpessoa = doc2.idpessoa and doc2.iddocumentotipo = 2
						left join tb_contatos cont1 on pes.idpessoa = cont1.idpessoa and cont1.idcontatotipo = 1
						left join tb_contatos cont2 on pes.idpessoa = cont2.idpessoa and cont2.idcontatotipo = 2
						left join tb_contatos cont3 on pes.idpessoa = cont3.idpessoa and cont3.idcontatotipo = 3
						left join tb_endereco endco on ped.idendereco = endco.idendereco
						left join tb_planos plan on ped.idplano = plan.idplano
						left join tb_pagamentos pag on ped.idpedido = pag.idpedido
						left join tb_status sta on pag.idstatus = sta.idstatus
						where ped.idpedido = ".post('idpedido')." group by pes.despessoa,
						cont3.descontato,
						cont1.descontato,
						cont2.descontato,
						doc.desdocumento,
						endco.desendereco,
						endco.desnumero,
						endco.descomplemento,
						endco.descep,
						endco.desbairro,
						endco.descidade,
						endco.desestado,
						ped.idpedido,
						ped.vlpedido,
						ped.dtcadastro,
						plan.desplano;",false);

$query_ambiente = $sql->arrays("select 
						a.desambiente,
						a.desmetragem,
						a.desobservacao,
						b.desarquivo,
						a.idpedido
					from tb_pedidoambiente a
					left join tb_pedidoarquivos b on a.idpedidoambiente = b.idambiente
					where a.idpedido = ".post('idpedido').";");


echo json_encode(array(
	"success" => TRUE,
	"pedido" => $query,
	"ambiente" => $query_ambiente
));

?>
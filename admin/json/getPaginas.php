<?php
require_once("../inc/configuration.php");

$diretorio = dir("../../res/tpl/");
$data = array();

while($arquivo = $diretorio -> read()){
    if(strpos($arquivo,".html")){
        if(str_replace("-"," ",str_replace(".html","",$arquivo)) != "head" &&
        str_replace("-"," ",str_replace(".html","",$arquivo)) != "nav" &&
        str_replace("-"," ",str_replace(".html","",$arquivo)) != "model" &&
        str_replace("-"," ",str_replace(".html","",$arquivo)) != "footer" &&
        str_replace("-"," ",str_replace(".html","",$arquivo)) != "header"){
            array_push($data,array(
                "desarquivo"=>str_replace("-"," ",str_replace(".html","",$arquivo))
            ));
        }

    }
}
$diretorio -> close();

echo json_encode(array("myData"=>$data));

?>
<?php
require_once("../inc/configuration.php");

$de = post('de');
$ate = post('ate');
$num_pedido = (post('num_pedido')) ? post('num_pedido') : "NULL";
$cliente = (post('cliente')) ? "'".post('cliente')."'" : "NULL";
$where = '';

if($num_pedido != "NULL" || $cliente != "NULL"){

	$where = "where a.idpedido = $num_pedido or b.despessoa = $cliente";

}else{

	$where = "where a.dtcadastro between '$de' and '$ate'";
}

$sql = new Sql();

$a = $sql->arrays("select
	a.idpedido,
	b.despessoa,
	a.vlpedido,
	c.desplano,
	a.dtcadastro,
	ifnull((select max(idstatus) from tb_pagamentos where idpedido = a.idpedido order by idpagamento desc limit 1),1) as idstatus,
	ifnull((select desstatus from tb_pagamentos a1 inner join tb_status a2 on a1.idstatus = a2.idstatus where a1.idpedido = a.idpedido  order by idpagamento desc limit 1),'Orcamento') as desstatus,
	(select max(idpagamento) from tb_pagamentos where idpedido = a.idpedido order by idpagamento desc limit 1) as idpagamento,
    (select max(dtcadastro) from tb_pagamentos where idpedido = a.idpedido order by idpagamento desc limit 1) as dtpagamento
from tb_pedidos a
inner join tb_pessoas b on a.idpessoa = b.idpessoa
inner join tb_planos c on a.idplano = c.idplano
".$where." and a.instatus = 1
group by a.idpedido,
	b.despessoa,
	a.vlpedido,
	c.desplano,
	a.dtcadastro;");

$data = array();
foreach($a as $val){
	array_push($data,array(
		"idpedido"=>$val["idpedido"],
		"despessoa"=>ucfirst($val["despessoa"]),
		"vlpedido"=>$val["vlpedido"],
		"desplano"=>$val["desplano"],
		"dtcadastro"=>$val["dtcadastro"],
		"dtpagamento"=>$val["dtpagamento"],
		"idstatus"=>$val["idstatus"],
		"desstatus"=>$val["desstatus"],
		"idpagamento"=>$val["idpagamento"],
	));
}

echo json_encode(array("myData"=>$data));

?>
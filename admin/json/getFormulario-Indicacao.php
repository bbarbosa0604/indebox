<?php
require_once("../inc/configuration.php");

$dt1 = post("dt1");
$dt2 = post("dt2");

$sql = new Sql();

if($dt1 && $dt2){
    $a = $sql->arrays("select * from tb_indicacao where idstatus not in(0) and dtcadastro between '$dt1' and '$dt2'");
} else {
	$a = $sql->arrays("select * from tb_indicacao where idstatus not in(0)");
}

$data = array();
foreach ($a as $val) {
	array_push($data, array(
		"idindicacao"=>$val["idindicacao"],
		"desnome"=>ucfirst($val["desnome"]),
		"desemail"=>ucfirst($val["desemail"]),
		"desnomeamigo"=>ucfirst($val["desnomeamigo"]),
		"desemailamigo"=>ucfirst($val["desemailamigo"]),
		"desmensagem"=>ucfirst($val["desmensagem"]),
		"dtcadastro"=>$val["dtcadastro"],
		"idstatus"=>$val["idstatus"],
	));
}

 echo json_encode(array("myData"=>$data));

?>
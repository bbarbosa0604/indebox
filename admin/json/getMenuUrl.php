<?php
require_once("../inc/configuration.php");

$idmenu = get("idmenu");

if($idmenu){
    $menu = new Menu($idmenu);
    echo json_encode(array("success"=>true,"desurl"=>$menu->desurl));
} else {
    echo json_encode(array("success"=>false));
}

?> 
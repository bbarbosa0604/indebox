<?php
require_once("../inc/configuration.php");

$usuario = new Usuario(array(
    "desusuario"=>post("desusuario"),
    "dessenha"=>post("dessenha")
));

if($usuario->idusuario){
    $_SESSION["idusuario"] = (int)$usuario->idusuario;
    $_SESSION["desusuario"] = $usuario->desusuario;
    $_SESSION["desemail"] = $usuario->desemail;
    echo json_encode(array("success"=>true));
} else {
    echo json_encode(array("success"=>false));
}

?>
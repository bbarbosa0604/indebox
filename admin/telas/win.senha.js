var windowIDsenha = 'win.senha';
//
if (Ext.getCmp(windowIDsenha)) {
    Ext.getCmp(windowIDsenha).show();
} else {
    new Ext.Window({
        title: "Alterar Senha",
        width: 400,
        height: 130,
        modal: true,
        id: windowIDsenha,
        items: [{
            xtype:"form",
            padding:10,
            border:false,
            labelWidth:150,
            id:windowIDsenha+'-form',
            defaults:{
                anchor:"100%",
                allowBlank:false
            },
            items:[{
                fieldLabel:'Digite a nova Senha',
                xtype:"numberfield",
                inputType:"password",
                name:"dessenha",
                id:windowIDsenha+"-senha1",
                minValue:6,
                minText:"Senha precisa ter no mínimo 6 números"
            },{
                fieldLabel:"Confirme a nova Senha",
                xtype:"numberfield",
                inputType:"password",
                id:windowIDsenha+"-senha2",
                minValue:6,
                minText:"Senha precisa ter no minimo 6 numeros"
            }]
        }],
        buttons:[{
            text:"Salvar",
            iconCls:"ico_save",
            handler:function(){
                if(Ext.getCmp(windowIDsenha+'-form').getForm().isValid()){
                    if(Ext.getCmp(windowIDsenha+'-senha1').getValue()==Ext.getCmp(windowIDsenha+'-senha2').getValue()){
                        Ext.getCmp(windowIDsenha+'-form').getForm().submit({
                            url:"ajax/addSenha.php",
                            success:function(){
                                alertSuccess("Senha Alterada com sucesso!");
                                Ext.getCmp(windowIDsenha).close();
                            }
                        });
                    } else{
                        alertError("AS senhas digitadas estão diferentes");
                    }
                }
            }
        }]
    }).show();
}
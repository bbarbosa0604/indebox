var windowIDusuario = 'win.usuario';
//
if (Ext.getCmp(windowIDusuario)) {
    Ext.getCmp(windowIDusuario).show();
} else {
    myMask.show();
    //
    var storeUsuarios = new Ext.data.JsonStore({
        url:"json/getUsuarios.php",
        root:"myData",
        fields:['idusuario','desusuario','desemail','dessenha'],
        autoLoad:true,
        listeners:{
            load:function(){
                myMask.hide();
            }
        }
    });

    this.windowUsuario = function(idusuario, desusuario, desemail){
        new Ext.Window({
            title:'Gerenciar Usuario',
            width:300,
            id:windowIDusuario+"-windowusuario",
            height:150,
            modal:true,
            items:[{
                xtype:"form",
                padding:10,
                border:false,
                height:150,
                id:windowIDusuario+"-form-addusuario",
                defaults:{
                    anchor:"100%",
                    allowBlank:false
                },
                items:[{
                    fieldLabel:'Usuario',
                    xtype:'textfield',
                    name:"desusuario",
                    value:desusuario,
                    readOnly:(idusuario!=0)?true:false
                },{
                    fieldLabel:'Email',
                    xtype:'textfield',
                    name:"desemail",
                    value:desemail
                },{
                    fieldLabel:'Senha',
                    xtype:'textfield',
                    name:"dessenha",
                    allowBlank:true,
                    inputType:"password",
                    hidden:(idusuario!=0)?true:false
                }]
            }],
            buttons:[{
                iconCls:'ico_save',
                text:'Salvar',
                handler:function(){
                    if(Ext.getCmp(windowIDusuario+"-form-addusuario").getForm().isValid()){
                        Ext.getCmp(windowIDusuario+"-form-addusuario").getForm().submit({
                            url:'ajax/addUsuario.php',
                            params:{
                                idusuario:idusuario
                            },
                            success:function(a,b){
                                var json = $.parseJSON(b.response.responseText);
                                if(json.success){
                                    alertSuccess("Usuário Cadastrado/alterado com sucesso!");
                                }
                                //
                                Ext.getCmp(windowIDusuario+"-windowusuario").close();
                                storeUsuarios.reload();
                            },
                            failure:function(a,b){
                                var json = $.parseJSON(b.response.responseText);
                                alertError("Não foi possivel criar este usuario. Usuario já existe!");
                            }
                        });
                    }
                }
            }]
        }).show();
    }

    setTimeout(function(){
        new Ext.Window({
            title: "Gerir Usuarios",
            width: 400,
            height: 300,
            modal: true,
            id: windowIDusuario,
            layout:'form',
            items: [{
                xtype:"listview",
                store: storeUsuarios,
                autoScroll:true,
                border:false,
                loadMask:true,
                emptyText: 'Nenhum usuario cadastrado no sistema',
                reserveScrollOffset: true,
                columns: [{
                    header: 'Usuario',
                    width: .5,
                    dataIndex: 'desusuario'
                },{
                    header: 'Email',
                    width: .5,
                    dataIndex: 'desemail'
                }],
                listeners:{
                    contextmenu:function(obj,index,node,e){
                        e.stopEvent();
                        var usuario = obj.getStore().getAt(index);
                        //
                        new Ext.menu.Menu({
                            items:[{
                                text:'Alterar',
                                iconCls:"ico_pencil",
                                handler:function(){
                                    windowUsuario(usuario.get("idusuario"),usuario.get("desusuario"),usuario.get("desemail"))
                                }
                            },{
                                text:'Excluir',
                                iconCls:'ico_delete',
                                handler:function(){
                                    Ext.MessageBox.confirm("Aviso!","Deseja realmente excluir este usuario?",function(btn){
                                        if(btn=='yes'){
                                            Ext.Ajax.request({
                                                url:'ajax/removeUsuario.php',
                                                params:{
                                                    idusuario:usuario.get("idusuario")
                                                },
                                                success:function(a){
                                                    var json = $.parseJSON(a.responseText);
                                                    if(json.success){
                                                        storeUsuarios.reload();
                                                    } else {
                                                        alertError(json.msg);
                                                    }
                                                }
                                            })
                                        }
                                    });
                                }
                            }]
                        }).showAt(e.xy);
                    }
                }
            }],
            tbar:[{
                text:'Adicionar',
                iconCls:'ico_add',
                handler:function(){
                    windowUsuario(0,"","");
                }
            }]
        }).show();
    },100);
}
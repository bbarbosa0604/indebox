var windowIDAdmPaginas = "win.adm-paginas";

function removeAcento(strToReplace) {
    str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
    str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var nova = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
            nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
        } else {
            nova += strToReplace.substr(i, 1);
        }
    }
    return nova;
}

if(Ext.getCmp(windowIDAdmPaginas)){
    Ext.getCmp(windowIDAdmPaginas).show();
} else {

    /*myMask.show();*/

    var combo_categorias = new Ext.data.JsonStore({
        url: SITE_PATH + "admin/json/getCategorias.php",
        autoLoad:true,
        root:"categorias",
        fields:[
            {name: 'idcategoria', type: 'int'},
            {name: 'descategoria', type: 'string'}
        ]
    });

    /*var tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap" id="{name}">',
            '<div class="thumb"><img src="{url}" title="{name}"></div>',
            '<span class="x-editable">{shortName}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );   */ 
    
    function loadBanners(idpagina, idpanel) {
        $.ajax({
            type    : "POST",
            url     : SITE_PATH + "admin/json/getBanners.php",
            dataType: 'JSON',
            data    : {
                idpagina: idpagina
            },
            success:function(results, success){
                html_img = '';
                $.each(results.banners, function(index, rec){
                    
                    html_img += '<div id="banner-'+rec.idbanner+'" >';
                        html_img += '<span class="html">';
                            html_img += '<img src="'+SITE_PATH+rec.desurl+'">';
                            html_img += '<button data-idbanner="'+rec.idbanner+'" type="button" class="btn-close">x</button>';
                        html_img += '</span>';
                    html_img += '</div>';
                    
                });
                                       
                var panel = Ext.getCmp(windowIDAdmPaginas+idpanel);

                var tpl = new Ext.XTemplate('<div class="banners-conteiner">',html_img,'</div>');


                tpl.overwrite(panel.body, {foo: 'bar'});
                panel.body.highlight('#c3daf9', {block:true});

                $('button.btn-close').off().on('click', function() {
                    var idbanner = $( this ).attr('data-idbanner');

                    Ext.Msg.confirm('Atenção!', "Deseja remover este banner?", function(resposta){
                        if ( resposta == 'yes' ) {

                            $.ajax({
                                url: SITE_PATH + "admin/ajax/removeBanner.php",
                                type: 'POST',
                                data: {
                                    idbanner: idbanner
                                },
                                dataType: 'JSON',
                                success: function( response ) {
                                    if ( response.success ) {
                                        Ext.Msg.alert('Alerta!', response.msg, function(){
                                            $('#banner-'+idbanner).remove();
                                        });                                                
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });
    }

    var idcategoria = 0;

    function loadCategorias () {
        $.ajax({
            type    : "POST",
            url     : SITE_PATH + "admin/json/getCategorias.php",
            dataType: 'JSON',
            success:function(results, success){
                html_img = '';
                $.each(results.categorias, function(index, rec){
                    
                    html_img += '<div class="galeria" data-idcategoria="'+rec.idcategoria+'">';
                        html_img += '<div class="galeria-components">';
                            html_img += '<img src="'+SITE_PATH+rec.desurlcapa+'">';
                            html_img += '<div class="descricao">'+rec.descategoria+'</div>';
                            html_img += '<button data-idcategoria="'+rec.idcategoria+'" type="button" class="btn-edit">Editar</button>';
                            html_img += '<button data-idcategoria="'+rec.idcategoria+'" type="button" class="btn-close">x</button>';
                        html_img += '</div>';
                    html_img += '</div>';

                });
                                       
                var p = Ext.getCmp(windowIDAdmPaginas+'galeria-categorias');

                var tpl = new Ext.XTemplate('<div class="banners-conteiner">',html_img,'</div>');


                tpl.overwrite(p.body, {foo: 'bar'});
                p.body.highlight('#c3daf9', {block:true});

                $('.banners-conteiner').sortable({
                    placeholder: "ui-state-highlight",
                    helper: 'clone',
                    out: function() {
                        var categorias = '';
                        $.each($(this).find('.galeria'), function(index, value){
                            categorias += $(value).attr('data-idcategoria');
                            if (($('.banners-conteiner').find('.galeria').length - 1) != index) {
                                categorias += ',';
                            }
                        });
                        
                        $.ajax({
                            type    : "POST",
                            url     : SITE_PATH + "admin/ajax/ordenarCategorias.php",
                            dataType: 'JSON',
                            data: {
                                categorias: categorias
                            },
                            success:function(results, success){

                            }
                        });
                    }
                });

                $('.galeria').off().on('click', function() {
                    idcategoria = $( this ).attr('data-idcategoria');

                    $.ajax({
                        type    : "POST",
                        url     : SITE_PATH + "admin/json/getGaleriaFotos.php",
                        dataType: 'JSON',
                        data: {
                            galeria: idcategoria
                        },
                        success:function(results, success){
                            html_img = '';
                            $.each(results.fotos, function(index, rec){
                                
                                html_img += '<div class="foto" data-idfoto="'+rec.idfoto+'">';
                                    html_img += '<span class="html">';
                                        html_img += '<img src="'+SITE_PATH+rec.desurl+'">';
                                        html_img += '<button data-idfoto="'+rec.idfoto+'" type="button" class="btn-close">x</button>';
                                    html_img += '</span>';
                                html_img += '</div>';

                            });

                            var p = Ext.getCmp(windowIDAdmPaginas+'galeria-fotos');

                            p.setTitle('Fotos - ' + $('div[data-idcategoria='+idcategoria+'] .descricao').html());
                            var tpl = new Ext.XTemplate('<div class="categorias-conteiner">',html_img,'</div>');


                            tpl.overwrite(p.body, {foo: 'bar'});
                            p.body.highlight('#c3daf9', {block:true});

                            $('.foto button.btn-close').off().on('click', function() {
                                var idfoto = $( this ).attr('data-idfoto');
                                
                                Ext.Msg.confirm('Atenção!', "Deseja remover esta foto da galeria?", function(resposta){
                                    if ( resposta == 'yes' ) {

                                        $.ajax({
                                            url: SITE_PATH + "admin/ajax/removeFoto.php",
                                            type: 'POST',
                                            data: {
                                                idfoto: idfoto
                                            },
                                            dataType: 'JSON',
                                            success: function( response ) {
                                                if ( response.success ) {
                                                    Ext.Msg.alert('Alerta!', response.msg, function(){
                                                        $('div[data-idfoto='+idfoto+']').remove();
                                                    });                                                
                                                }
                                            }
                                        });
                                    }
                                });
                            });

                            $('.categorias-conteiner').sortable({
                                placeholder: "ui-state-highlight",
                                helper: 'clone',
                                out: function() {
                                    var fotos = '';
                                    $.each($(this).find('.foto'), function(index, value){
                                        fotos += $(value).attr('data-idfoto');
                                        if (($('.categorias-conteiner').find('.foto').length - 1) != index) {
                                            fotos += ',';
                                        }
                                    });

                                    
                                    $.ajax({
                                        type    : "POST",
                                        url     : SITE_PATH + "admin/ajax/ordenarFotos.php",
                                        dataType: 'JSON',
                                        data: {
                                            idcategoria: idcategoria,
                                            fotos: fotos
                                        },
                                        success:function(results, success){

                                        }
                                    });
                                }
                            });

                        }
                    });
                });

                $('.galeria-components button.btn-close').off().on('click', function() {
                    var idcategoria = $( this ).attr('data-idcategoria');
                    
                    Ext.Msg.confirm('Atenção!', "Deseja remover esta categoria?", function(resposta){
                        if ( resposta == 'yes' ) {

                            $.ajax({
                                url: SITE_PATH + "admin/ajax/removeCategoria.php",
                                type: 'POST',
                                data: {
                                    idcategoria: idcategoria
                                },
                                dataType: 'JSON',
                                success: function( response ) {
                                    if ( response.success ) {
                                        Ext.Msg.alert('Alerta!', response.msg, function(){
                                            $('div[data-idcategoria='+idcategoria+']').remove();
                                        });                                                
                                    }
                                }
                            });
                        }
                    });
                });

                $('.galeria-components button.btn-edit').off().on('click', function() {
                    var idcategoria = $( this ).attr('data-idcategoria');
                    windowAddCategoria(idcategoria, true);
                });
            }
        });
    }

    function windowUpload(uploadUrl, idpagina, despagina) {
        new Ext.Window({
            title   : 'Adicionar banner',
            width   : 600,
            height  : 120,
            id      : 'window-'+despagina,
            modal   : true,
            layout  : 'fit',
            iconCls : 'ico_arrow_top',
            items   : [{
                xtype: 'form',
                id: 'upload-form-'+despagina,
                fileUpload: true,
                border: false,
                width: 500,
                autoHeight: true,
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                labelWidth: 50,
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items:[{
                    xtype: 'fileuploadfield',
                    id: 'upload_file-'+despagina,
                    name: 'arquivo[]',
                    multiple : false,
                    emptyText: 'selecione uma imagem para fazer o upload...',
                    fieldLabel: 'Arquivo',
                    buttonText: 'Buscar'
                }],
                buttons: [{
                    text: 'Upload',
                    iconCls : 'ico_arrow_top',
                    handler: function(){
                        $.ajax({
                            type    : "POST",
                            url     : SITE_PATH + "admin/json/getBanners.php",
                            dataType: 'JSON',
                            data    : {
                                idpagina: idpagina
                            },
                            success:function(results, success){
                                var doUpload = true;

                                if ( idpagina == 2 ) {
                                    if ( results.banners.length > 0 ) {
                                        doUpload = false;
                                    }
                                }

                                if ( doUpload == true ) {
                                    if(Ext.getCmp('upload-form-'+despagina).getForm().isValid()){
                                        
                                        Ext.getCmp('upload-form-'+despagina).getForm().submit({
                                            url: 'ajax/upload_file.php',
                                            params : {
                                                upload_url: uploadUrl,
                                                idpagina: idpagina,
                                                tipoupload: 'banner'
                                            },
                                            waitMsg: 'Salvando o arquivo...',
                                            success: function(form,action){
                                                Ext.getCmp('upload-form-'+despagina).getForm().reset();
                                                loadBanners(idpagina, '-panel-'+despagina);
                                                alertSuccess('Imagem cadastrada com sucesso');
                                            }
                                        });
                                    }                                 
                                }
                                else{
                                    Ext.getCmp('window-'+despagina).close();
                                    alertError('Essa página não suporta mais de um banner');
                                }
                            }
                        });
                    }
                }]
            }]
        }).show();
    }

    function windowAddCategoria (idcategoria, editar) {
        new Ext.Window({
            title   : 'Adicionar categoria',
            width   : 600,
            height  : 140,
            id      : 'window-categoria-add',
            modal   : true,
            layout  : 'fit',
            iconCls : 'ico_add',
            items   : [{
                xtype: 'form',
                id: 'upload-form-categoria-add',
                fileUpload: true,
                border: false,
                width: 500,
                autoHeight: true,
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                labelWidth: 50,
                defaults: {
                    anchor: '95%',
                    msgTarget: 'side'
                },
                items:[{
                    xtype: 'fileuploadfield',
                    id: 'upload_file-categoria-add',
                    name: 'arquivo[]',
                    emptyText: 'selecione uma imagem de capa...',
                    fieldLabel: 'Arquivo',
                    buttonText: 'Buscar'
                },{
                    xtype: 'textfield',
                    fieldLabel: 'Descrição',
                    id: 'descategoria',
                    name: 'descategoria',
                    allowBlank: false
                },{
                    xtype: 'textfield',
                    id: 'desurlcapa',
                    name: 'desurlcapa',
                    hidden: true
                },{
                    xtype: 'textfield',
                    id: 'idcategoria',
                    name: 'idcategoria',
                    value: idcategoria,
                    hidden: true
                }],
                buttons: [{
                    text: 'Salvar',
                    iconCls : 'ico_save',
                    handler: function(){
                        
                        if(Ext.getCmp('upload-form-categoria-add').getForm().isValid()){
                            
                            if ( Ext.getCmp('upload_file-categoria-add').getValue() != '' ) {
                                Ext.getCmp('upload-form-categoria-add').getForm().submit({
                                    url: 'ajax/upload_file.php',
                                    params : {
                                        upload_url: '../../res/img/galeria/',
                                        idcategoria: idcategoria,
                                        tipoupload: 'categoria'
                                    },
                                    waitMsg: 'Salvando o arquivo...',
                                    success: function(form,action){
                                        Ext.getCmp('window-categoria-add').close();
                                        loadCategorias();
                                        alertSuccess('Categoria cadastrada com sucesso');
                                    }
                                });
                            }
                            else {
                                Ext.getCmp('upload-form-categoria-add').getForm().submit({
                                    url: 'ajax/editCategoria.php',
                                    params : {
                                        idcategoria: idcategoria
                                    },
                                    waitMsg: 'Salvando o arquivo...',
                                    success: function(form,action){
                                        Ext.getCmp('window-categoria-add').close();
                                        loadCategorias();
                                        alertSuccess('Categoria editada com sucesso');
                                    }
                                });
                            }
                            
                        }
                    }
                }]
            }],
            listeners: {
                'beforerender': function ( This ) {
                    if (editar == true) {
                        Ext.getCmp('window-categoria-add').setTitle('Editar categoria');

                        $.ajax({
                            url: SITE_PATH + "admin/json/getCategoria.php",
                            type: 'POST',
                            data: {
                                idcategoria: idcategoria
                            },
                            dataType: 'JSON',
                            success: function( reponse ) {
                                Ext.getCmp('upload-form-categoria-add').getForm().setValues( reponse.categoria );
                            }
                        });
                    }
                }
            }
        }).show();
    }

    if ( $("#config-img").length == 0 ) {
        var $style = $("<style id='config-img'>");
        $style.text(".categorias-conteiner{ float:left;clear:both; width:94%; margin-left:50px;margin-top:20px; position:relative; } .foto{float: left;position: relative;} .html {position: relative;}.html img{width: 80px; height: 80px; margin: 5px; float: left; border: 2px solid #19AFBB; }.html button.btn-close { position: absolute;top: 0px;right: 0px;border-radius: 50%;background: rgba(245, 51, 51, 0.98);border: #FA0707;color: #FFF; line-height: 20px;padding: inherit;padding: 2px 6px;cursor: pointer;} .html button.btn-close:hover {background: rgba(241, 18, 18, 0.98);} .layout-img{width: 600px; height: 600px; background-size: cover;} .galeria{margin-bottom: 15px;} .galeria-components{position:relative;width:200px;height:150px}.galeria-components img{width:100%;height:150px;float:left;border:2px solid #19AFBB}.galeria-components button.btn-close{position:absolute;top:-5px;right:-10px;border-radius:50%;background:rgba(245,51,51,.98);border:#FA0707;color:#FFF;line-height:20px;padding:2px 6px;cursor:pointer}.galeria-components button.btn-close:hover{background:rgba(241,18,18,.98)}div.descricao{position:absolute;bottom:-2px;margin-left:2px;width:100%;text-align:center;height:51px;font-size:20px;color:#FFF;display:block;overflow-wrap:break-word;background-color:rgba(0,0,0,.5)} .galeria-components button.btn-edit{position:absolute;top:-5px;right:15px;background:#eaab39;border:#E09610;color:#FFF;line-height:20px;padding:2px 6px;cursor:pointer}.galeria-components button.btn-edit:hover{background:#e09610}");
        $style.appendTo("head");
    }

    new Ext.Window({
        id      : windowIDAdmPaginas,
        width   : window.innerWidth,
        height  : window.innerHeight - 60,
        title   : 'Gerenciar páginas',
        iconCls : 'ico_application_osx',
        modal   : true,
        layout  : 'fit',
        items: [{
            xtype       : 'tabpanel',
            activeTab   : 0,
            items       : [{
                title       : 'Home',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-home",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(btn){
                        btn.disable();
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-home").getForm().isValid()){
                            var desarquivo = 'index.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-home").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    btn.enable();
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                        else {
                            btn.enable();
                        }
                    }
                }, '->', {
                    text    : 'Adicionar Banner',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        windowUpload('../../res/img/', 1, 'home');
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'index';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'index'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-home").getHeight() - 100;
                            Ext.getCmp(windowIDAdmPaginas+"-editor-home").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-home").setValue(conteudo);
                        });

                        loadBanners(1, '-panel-home');
                    }
                },
                items       : [{
                    xtype   : 'fieldset',
                    margins : {top:0, right:10, bottom:0, left:10},
                    title   : 'banners', 
                    items: [{
                        xtype       : 'panel',
                        anchor      : "99%",
                        height      : 100,
                        border      : false,
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+'-panel-home',
                        items       : [{}]
                    }]
                },{
                    xtype   :"form",
                    id      :windowIDAdmPaginas+"-form-home",
                    border  :false,
                    items   :[{
                        hideLabel        : true,
                        anchor           : "100%",
                        id               : windowIDAdmPaginas+"-editor-home",
                        xtype            : "htmleditor",
                        enableFont       : false,
                        enableAlignments : false,
                        height           : 365,
                        name             : "desconteudo"
                    }]
                }]
            },{
                title       : 'Sobre nós',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-sobre-nos",
                bbar        : [{
                    text    :"Salvar",
                    iconCls :"ico_save",
                    handler :function(){
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-sobre-nos").getForm().isValid()){
                            var desarquivo = 'sobre-nos.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-sobre-nos").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase(),
                                    classe: '.container-left'
                                },
                                success:function(){
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                    }
                }, '->', {
                    text    : 'Adicionar Banner',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        windowUpload('../../res/img/', 2, 'sobre-nos');
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'sobre-nos';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'sobre-nos',
                            classe: '.container-left'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-sobre-nos").getHeight() - 1000;
                            Ext.getCmp(windowIDAdmPaginas+"-editor-sobre-nos").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-sobre-nos").setValue(conteudo);
                        });

                        loadBanners(2, '-panel-sobre-nos');
                    }
                },
                items       : [{
                    xtype   : 'fieldset',
                    margins : {top:0, right:10, bottom:0, left:10},
                    title   : 'banner lateral', 
                    items: [{
                        xtype       : 'panel',
                        anchor      : "99%",
                        height      : 100,
                        border      : false,
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+'-panel-sobre-nos',
                        items       : [{}]
                    }]
                },{
                    xtype   :"form",
                    id      :windowIDAdmPaginas+"-form-sobre-nos",
                    border  :false,
                    items   :[{
                        hideLabel   : true,
                        anchor      : "100%",
                        id          : windowIDAdmPaginas+"-editor-sobre-nos",
                        xtype       : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        height      : 365,
                        name        : "desconteudo"
                    }]
                }]
            },{
                title       : 'Nossos serviços',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-nossos-servicos",
                layout      : 'fit',
                items       : [{
                    xtype       : 'tabpanel',
                    activeTab   : 0,
                    items       : [{
                        title   :  'Nossos serviços',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-servicos",
                        bbar        : [{
                            /*text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-tab-servicos").getForm().isValid()){
                                    var desarquivo = 'design-online.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-tab-servicos").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }*/
                            
                        },'-','->','-',{
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'nossos-servicos';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                var height = Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-servicos").getHeight();
                                Ext.getCmp(windowIDAdmPaginas+"-grid-servicos").setHeight(height - 40);
                            }
                        },
                        items:[{
                            xtype: 'grid',
                            id: windowIDAdmPaginas+'-grid-servicos',
                            loadMask: true,
                            height: 320,
                            autoExpandColumn: 'column_servicos',
                            store: new Ext.data.JsonStore({
                                url: 'json/listServicos.php',
                                root: 'myData',
                                autoLoad: true,
                                fields:[{name: 'idservico'},{name: 'desservico'},{name: 'desdescricao'},{name: 'desurl'},{name: 'desicon'},{name: 'instatus'}]
                            }),
                            columns:[{
                                header: '<center><b>Código</b></center>',
                                width: 300,
                                dataIndex: 'idservico'
                            },{
                                header: '<center><b>Serviço</b></center>',
                                width: 300,
                                dataIndex: 'desservico'
                            },{
                                header: '<center><b>Descrição</b></center>',
                                width: 300,
                                dataIndex: 'desdescricao',
                                id: 'column_servicos'
                            },{
                                header: '<center><b>Status</b></center>',
                                width: 100,
                                dataIndex: 'instatus',
                                align: 'center',
                                renderer: function(value){
                                    if(value == 1){
                                        return '<img src="res/img/icons/16/action_check.png" />';
                                    }else{
                                        return '<img src="res/img/icons/16/action_delete.png" />';
                                    }
                                }
                            }],
                            listeners:{
                                rowdblclick: function(grid,row,e){

                                    var rec = grid.getStore().getAt(row);
                                    
                                    var windowIDeditServico = 'win_edit_servico';
                                    
                                    if(Ext.getCmp(windowIDeditServico)){
                                        Ext.getCmp(windowIDeditServico).show();
                                    }else{

                                        new Ext.Window({
                                            title: "Editar serviço",
                                            id: windowIDeditServico,
                                            iconCls:'ico_Essen_settings',
                                            height: 600,
                                            width: 500,
                                            //border: false,
                                            modal: true,
                                            buttonAlign: 'center',
                                            buttons:[{
                                                text: 'Editar',
                                                iconCls: 'ico_arrow_refresh',
                                                height: 30,
                                                width: 100,
                                                handler: function(){

                                                    if(Ext.getCmp(windowIDeditServico+'-form').getForm().isValid()){

                                                        Ext.Ajax.request({
                                                            url:'ajax/editServico.php',
                                                            params:{
                                                                idservico: rec.get('idservico'),
                                                                desservico: Ext.getCmp(windowIDeditServico+'-edit-desservico').getValue(),
                                                                desdescricao: Ext.getCmp(windowIDeditServico+'-edit-servico-descricao').getValue(),
                                                                instatus: Ext.getCmp(windowIDeditServico+'-edit-servicos-status').getValue().inputValue
                                                            },
                                                            success:function(){
                                                                Ext.getCmp(windowIDeditServico).close();
                                                                Ext.getCmp(windowIDAdmPaginas+'-grid-servicos').getStore().reload();
                                                                Ext.Msg.alert('Aviso','Serviço atualizado com sucesso.');
                                                            }
                                                        })
                                                    }
                                                }
                                            }],
                                            items:[{
                                                xtype: 'form',
                                                id: windowIDeditServico+'-form',
                                                padding: 10,
                                                labelWidth: 70,
                                                border: false,
                                                defaults: {
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                },
                                                items:[{
                                                    xtype: 'textfield',
                                                    id: windowIDeditServico+'-edit-desservico',
                                                    fieldLabel: 'Serviço',
                                                    value: rec.get('desservico')
                                                },{
                                                    xtype: 'textarea',
                                                    id: windowIDeditServico+'-edit-servico-descricao',
                                                    fieldLabel: 'Descri\u00e7\u00e3o',
                                                    height: 200,
                                                    value: rec.get('desdescricao')
                                                },{
                                                    xtype: 'radiogroup',
                                                    fieldLabel: 'Status',
                                                    id: windowIDeditServico+'-edit-servicos-status',
                                                    items:[
                                                        {boxLabel: 'Ativado', name: 'rd_status', inputValue: 1},
                                                        {boxLabel: 'Desativado', name: 'rd_status', inputValue: 0}
                                                    ]
                                                }]
                                            }],
                                            listeners: {
                                                'beforerender': function() {
                                                    Ext.getCmp(windowIDeditServico+'-edit-servicos-status').setValue(rec.get('instatus'));
                                                }
                                            }
                                        }).show();
                                    }
                                }
                            }
                        }]
                    },{
                        title       :  'Arquitetura e design',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-arquitetura",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-arquitetura").getForm().isValid()){
                                    var desarquivo = 'arquitetura-e-design.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-arquitetura").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'arquitetura-e-design';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'arquitetura-e-design'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-arquitetura").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-arquitetura").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-arquitetura").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-arquitetura",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-arquitetura",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Consultoria técnica',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-consultoria",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-consultoria").getForm().isValid()){
                                    var desarquivo = 'consultoria-tecnica.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-consultoria").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'consultoria-tecnica';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'consultoria-tecnica'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-consultoria").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-consultoria").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-consultoria").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-consultoria",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-consultoria",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Sustentabilidade',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-sustentabilidade",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-sustentabilidade").getForm().isValid()){
                                    var desarquivo = 'sustentabilidade.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-sustentabilidade").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'sustentabilidade';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'sustentabilidade'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-sustentabilidade").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-sustentabilidade").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-sustentabilidade").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-sustentabilidade",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-sustentabilidade",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Gerenciamento de obra',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-gerenciamento",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-gerenciamento").getForm().isValid()){
                                    var desarquivo = 'gerenciamento-de-obra.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-gerenciamento").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'gerenciamento-de-obra';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'gerenciamento-de-obra'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-gerenciamento").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-gerenciamento").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-gerenciamento").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-gerenciamento",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-gerenciamento",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Maquete eletrónica',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-maquete",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-maquete").getForm().isValid()){
                                    var desarquivo = 'maquete-eletronica.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-maquete").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'maquete-eletronica';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'maquete-eletronica'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-maquete").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-maquete").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-maquete").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-maquete",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                                enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-maquete",
                                name      : "desconteudo",
                            }]
                        }]
                    }]
                }] 

            },{
                title: 'Galeria',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-galeria",
                layout      : 'border',
                bbar        : [{
                    text:"Adicionar Categoria",
                    iconCls:"ico_add",
                    handler:function(btn){
                        windowAddCategoria(0, false);
                    }
                }, '->', {
                    text    : 'Adicionar foto',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        new Ext.Window({
                            title   : 'Adicionar foto',
                            width   : 600,
                            height  : 140,
                            id      : 'window-galeria-foto',
                            modal   : true,
                            layout  : 'fit',
                            iconCls : 'ico_arrow_top',
                            items   : [{
                                xtype: 'form',
                                id: 'upload-form-galeria-foto',
                                fileUpload: true,
                                border: false,
                                multiple: true,
                                width: 500,
                                autoHeight: true,
                                bodyStyle: 'padding: 10px 10px 10px 10px;',
                                labelWidth: 50,
                                defaults: {
                                    anchor: '95%',
                                    allowBlank: false,
                                    msgTarget: 'side'
                                },
                                items:[{
                                    xtype: 'combo',
                                    id: 'combo-categorias',
                                    typeAhead: true,
                                    triggerAction: 'all',
                                    lazyRender: true,
                                    store: combo_categorias,
                                    valueField: 'idcategoria',
                                    displayField: 'descategoria',
                                    mode: 'local',
                                    name: 'descategoria',
                                    fieldLabel: 'Categoria',
                                    forceSelection: true,
                                    listeners: {
                                        'select':function(combo, record, index) {
                                            idcategoria = record.get('idcategoria');
                                        }
                                    }
                                },{
                                    xtype: 'fileuploadfield',
                                    id: 'upload_file-galeria-foto',
                                    name: 'arquivo[]',
                                    multiple: true,
                                    emptyText: 'selecione uma imagem para fazer o upload...',
                                    fieldLabel: 'Arquivo',
                                    buttonText: 'Buscar'
                                }],
                                buttons: [{
                                    text: 'Upload',
                                    iconCls : 'ico_arrow_top',
                                    handler: function(){
                                        if(Ext.getCmp('upload-form-galeria-foto').getForm().isValid()){
                                                
                                            Ext.getCmp('upload-form-galeria-foto').getForm().submit({
                                                url: 'ajax/upload_file.php',
                                                params : {
                                                    upload_url: '../../res/img/galeria/',
                                                    tipoupload: 'foto',
                                                    idcategoria: Ext.getCmp('combo-categorias').getValue()
                                                },
                                                waitMsg: 'Salvando o arquivo...',
                                                success: function(form,action){
                                                    $('.galeria[data-idcategoria='+idcategoria+']').trigger('click');
                                                    Ext.getCmp('upload-form-galeria-foto').getForm().reset();
                                                    
                                                    alertSuccess('Foto cadastrada com sucesso');
                                                }
                                            });                                
                                        }
                                    }
                                }]
                            }],
                            listeners: {
                                'afterrender':function (){
                                    if (idcategoria > 0) {
                                        Ext.getCmp('combo-categorias').setValue(idcategoria);                                        
                                    }
                                }
                            }
                        }).show();
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'galeria';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                items       : [{
                    region: 'west',
                    id: windowIDAdmPaginas+'galeria-categorias', // see Ext.getCmp() below
                    autoScroll  : true,
                    xtype: 'panel',
                    split: true,                   
                    title: 'Categorias',
                    collapsible: true,
                    width: 240,
                    minWidth: 240,
                    maxWidth: 240,
                    collapsible: true,
                    animCollapse: true,
                    padding: 10,
                    margins: '0 0 0 0'
                }, {
                    region      : 'center',
                    xtype       : 'panel',
                    id: windowIDAdmPaginas+'galeria-fotos', // see Ext.getCmp() below
                    autoScroll  : true,
                    split: true,
                    title: 'Fotos',
                    items       : [{}]
                }],
                listeners : {
                    activate : function() {
                        loadCategorias();
                    }
                }
            },/*{
                title: 'Contato'
            },*/{
                title       : 'FAQ',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-faq",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(btn){
                        loadInBtn(btn);
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-faq").getForm().isValid()){
                            var desarquivo = 'FAQ.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-faq").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    loadInBtn(btn);
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                        else {
                            loadInBtn(btn);
                        }
                    }
                }, '->', {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'FAQ';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'FAQ'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-faq").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+"-editor-faq").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-faq").setValue(conteudo);
                        });
                    }
                },
                items: [{
                    xtype   : "form",
                    id      : windowIDAdmPaginas+"-form-faq",
                    border  : false,
                    items:[{
                        hideLabel : true,
                        anchor    : "100%",
                        xtype     : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        id        : windowIDAdmPaginas+"-editor-faq",
                        name      : "desconteudo",
                    }]
                }]
            },{
                title       : 'Política de privacidade',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-politicas",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(){
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-politicas").getForm().isValid()){
                            var desarquivo = 'politica-de-privacidade.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-politicas").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                    }
                }, '->', {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'politica-de-privacidade';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = removeAcento(pagina.toLowerCase())+'.php';
                        }
                        else{
                            var despagina = removeAcento(pagina.toLowerCase());
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'politica-de-privacidade'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-politicas").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setValue(conteudo);
                        });
                    }
                },
                items: [{
                    xtype   : "form",
                    id      : windowIDAdmPaginas+"-form-politicas",
                    border  : false,
                    items:[{
                        hideLabel : true,
                        anchor    : "100%",
                        xtype     : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        id        : windowIDAdmPaginas+"-editor-politicas",
                        name      : "desconteudo",
                    }]
                }]
            },{
                title       : 'Nossos Planos',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-nossos-planos",
                bbar: ['Filtrar:', {
                    xtype:"combo",
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'tipo'
                        ],
                        data: [[1, 'Ativo'], [2, 'Inativo']]
                    }),
                    valueField: 'id',
                    displayField: 'tipo',
                    listeners: {
                        select: function (obj, record) {
                            if(record.get("id")==1){
                                Ext.getCmp(windowIDAdmPaginas + '-grid-nossos-planos').getStore().filter('instatus', 1);
                            } else {
                                Ext.getCmp(windowIDAdmPaginas + '-grid-nossos-planos').getStore().filter('instatus', 0);
                            }
                        }
                    }
                }, '-', {
                    // text:"Salvar",
                    // iconCls:"ico_save",
                    // handler:function(){
                    //     if(Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getForm().isValid()){
                    //         var desarquivo = 'design-online.html';
                    //         Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getForm().submit({
                    //             url:"ajax/editPagina.php",
                    //             params:{
                    //                 despagina: desarquivo.toLowerCase()
                    //             },
                    //             success:function(){
                    //                 alertSuccess("Página editada com sucesso!");
                    //             }
                    //         });
                    //     }
                    // }
                    text    : 'Adicionar Plano',
                    iconCls : 'ico_add',
                    handler : function () {

                        var windowIDaddPlano = 'win_add_plano';
                        
                        if(Ext.getCmp(windowIDaddPlano)){
                            Ext.getCmp(windowIDaddPlano).show();
                        }else{

                            new Ext.Window({
                                title: "Adicionar Plano",
                                id: windowIDaddPlano,
                                iconCls:'ico_Essen_product_add',
                                height: 550,
                                width: 650,
                                //border: false,
                                modal: true,
                                buttonAlign: 'center',
                                buttons:[{
                                    text: 'Cadastrar',
                                    iconCls: 'ico_save',
                                    height: 30,
                                    width: 100,
                                    handler: function(){

                                        if(Ext.getCmp(windowIDaddPlano+'-form').getForm().isValid()){

                                            Ext.Ajax.request({
                                                url:'ajax/addNossosPlanos.php',
                                                params:{
                                                    titulo: Ext.getCmp(windowIDaddPlano+'-add-plano-titulo').getValue(),
                                                    valor: Ext.getCmp(windowIDaddPlano+'-add-plano-valor').getValue(),
                                                    descricao: Ext.getCmp(windowIDaddPlano+'-add-plano-descricao').getValue(),
                                                    informacao: Ext.getCmp(windowIDaddPlano+'-add-plano-informacao').getValue(),
                                                },
                                                success:function(){

                                                    Ext.getCmp(windowIDaddPlano+'-form').getForm().reset();
                                                    Ext.getCmp(windowIDaddPlano).close();
                                                    Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                    Ext.Msg.alert('Aviso','Plano cadastrado com sucesso.');
                                                }
                                            })
                                        }
                                    }
                                }],
                                items:[{
                                    xtype: 'form',
                                    id: windowIDaddPlano+'-form',
                                    padding: 10,
                                    labelWidth: 70,
                                    border: false,
                                    defaults: {
                                        allowBlank: false,
                                        anchor: '100%'
                                    },
                                    items:[{
                                        xtype: 'textfield',
                                        id: windowIDaddPlano+'-add-plano-titulo',
                                        fieldLabel: 'T\u00edtulo'
                                    },{
                                        xtype: 'numberfield',
                                        id: windowIDaddPlano+'-add-plano-valor',
                                        fieldLabel: 'Valor',
                                        decimalSeparator: ','
                                    },{
                                        xtype: "htmleditor",
                                        enableFont: false,
                                        enableAlignments: false,
                                        id: windowIDaddPlano+'-add-plano-descricao',
                                        fieldLabel: 'Descri\u00e7\u00e3o',
                                        height: 200
                                    },{
                                        xtype: "htmleditor",
                                        enableFont: false,
                                        enableAlignments: false,
                                        id: windowIDaddPlano+'-add-plano-informacao',
                                        fieldLabel: 'Mais Informa\u00e7\u00e3o',
                                        height: 200,
                                        allowBlank: true
                                    }]
                                }]
                            }).show();
                        }
                    }
                },'-','->','-',{
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'design-online';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = removeAcento(pagina.toLowerCase())+'.php';
                        }
                        else{
                            var despagina = removeAcento(pagina.toLowerCase());
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'design-online'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').setHeight(height);
                            // Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setValue(conteudo);
                        });
                    }
                },
                items:[{
                    xtype: 'grid',
                    id: windowIDAdmPaginas+'-grid-nossos-planos',
                    loadMask: true,
                    height: 320,
                    autoExpandColumn: 'column_planos_descricao',
                    store: new Ext.data.JsonStore({
                        url: 'json/listNossosPlanos.php',
                        root: 'myData',
                        autoLoad: true,
                        fields:[{name: 'idplano'},{name: 'desplano'},{name: 'vlplano'},{name: 'instatus'},{name: 'descricao'},{name: 'desinformacao'},'nrposicao']
                    }),
                    columns:[{
                        header: '<center><b>Plano</b></center>',
                        width: 300,
                        dataIndex: 'desplano'
                    },{
                        header: '<center><b>Valor</b></center>',
                        width: 250,
                        dataIndex: 'vlplano',
                        renderer: Ext.util.Format.brMoney,
                    },{
                        header: '<center><b>Descrição</b></center>',
                        id: 'column_planos_descricao',
                        dataIndex: 'descricao',
                    },{
                        header: '<center><b>Mais Informação</b></center>',
                        width: 600,
                        dataIndex: 'desinformacao',
                    },{
                        header: '<center><b>Status</b></center>',
                        width: 100,
                        dataIndex: 'instatus',
                        align: 'center',
                        renderer: function(value){
                            if(value == 1){
                                return '<img src="res/img/icons/16/action_check.png" />';
                            }else{
                                return '<img src="res/img/icons/16/action_delete.png" />';
                            }
                        }
                    }],
                    listeners:{
                        rowdblclick: function(grid,row,e){
                            
                            var windowIDeditPlano = 'win_edit_plano';
                            
                            if(Ext.getCmp(windowIDeditPlano)){
                                Ext.getCmp(windowIDeditPlano).show();
                            }else{

                                new Ext.Window({
                                    title: "Editar Plano",
                                    id: windowIDeditPlano,
                                    iconCls:'ico_Essen_product_edit',
                                    height: 600,
                                    width: 600,
                                    //border: false,
                                    modal: true,
                                    buttonAlign: 'center',
                                    buttons:[{
                                        text: 'Editar',
                                        iconCls: 'ico_arrow_refresh',
                                        height: 30,
                                        width: 100,
                                        handler: function(){

                                            if(Ext.getCmp(windowIDeditPlano+'-form').getForm().isValid()){

                                                Ext.Ajax.request({
                                                    url:'ajax/editNossosPlanos.php',
                                                    params:{
                                                        idplano: Ext.getCmp(windowIDeditPlano+'-edit-plano-idplano').getValue(),
                                                        titulo: Ext.getCmp(windowIDeditPlano+'-edit-plano-titulo').getValue(),
                                                        nrposicao: Ext.getCmp(windowIDeditPlano + '-edit-nrposicao').getValue(),
                                                        valor: Ext.getCmp(windowIDeditPlano + '-edit-plano-valor').getValue(),
                                                        descricao: Ext.getCmp(windowIDeditPlano+'-edit-plano-descricao').getValue(),
                                                        informacao: Ext.getCmp(windowIDeditPlano+'-edit-plano-informacao').getValue(),
                                                        instatus: Ext.getCmp(windowIDeditPlano+'-edit-plano-status').getValue().inputValue,
                                                    },
                                                    success:function(){
                                                        Ext.getCmp(windowIDeditPlano).close();
                                                        Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                        Ext.Msg.alert('Aviso','Plano atualizado com sucesso.');
                                                    }
                                                })
                                            }
                                        }
                                    }],
                                    listeners:{
                                        afterrender:function(){

                                            var rec = grid.getStore().getAt(row);

                                            Ext.getCmp(windowIDeditPlano + '-edit-nrposicao').setValue(rec.data.nrposicao);
                                            Ext.getCmp(windowIDeditPlano + '-edit-plano-idplano').setValue(rec.data.idplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-titulo').setValue(rec.data.desplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-valor').setValue(rec.data.vlplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-descricao').setValue(rec.data.descricao);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-informacao').setValue(rec.data.desinformacao);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-status').setValue(rec.data.instatus);
                                        }
                                    },
                                    items:[{
                                        xtype: 'form',
                                        id: windowIDeditPlano+'-form',
                                        padding: 10,
                                        labelWidth: 70,
                                        border: false,
                                        defaults: {
                                            allowBlank: false,
                                            anchor: '100%'
                                        },
                                        items:[{
                                            xtype: 'hidden',
                                            id: windowIDeditPlano+'-edit-plano-idplano',
                                        },{
                                            xtype: 'textfield',
                                            id: windowIDeditPlano+'-edit-plano-titulo',
                                            fieldLabel: 'T\u00edtulo'
                                        },{
                                            xtype: 'numberfield',
                                            id: windowIDeditPlano+'-edit-plano-valor',
                                            fieldLabel: 'Valor',
                                            decimalSeparator: ',',
                                        }, {
                                            xtype: 'numberfield',
                                            id: windowIDeditPlano + '-edit-nrposicao',
                                            fieldLabel: 'Posição',
                                            decimalSeparator: ',',
                                        }, {
                                            xtype: "htmleditor",
                                            enableFont: false,
                                            enableAlignments: false,
                                            id: windowIDeditPlano+'-edit-plano-descricao',
                                            fieldLabel: 'Descri\u00e7\u00e3o',
                                            height: 200
                                        },{
                                            xtype: "htmleditor",
                                            enableFont: false,
                                            enableAlignments: false,
                                            id: windowIDeditPlano+'-edit-plano-informacao',
                                            fieldLabel: 'Mais Informa\u00e7\u00e3o',
                                            height: 200,
                                            allowBlank: true
                                        },{
                                            xtype: 'radiogroup',
                                            fieldLabel: 'Status',
                                            id: windowIDeditPlano+'-edit-plano-status',
                                            items:[
                                                {boxLabel: 'Ativado', name: 'rd_status', inputValue: 1},
                                                {boxLabel: 'Desativado', name: 'rd_status', inputValue: 0}
                                            ]
                                        }]
                                    }]
                                }).show();
                            }
                        },
                        rowcontextmenu: function (obj, rowIndex, e) {
                            e.stopEvent();
                            obj.getSelectionModel().selectRow(rowIndex);
                            //
                            new Ext.menu.Menu({
                                items: [{
                                    text: 'Excluir Plano',
                                    iconCls: 'ico_action_delete',
                                    handler: function () {
                                        if (Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getSelectionModel().hasSelection()) {
                                            var row = Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getSelectionModel().getSelected();

                                            Ext.Ajax.request({
                                                url: 'ajax/removePlano.php',
                                                params: {
                                                    idplano: row.get('idplano')
                                                },
                                                success: function (a) {
                                                    var json = $.parseJSON(a.responseText);
                                                    if (json.success) {
                                                        Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                        Ext.Msg.alert('Aviso', 'O plano foi removido com sucesso.');
                                                    } else {
                                                        Ext.Msg.alert('Aviso', json.msg);
                                                    }

                                                }
                                            });

                                        } else {
                                            Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                                        }
                                    }
                                }]
                            }).showAt(e.xy);
                        }
                    }
                }]
            }]
        }]
    }).show();
}var windowIDAdmPaginas = "win.adm-paginas";

function removeAcento(strToReplace) {
    str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
    str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var nova = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
            nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
        } else {
            nova += strToReplace.substr(i, 1);
        }
    }
    return nova;
}

if(Ext.getCmp(windowIDAdmPaginas)){
    Ext.getCmp(windowIDAdmPaginas).show();
} else {

    /*myMask.show();*/

    var combo_categorias = new Ext.data.JsonStore({
        url: SITE_PATH + "admin/json/getCategorias.php",
        autoLoad:true,
        root:"categorias",
        fields:[
            {name: 'idcategoria', type: 'int'},
            {name: 'descategoria', type: 'string'}
        ]
    });

    /*var tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap" id="{name}">',
            '<div class="thumb"><img src="{url}" title="{name}"></div>',
            '<span class="x-editable">{shortName}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );   */ 
    
    function loadBanners(idpagina, idpanel) {
        $.ajax({
            type    : "POST",
            url     : SITE_PATH + "admin/json/getBanners.php",
            dataType: 'JSON',
            data    : {
                idpagina: idpagina
            },
            success:function(results, success){
                html_img = '';
                $.each(results.banners, function(index, rec){
                    
                    html_img += '<div id="banner-'+rec.idbanner+'" >';
                        html_img += '<span class="html">';
                            html_img += '<img src="'+SITE_PATH+rec.desurl+'">';
                            html_img += '<button data-idbanner="'+rec.idbanner+'" type="button" class="btn-close">x</button>';
                        html_img += '</span>';
                    html_img += '</div>';
                    
                });
                                       
                var panel = Ext.getCmp(windowIDAdmPaginas+idpanel);

                var tpl = new Ext.XTemplate('<div class="banners-conteiner">',html_img,'</div>');


                tpl.overwrite(panel.body, {foo: 'bar'});
                panel.body.highlight('#c3daf9', {block:true});

                $('button.btn-close').off().on('click', function() {
                    var idbanner = $( this ).attr('data-idbanner');

                    Ext.Msg.confirm('Atenção!', "Deseja remover este banner?", function(resposta){
                        if ( resposta == 'yes' ) {

                            $.ajax({
                                url: SITE_PATH + "admin/ajax/removeBanner.php",
                                type: 'POST',
                                data: {
                                    idbanner: idbanner
                                },
                                dataType: 'JSON',
                                success: function( response ) {
                                    if ( response.success ) {
                                        Ext.Msg.alert('Alerta!', response.msg, function(){
                                            $('#banner-'+idbanner).remove();
                                        });                                                
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });
    }

    var idcategoria = 0;

    function loadCategorias () {
        $.ajax({
            type    : "POST",
            url     : SITE_PATH + "admin/json/getCategorias.php",
            dataType: 'JSON',
            success:function(results, success){
                html_img = '';
                $.each(results.categorias, function(index, rec){
                    
                    html_img += '<div class="galeria" data-idcategoria="'+rec.idcategoria+'">';
                        html_img += '<div class="galeria-components">';
                            html_img += '<img src="'+SITE_PATH+rec.desurlcapa+'">';
                            html_img += '<div class="descricao">'+rec.descategoria+'</div>';
                            html_img += '<button data-idcategoria="'+rec.idcategoria+'" type="button" class="btn-edit">Editar</button>';
                            html_img += '<button data-idcategoria="'+rec.idcategoria+'" type="button" class="btn-close">x</button>';
                        html_img += '</div>';
                    html_img += '</div>';

                });
                                       
                var p = Ext.getCmp(windowIDAdmPaginas+'galeria-categorias');

                var tpl = new Ext.XTemplate('<div class="banners-conteiner">',html_img,'</div>');


                tpl.overwrite(p.body, {foo: 'bar'});
                p.body.highlight('#c3daf9', {block:true});

                $('.banners-conteiner').sortable({
                    placeholder: "ui-state-highlight",
                    helper: 'clone',
                    out: function() {
                        var categorias = '';
                        $.each($(this).find('.galeria'), function(index, value){
                            categorias += $(value).attr('data-idcategoria');
                            if (($('.banners-conteiner').find('.galeria').length - 1) != index) {
                                categorias += ',';
                            }
                        });
                        
                        $.ajax({
                            type    : "POST",
                            url     : SITE_PATH + "admin/ajax/ordenarCategorias.php",
                            dataType: 'JSON',
                            data: {
                                categorias: categorias
                            },
                            success:function(results, success){

                            }
                        });
                    }
                });

                $('.galeria').off().on('click', function() {
                    idcategoria = $( this ).attr('data-idcategoria');

                    $.ajax({
                        type    : "POST",
                        url     : SITE_PATH + "admin/json/getGaleriaFotos.php",
                        dataType: 'JSON',
                        data: {
                            galeria: idcategoria
                        },
                        success:function(results, success){
                            html_img = '';
                            $.each(results.fotos, function(index, rec){
                                
                                html_img += '<div class="foto" data-idfoto="'+rec.idfoto+'">';
                                    html_img += '<span class="html">';
                                        html_img += '<img src="'+SITE_PATH+rec.desurl+'">';
                                        html_img += '<button data-idfoto="'+rec.idfoto+'" type="button" class="btn-close">x</button>';
                                    html_img += '</span>';
                                html_img += '</div>';

                            });

                            var p = Ext.getCmp(windowIDAdmPaginas+'galeria-fotos');

                            p.setTitle('Fotos - ' + $('div[data-idcategoria='+idcategoria+'] .descricao').html());
                            var tpl = new Ext.XTemplate('<div class="categorias-conteiner">',html_img,'</div>');


                            tpl.overwrite(p.body, {foo: 'bar'});
                            p.body.highlight('#c3daf9', {block:true});

                            $('.foto button.btn-close').off().on('click', function() {
                                var idfoto = $( this ).attr('data-idfoto');
                                
                                Ext.Msg.confirm('Atenção!', "Deseja remover esta foto da galeria?", function(resposta){
                                    if ( resposta == 'yes' ) {

                                        $.ajax({
                                            url: SITE_PATH + "admin/ajax/removeFoto.php",
                                            type: 'POST',
                                            data: {
                                                idfoto: idfoto
                                            },
                                            dataType: 'JSON',
                                            success: function( response ) {
                                                if ( response.success ) {
                                                    Ext.Msg.alert('Alerta!', response.msg, function(){
                                                        $('div[data-idfoto='+idfoto+']').remove();
                                                    });                                                
                                                }
                                            }
                                        });
                                    }
                                });
                            });

                            $('.categorias-conteiner').sortable({
                                placeholder: "ui-state-highlight",
                                helper: 'clone',
                                out: function() {
                                    var fotos = '';
                                    $.each($(this).find('.foto'), function(index, value){
                                        fotos += $(value).attr('data-idfoto');
                                        if (($('.categorias-conteiner').find('.foto').length - 1) != index) {
                                            fotos += ',';
                                        }
                                    });

                                    
                                    $.ajax({
                                        type    : "POST",
                                        url     : SITE_PATH + "admin/ajax/ordenarFotos.php",
                                        dataType: 'JSON',
                                        data: {
                                            idcategoria: idcategoria,
                                            fotos: fotos
                                        },
                                        success:function(results, success){

                                        }
                                    });
                                }
                            });

                        }
                    });
                });

                $('.galeria-components button.btn-close').off().on('click', function() {
                    var idcategoria = $( this ).attr('data-idcategoria');
                    
                    Ext.Msg.confirm('Atenção!', "Deseja remover esta categoria?", function(resposta){
                        if ( resposta == 'yes' ) {

                            $.ajax({
                                url: SITE_PATH + "admin/ajax/removeCategoria.php",
                                type: 'POST',
                                data: {
                                    idcategoria: idcategoria
                                },
                                dataType: 'JSON',
                                success: function( response ) {
                                    if ( response.success ) {
                                        Ext.Msg.alert('Alerta!', response.msg, function(){
                                            $('div[data-idcategoria='+idcategoria+']').remove();
                                        });                                                
                                    }
                                }
                            });
                        }
                    });
                });

                $('.galeria-components button.btn-edit').off().on('click', function() {
                    var idcategoria = $( this ).attr('data-idcategoria');
                    windowAddCategoria(idcategoria, true);
                });
            }
        });
    }

    function windowUpload(uploadUrl, idpagina, despagina) {
        new Ext.Window({
            title   : 'Adicionar banner',
            width   : 600,
            height  : 120,
            id      : 'window-'+despagina,
            modal   : true,
            layout  : 'fit',
            iconCls : 'ico_arrow_top',
            items   : [{
                xtype: 'form',
                id: 'upload-form-'+despagina,
                fileUpload: true,
                border: false,
                width: 500,
                autoHeight: true,
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                labelWidth: 50,
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items:[{
                    xtype: 'fileuploadfield',
                    id: 'upload_file-'+despagina,
                    name: 'arquivo[]',
                    multiple : false,
                    emptyText: 'selecione uma imagem para fazer o upload...',
                    fieldLabel: 'Arquivo',
                    buttonText: 'Buscar'
                }],
                buttons: [{
                    text: 'Upload',
                    iconCls : 'ico_arrow_top',
                    handler: function(){
                        $.ajax({
                            type    : "POST",
                            url     : SITE_PATH + "admin/json/getBanners.php",
                            dataType: 'JSON',
                            data    : {
                                idpagina: idpagina
                            },
                            success:function(results, success){
                                var doUpload = true;

                                if ( idpagina == 2 ) {
                                    if ( results.banners.length > 0 ) {
                                        doUpload = false;
                                    }
                                }

                                if ( doUpload == true ) {
                                    if(Ext.getCmp('upload-form-'+despagina).getForm().isValid()){
                                        
                                        Ext.getCmp('upload-form-'+despagina).getForm().submit({
                                            url: 'ajax/upload_file.php',
                                            params : {
                                                upload_url: uploadUrl,
                                                idpagina: idpagina,
                                                tipoupload: 'banner'
                                            },
                                            waitMsg: 'Salvando o arquivo...',
                                            success: function(form,action){
                                                Ext.getCmp('upload-form-'+despagina).getForm().reset();
                                                loadBanners(idpagina, '-panel-'+despagina);
                                                alertSuccess('Imagem cadastrada com sucesso');
                                            }
                                        });
                                    }                                 
                                }
                                else{
                                    Ext.getCmp('window-'+despagina).close();
                                    alertError('Essa página não suporta mais de um banner');
                                }
                            }
                        });
                    }
                }]
            }]
        }).show();
    }

    function windowAddCategoria (idcategoria, editar) {
        new Ext.Window({
            title   : 'Adicionar categoria',
            width   : 600,
            height  : 140,
            id      : 'window-categoria-add',
            modal   : true,
            layout  : 'fit',
            iconCls : 'ico_add',
            items   : [{
                xtype: 'form',
                id: 'upload-form-categoria-add',
                fileUpload: true,
                border: false,
                width: 500,
                autoHeight: true,
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                labelWidth: 50,
                defaults: {
                    anchor: '95%',
                    msgTarget: 'side'
                },
                items:[{
                    xtype: 'fileuploadfield',
                    id: 'upload_file-categoria-add',
                    name: 'arquivo[]',
                    emptyText: 'selecione uma imagem de capa...',
                    fieldLabel: 'Arquivo',
                    buttonText: 'Buscar'
                },{
                    xtype: 'textfield',
                    fieldLabel: 'Descrição',
                    id: 'descategoria',
                    name: 'descategoria',
                    allowBlank: false
                },{
                    xtype: 'textfield',
                    id: 'desurlcapa',
                    name: 'desurlcapa',
                    hidden: true
                },{
                    xtype: 'textfield',
                    id: 'idcategoria',
                    name: 'idcategoria',
                    value: idcategoria,
                    hidden: true
                }],
                buttons: [{
                    text: 'Salvar',
                    iconCls : 'ico_save',
                    handler: function(){
                        
                        if(Ext.getCmp('upload-form-categoria-add').getForm().isValid()){
                            
                            if ( Ext.getCmp('upload_file-categoria-add').getValue() != '' ) {
                                Ext.getCmp('upload-form-categoria-add').getForm().submit({
                                    url: 'ajax/upload_file.php',
                                    params : {
                                        upload_url: '../../res/img/galeria/',
                                        idcategoria: idcategoria,
                                        tipoupload: 'categoria'
                                    },
                                    waitMsg: 'Salvando o arquivo...',
                                    success: function(form,action){
                                        Ext.getCmp('window-categoria-add').close();
                                        loadCategorias();
                                        alertSuccess('Categoria cadastrada com sucesso');
                                    }
                                });
                            }
                            else {
                                Ext.getCmp('upload-form-categoria-add').getForm().submit({
                                    url: 'ajax/editCategoria.php',
                                    params : {
                                        idcategoria: idcategoria
                                    },
                                    waitMsg: 'Salvando o arquivo...',
                                    success: function(form,action){
                                        Ext.getCmp('window-categoria-add').close();
                                        loadCategorias();
                                        alertSuccess('Categoria editada com sucesso');
                                    }
                                });
                            }
                            
                        }
                    }
                }]
            }],
            listeners: {
                'beforerender': function ( This ) {
                    if (editar == true) {
                        Ext.getCmp('window-categoria-add').setTitle('Editar categoria');

                        $.ajax({
                            url: SITE_PATH + "admin/json/getCategoria.php",
                            type: 'POST',
                            data: {
                                idcategoria: idcategoria
                            },
                            dataType: 'JSON',
                            success: function( reponse ) {
                                Ext.getCmp('upload-form-categoria-add').getForm().setValues( reponse.categoria );
                            }
                        });
                    }
                }
            }
        }).show();
    }

    if ( $("#config-img").length == 0 ) {
        var $style = $("<style id='config-img'>");
        $style.text(".categorias-conteiner{ float:left;clear:both; width:94%; margin-left:50px;margin-top:20px; position:relative; } .foto{float: left;position: relative;} .html {position: relative;}.html img{width: 80px; height: 80px; margin: 5px; float: left; border: 2px solid #19AFBB; }.html button.btn-close { position: absolute;top: 0px;right: 0px;border-radius: 50%;background: rgba(245, 51, 51, 0.98);border: #FA0707;color: #FFF; line-height: 20px;padding: inherit;padding: 2px 6px;cursor: pointer;} .html button.btn-close:hover {background: rgba(241, 18, 18, 0.98);} .layout-img{width: 600px; height: 600px; background-size: cover;} .galeria{margin-bottom: 15px;} .galeria-components{position:relative;width:200px;height:150px}.galeria-components img{width:100%;height:150px;float:left;border:2px solid #19AFBB}.galeria-components button.btn-close{position:absolute;top:-5px;right:-10px;border-radius:50%;background:rgba(245,51,51,.98);border:#FA0707;color:#FFF;line-height:20px;padding:2px 6px;cursor:pointer}.galeria-components button.btn-close:hover{background:rgba(241,18,18,.98)}div.descricao{position:absolute;bottom:-2px;margin-left:2px;width:100%;text-align:center;height:51px;font-size:20px;color:#FFF;display:block;overflow-wrap:break-word;background-color:rgba(0,0,0,.5)} .galeria-components button.btn-edit{position:absolute;top:-5px;right:15px;background:#eaab39;border:#E09610;color:#FFF;line-height:20px;padding:2px 6px;cursor:pointer}.galeria-components button.btn-edit:hover{background:#e09610}");
        $style.appendTo("head");
    }

    new Ext.Window({
        id      : windowIDAdmPaginas,
        width   : window.innerWidth,
        height  : window.innerHeight - 60,
        title   : 'Gerenciar páginas',
        iconCls : 'ico_application_osx',
        modal   : true,
        layout  : 'fit',
        items: [{
            xtype       : 'tabpanel',
            activeTab   : 0,
            items       : [{
                title       : 'Home',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-home",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(btn){
                        btn.disable();
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-home").getForm().isValid()){
                            var desarquivo = 'index.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-home").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    btn.enable();
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                        else {
                            btn.enable();
                        }
                    }
                }, '->', {
                    text    : 'Adicionar Banner',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        windowUpload('../../res/img/', 1, 'home');
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'index';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'index'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-home").getHeight() - 100;
                            Ext.getCmp(windowIDAdmPaginas+"-editor-home").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-home").setValue(conteudo);
                        });

                        loadBanners(1, '-panel-home');
                    }
                },
                items       : [{
                    xtype   : 'fieldset',
                    margins : {top:0, right:10, bottom:0, left:10},
                    title   : 'banners', 
                    items: [{
                        xtype       : 'panel',
                        anchor      : "99%",
                        height      : 100,
                        border      : false,
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+'-panel-home',
                        items       : [{}]
                    }]
                },{
                    xtype   :"form",
                    id      :windowIDAdmPaginas+"-form-home",
                    border  :false,
                    items   :[{
                        hideLabel        : true,
                        anchor           : "100%",
                        id               : windowIDAdmPaginas+"-editor-home",
                        xtype            : "htmleditor",
                        enableFont       : false,
                        enableAlignments : false,
                        height           : 365,
                        name             : "desconteudo"
                    }]
                }]
            },{
                title       : 'Sobre nós',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-sobre-nos",
                bbar        : [{
                    text    :"Salvar",
                    iconCls :"ico_save",
                    handler :function(){
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-sobre-nos").getForm().isValid()){
                            var desarquivo = 'sobre-nos.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-sobre-nos").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase(),
                                    classe: '.container-left'
                                },
                                success:function(){
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                    }
                }, '->', {
                    text    : 'Adicionar Banner',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        windowUpload('../../res/img/', 2, 'sobre-nos');
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'sobre-nos';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'sobre-nos',
                            classe: '.container-left'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-sobre-nos").getHeight() - 1000;
                            Ext.getCmp(windowIDAdmPaginas+"-editor-sobre-nos").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-sobre-nos").setValue(conteudo);
                        });

                        loadBanners(2, '-panel-sobre-nos');
                    }
                },
                items       : [{
                    xtype   : 'fieldset',
                    margins : {top:0, right:10, bottom:0, left:10},
                    title   : 'banner lateral', 
                    items: [{
                        xtype       : 'panel',
                        anchor      : "99%",
                        height      : 100,
                        border      : false,
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+'-panel-sobre-nos',
                        items       : [{}]
                    }]
                },{
                    xtype   :"form",
                    id      :windowIDAdmPaginas+"-form-sobre-nos",
                    border  :false,
                    items   :[{
                        hideLabel   : true,
                        anchor      : "100%",
                        id          : windowIDAdmPaginas+"-editor-sobre-nos",
                        xtype       : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        height      : 365,
                        name        : "desconteudo"
                    }]
                }]
            },{
                title       : 'Nossos serviços',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-nossos-servicos",
                layout      : 'fit',
                items       : [{
                    xtype       : 'tabpanel',
                    activeTab   : 0,
                    items       : [{
                        title   :  'Nossos serviços',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-servicos",
                        bbar        : [{
                            /*text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-tab-servicos").getForm().isValid()){
                                    var desarquivo = 'design-online.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-tab-servicos").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }*/
                            
                        },'-','->','-',{
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'nossos-servicos';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                var height = Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-servicos").getHeight();
                                Ext.getCmp(windowIDAdmPaginas+"-grid-servicos").setHeight(height - 40);
                            }
                        },
                        items:[{
                            xtype: 'grid',
                            id: windowIDAdmPaginas+'-grid-servicos',
                            loadMask: true,
                            height: 320,
                            autoExpandColumn: 'column_servicos',
                            store: new Ext.data.JsonStore({
                                url: 'json/listServicos.php',
                                root: 'myData',
                                autoLoad: true,
                                fields:[{name: 'idservico'},{name: 'desservico'},{name: 'desdescricao'},{name: 'desurl'},{name: 'desicon'},{name: 'instatus'}]
                            }),
                            columns:[{
                                header: '<center><b>Código</b></center>',
                                width: 300,
                                dataIndex: 'idservico'
                            },{
                                header: '<center><b>Serviço</b></center>',
                                width: 300,
                                dataIndex: 'desservico'
                            },{
                                header: '<center><b>Descrição</b></center>',
                                width: 300,
                                dataIndex: 'desdescricao',
                                id: 'column_servicos'
                            },{
                                header: '<center><b>Status</b></center>',
                                width: 100,
                                dataIndex: 'instatus',
                                align: 'center',
                                renderer: function(value){
                                    if(value == 1){
                                        return '<img src="res/img/icons/16/action_check.png" />';
                                    }else{
                                        return '<img src="res/img/icons/16/action_delete.png" />';
                                    }
                                }
                            }],
                            listeners:{
                                rowdblclick: function(grid,row,e){

                                    var rec = grid.getStore().getAt(row);
                                    
                                    var windowIDeditServico = 'win_edit_servico';
                                    
                                    if(Ext.getCmp(windowIDeditServico)){
                                        Ext.getCmp(windowIDeditServico).show();
                                    }else{

                                        new Ext.Window({
                                            title: "Editar serviço",
                                            id: windowIDeditServico,
                                            iconCls:'ico_Essen_settings',
                                            height: 600,
                                            width: 500,
                                            //border: false,
                                            modal: true,
                                            buttonAlign: 'center',
                                            buttons:[{
                                                text: 'Editar',
                                                iconCls: 'ico_arrow_refresh',
                                                height: 30,
                                                width: 100,
                                                handler: function(){

                                                    if(Ext.getCmp(windowIDeditServico+'-form').getForm().isValid()){

                                                        Ext.Ajax.request({
                                                            url:'ajax/editServico.php',
                                                            params:{
                                                                idservico: rec.get('idservico'),
                                                                desservico: Ext.getCmp(windowIDeditServico+'-edit-desservico').getValue(),
                                                                desdescricao: Ext.getCmp(windowIDeditServico+'-edit-servico-descricao').getValue(),
                                                                instatus: Ext.getCmp(windowIDeditServico+'-edit-servicos-status').getValue().inputValue
                                                            },
                                                            success:function(){
                                                                Ext.getCmp(windowIDeditServico).close();
                                                                Ext.getCmp(windowIDAdmPaginas+'-grid-servicos').getStore().reload();
                                                                Ext.Msg.alert('Aviso','Serviço atualizado com sucesso.');
                                                            }
                                                        })
                                                    }
                                                }
                                            }],
                                            items:[{
                                                xtype: 'form',
                                                id: windowIDeditServico+'-form',
                                                padding: 10,
                                                labelWidth: 70,
                                                border: false,
                                                defaults: {
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                },
                                                items:[{
                                                    xtype: 'textfield',
                                                    id: windowIDeditServico+'-edit-desservico',
                                                    fieldLabel: 'Serviço',
                                                    value: rec.get('desservico')
                                                },{
                                                    xtype: 'textarea',
                                                    id: windowIDeditServico+'-edit-servico-descricao',
                                                    fieldLabel: 'Descri\u00e7\u00e3o',
                                                    height: 200,
                                                    value: rec.get('desdescricao')
                                                },{
                                                    xtype: 'radiogroup',
                                                    fieldLabel: 'Status',
                                                    id: windowIDeditServico+'-edit-servicos-status',
                                                    items:[
                                                        {boxLabel: 'Ativado', name: 'rd_status', inputValue: 1},
                                                        {boxLabel: 'Desativado', name: 'rd_status', inputValue: 0}
                                                    ]
                                                }]
                                            }],
                                            listeners: {
                                                'beforerender': function() {
                                                    Ext.getCmp(windowIDeditServico+'-edit-servicos-status').setValue(rec.get('instatus'));
                                                }
                                            }
                                        }).show();
                                    }
                                }
                            }
                        }]
                    },{
                        title       :  'Arquitetura e design',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-arquitetura",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-arquitetura").getForm().isValid()){
                                    var desarquivo = 'arquitetura-e-design.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-arquitetura").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'arquitetura-e-design';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'arquitetura-e-design'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-arquitetura").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-arquitetura").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-arquitetura").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-arquitetura",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-arquitetura",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Consultoria técnica',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-consultoria",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-consultoria").getForm().isValid()){
                                    var desarquivo = 'consultoria-tecnica.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-consultoria").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'consultoria-tecnica';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'consultoria-tecnica'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-consultoria").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-consultoria").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-consultoria").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-consultoria",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-consultoria",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Sustentabilidade',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-sustentabilidade",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-sustentabilidade").getForm().isValid()){
                                    var desarquivo = 'sustentabilidade.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-sustentabilidade").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'sustentabilidade';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'sustentabilidade'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-sustentabilidade").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-sustentabilidade").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-sustentabilidade").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-sustentabilidade",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-sustentabilidade",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Gerenciamento de obra',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-gerenciamento",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-gerenciamento").getForm().isValid()){
                                    var desarquivo = 'gerenciamento-de-obra.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-gerenciamento").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'gerenciamento-de-obra';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'gerenciamento-de-obra'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-gerenciamento").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-gerenciamento").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-gerenciamento").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-gerenciamento",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                        enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-gerenciamento",
                                name      : "desconteudo",
                            }]
                        }]
                    },{
                        title   :  'Maquete eletrónica',
                        autoScroll  : true,
                        id          : windowIDAdmPaginas+"-tab-maquete",
                        bbar        : [{
                            text:"Salvar",
                            iconCls:"ico_save",
                            handler:function(){
                                if(Ext.getCmp(windowIDAdmPaginas+"-form-maquete").getForm().isValid()){
                                    var desarquivo = 'maquete-eletronica.html';
                                    Ext.getCmp(windowIDAdmPaginas+"-form-maquete").getForm().submit({
                                        url:"ajax/editPagina.php",
                                        params:{
                                            despagina: desarquivo.toLowerCase()
                                        },
                                        success:function(){
                                            alertSuccess("Página editada com sucesso!");
                                        }
                                    });
                                }
                            }
                        }, '->', {
                            text    :'Visualizar',
                            iconCls :"ico_application_put",
                            handler :function(){
                                var pagina = 'maquete-eletronica';
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = removeAcento(pagina.toLowerCase())+'.php';
                                }
                                else{
                                    var despagina = removeAcento(pagina.toLowerCase());
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        }],
                        listeners   : {
                            activate: function(tab, eOpts) {
                                //
                                $.getJSON("json/getConteudoPagina.php",{
                                    despagina: 'maquete-eletronica'
                                },function(conteudo,b){
                                    var height = Ext.getCmp(windowIDAdmPaginas+"-tab-maquete").getHeight();
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-maquete").setHeight(height - 40);
                                    Ext.getCmp(windowIDAdmPaginas+"-editor-maquete").setValue(conteudo);
                                });
                            }
                        },
                        items: [{
                            xtype   : "form",
                            id      : windowIDAdmPaginas+"-form-maquete",
                            border  : false,
                            items:[{
                                hideLabel : true,
                                anchor    : "100%",
                                xtype     : "htmleditor",
                                enableFont  : false,
                                enableAlignments : false,
                                id        : windowIDAdmPaginas+"-editor-maquete",
                                name      : "desconteudo",
                            }]
                        }]
                    }]
                }] 

            },{
                title: 'Galeria',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-galeria",
                layout      : 'border',
                bbar        : [{
                    text:"Adicionar Categoria",
                    iconCls:"ico_add",
                    handler:function(btn){
                        windowAddCategoria(0, false);
                    }
                }, '->', {
                    text    : 'Adicionar foto',
                    iconCls : 'ico_arrow_top',
                    handler : function () {
                        new Ext.Window({
                            title   : 'Adicionar foto',
                            width   : 600,
                            height  : 140,
                            id      : 'window-galeria-foto',
                            modal   : true,
                            layout  : 'fit',
                            iconCls : 'ico_arrow_top',
                            items   : [{
                                xtype: 'form',
                                id: 'upload-form-galeria-foto',
                                fileUpload: true,
                                border: false,
                                multiple: true,
                                width: 500,
                                autoHeight: true,
                                bodyStyle: 'padding: 10px 10px 10px 10px;',
                                labelWidth: 50,
                                defaults: {
                                    anchor: '95%',
                                    allowBlank: false,
                                    msgTarget: 'side'
                                },
                                items:[{
                                    xtype: 'combo',
                                    id: 'combo-categorias',
                                    typeAhead: true,
                                    triggerAction: 'all',
                                    lazyRender: true,
                                    store: combo_categorias,
                                    valueField: 'idcategoria',
                                    displayField: 'descategoria',
                                    mode: 'local',
                                    name: 'descategoria',
                                    fieldLabel: 'Categoria',
                                    forceSelection: true
                                },{
                                    xtype: 'fileuploadfield',
                                    id: 'upload_file-galeria-foto',
                                    name: 'arquivo[]',
                                    multiple: true,
                                    emptyText: 'selecione uma imagem para fazer o upload...',
                                    fieldLabel: 'Arquivo',
                                    buttonText: 'Buscar'
                                }],
                                buttons: [{
                                    text: 'Upload',
                                    iconCls : 'ico_arrow_top',
                                    handler: function(){
                                        if(Ext.getCmp('upload-form-galeria-foto').getForm().isValid()){
                                                
                                            Ext.getCmp('upload-form-galeria-foto').getForm().submit({
                                                url: 'ajax/upload_file.php',
                                                params : {
                                                    upload_url: '../../res/img/galeria/',
                                                    tipoupload: 'foto',
                                                    idcategoria: Ext.getCmp('combo-categorias').getValue()
                                                },
                                                waitMsg: 'Salvando o arquivo...',
                                                success: function(form,action){
                                                    Ext.getCmp('upload-form-galeria-foto').getForm().reset();
                                                    
                                                    alertSuccess('Foto cadastrada com sucesso');
                                                }
                                            });                                
                                        }
                                    }
                                }]
                            }],
                            listeners: {
                                'afterrender':function (){
                                    if (idcategoria > 0) {
                                        Ext.getCmp('combo-categorias').setValue(idcategoria);                                        
                                    }
                                }
                            }
                        }).show();
                    }
                }, {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'galeria';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                items       : [{
                    region: 'west',
                    id: windowIDAdmPaginas+'galeria-categorias', // see Ext.getCmp() below
                    autoScroll  : true,
                    xtype: 'panel',
                    split: true,                   
                    title: 'Categorias',
                    collapsible: true,
                    width: 240,
                    minWidth: 240,
                    maxWidth: 240,
                    collapsible: true,
                    animCollapse: true,
                    padding: 10,
                    margins: '0 0 0 0'
                }, {
                    region      : 'center',
                    xtype       : 'panel',
                    id: windowIDAdmPaginas+'galeria-fotos', // see Ext.getCmp() below
                    autoScroll  : true,
                    split: true,
                    title: 'Fotos',
                    items       : [{}]
                }],
                listeners : {
                    activate : function() {
                        loadCategorias();
                    }
                }
            },/*{
                title: 'Contato'
            },*/{
                title       : 'FAQ',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-faq",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(btn){
                        loadInBtn(btn);
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-faq").getForm().isValid()){
                            var desarquivo = 'FAQ.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-faq").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    loadInBtn(btn);
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                        else {
                            loadInBtn(btn);
                        }
                    }
                }, '->', {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'FAQ';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = pagina.toLowerCase()+'.php';
                        }
                        else{
                            var despagina = pagina.toLowerCase();
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'FAQ'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-faq").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+"-editor-faq").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-faq").setValue(conteudo);
                        });
                    }
                },
                items: [{
                    xtype   : "form",
                    id      : windowIDAdmPaginas+"-form-faq",
                    border  : false,
                    items:[{
                        hideLabel : true,
                        anchor    : "100%",
                        xtype     : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        id        : windowIDAdmPaginas+"-editor-faq",
                        name      : "desconteudo",
                    }]
                }]
            },{
                title       : 'Política de privacidade',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-politicas",
                bbar        : [{
                    text:"Salvar",
                    iconCls:"ico_save",
                    handler:function(){
                        if(Ext.getCmp(windowIDAdmPaginas+"-form-politicas").getForm().isValid()){
                            var desarquivo = 'politica-de-privacidade.html';
                            Ext.getCmp(windowIDAdmPaginas+"-form-politicas").getForm().submit({
                                url:"ajax/editPagina.php",
                                params:{
                                    despagina: desarquivo.toLowerCase()
                                },
                                success:function(){
                                    alertSuccess("Página editada com sucesso!");
                                }
                            });
                        }
                    }
                }, '->', {
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'politica-de-privacidade';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = removeAcento(pagina.toLowerCase())+'.php';
                        }
                        else{
                            var despagina = removeAcento(pagina.toLowerCase());
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'politica-de-privacidade'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-politicas").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setHeight(height);
                            Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setValue(conteudo);
                        });
                    }
                },
                items: [{
                    xtype   : "form",
                    id      : windowIDAdmPaginas+"-form-politicas",
                    border  : false,
                    items:[{
                        hideLabel : true,
                        anchor    : "100%",
                        xtype     : "htmleditor",
                        enableFont  : false,
                        enableAlignments : false,
                        id        : windowIDAdmPaginas+"-editor-politicas",
                        name      : "desconteudo",
                    }]
                }]
            },{
                title       : 'Nossos Planos',
                autoScroll  : true,
                id          : windowIDAdmPaginas+"-tab-nossos-planos",
                bbar: ['Filtrar:', {
                    xtype:"combo",
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'tipo'
                        ],
                        data: [[1, 'Ativo'], [2, 'Inativo']]
                    }),
                    valueField: 'id',
                    displayField: 'tipo',
                    listeners: {
                        select: function (obj, record) {
                            if(record.get("id")==1){
                                Ext.getCmp(windowIDAdmPaginas + '-grid-nossos-planos').getStore().filter('instatus', 1);
                            } else {
                                Ext.getCmp(windowIDAdmPaginas + '-grid-nossos-planos').getStore().filter('instatus', 0);
                            }
                        }
                    }
                }, '-', {
                    // text:"Salvar",
                    // iconCls:"ico_save",
                    // handler:function(){
                    //     if(Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getForm().isValid()){
                    //         var desarquivo = 'design-online.html';
                    //         Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getForm().submit({
                    //             url:"ajax/editPagina.php",
                    //             params:{
                    //                 despagina: desarquivo.toLowerCase()
                    //             },
                    //             success:function(){
                    //                 alertSuccess("Página editada com sucesso!");
                    //             }
                    //         });
                    //     }
                    // }
                    text    : 'Adicionar Plano',
                    iconCls : 'ico_add',
                    handler : function () {

                        var windowIDaddPlano = 'win_add_plano';
                        
                        if(Ext.getCmp(windowIDaddPlano)){
                            Ext.getCmp(windowIDaddPlano).show();
                        }else{

                            new Ext.Window({
                                title: "Adicionar Plano",
                                id: windowIDaddPlano,
                                iconCls:'ico_Essen_product_add',
                                height: 550,
                                width: 650,
                                //border: false,
                                modal: true,
                                buttonAlign: 'center',
                                buttons:[{
                                    text: 'Cadastrar',
                                    iconCls: 'ico_save',
                                    height: 30,
                                    width: 100,
                                    handler: function(){

                                        if(Ext.getCmp(windowIDaddPlano+'-form').getForm().isValid()){

                                            Ext.Ajax.request({
                                                url:'ajax/addNossosPlanos.php',
                                                params:{
                                                    titulo: Ext.getCmp(windowIDaddPlano+'-add-plano-titulo').getValue(),
                                                    valor: Ext.getCmp(windowIDaddPlano+'-add-plano-valor').getValue(),
                                                    descricao: Ext.getCmp(windowIDaddPlano+'-add-plano-descricao').getValue(),
                                                    informacao: Ext.getCmp(windowIDaddPlano+'-add-plano-informacao').getValue(),
                                                },
                                                success:function(){

                                                    Ext.getCmp(windowIDaddPlano+'-form').getForm().reset();
                                                    Ext.getCmp(windowIDaddPlano).close();
                                                    Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                    Ext.Msg.alert('Aviso','Plano cadastrado com sucesso.');
                                                }
                                            })
                                        }
                                    }
                                }],
                                items:[{
                                    xtype: 'form',
                                    id: windowIDaddPlano+'-form',
                                    padding: 10,
                                    labelWidth: 70,
                                    border: false,
                                    defaults: {
                                        allowBlank: false,
                                        anchor: '100%'
                                    },
                                    items:[{
                                        xtype: 'textfield',
                                        id: windowIDaddPlano+'-add-plano-titulo',
                                        fieldLabel: 'T\u00edtulo'
                                    },{
                                        xtype: 'numberfield',
                                        id: windowIDaddPlano+'-add-plano-valor',
                                        fieldLabel: 'Valor',
                                        decimalSeparator: ','
                                    },{
                                        xtype: "htmleditor",
                                        enableFont: false,
                                        enableAlignments: false,
                                        id: windowIDaddPlano+'-add-plano-descricao',
                                        fieldLabel: 'Descri\u00e7\u00e3o',
                                        height: 200
                                    },{
                                        xtype: "htmleditor",
                                        enableFont: false,
                                        enableAlignments: false,
                                        id: windowIDaddPlano+'-add-plano-informacao',
                                        fieldLabel: 'Mais Informa\u00e7\u00e3o',
                                        height: 200,
                                        allowBlank: true
                                    }]
                                }]
                            }).show();
                        }
                    }
                },'-','->','-',{
                    text    :'Visualizar',
                    iconCls :"ico_application_put",
                    handler :function(){
                        var pagina = 'design-online';
                        if (SITE_PATH.indexOf('localhost')){
                            var despagina = removeAcento(pagina.toLowerCase())+'.php';
                        }
                        else{
                            var despagina = removeAcento(pagina.toLowerCase());
                        }
                        window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                    }
                }],
                listeners   : {
                    activate: function(tab, eOpts) {
                        //
                        $.getJSON("json/getConteudoPagina.php",{
                            despagina: 'design-online'
                        },function(conteudo,b){
                            var height = Ext.getCmp(windowIDAdmPaginas+"-tab-nossos-planos").getHeight();
                            Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').setHeight(height);
                            // Ext.getCmp(windowIDAdmPaginas+"-editor-politicas").setValue(conteudo);
                        });
                    }
                },
                items:[{
                    xtype: 'grid',
                    id: windowIDAdmPaginas+'-grid-nossos-planos',
                    loadMask: true,
                    height: 320,
                    autoExpandColumn: 'column_planos_descricao',
                    store: new Ext.data.JsonStore({
                        url: 'json/listNossosPlanos.php',
                        root: 'myData',
                        autoLoad: true,
                        fields:[{name: 'idplano'},{name: 'desplano'},{name: 'vlplano'},{name: 'instatus'},{name: 'descricao'},{name: 'desinformacao'},'nrposicao']
                    }),
                    columns:[{
                        header: '<center><b>Plano</b></center>',
                        width: 300,
                        dataIndex: 'desplano'
                    },{
                        header: '<center><b>Valor</b></center>',
                        width: 250,
                        dataIndex: 'vlplano',
                        renderer: Ext.util.Format.brMoney,
                    },{
                        header: '<center><b>Descrição</b></center>',
                        id: 'column_planos_descricao',
                        dataIndex: 'descricao',
                    },{
                        header: '<center><b>Mais Informação</b></center>',
                        width: 600,
                        dataIndex: 'desinformacao',
                    },{
                        header: '<center><b>Status</b></center>',
                        width: 100,
                        dataIndex: 'instatus',
                        align: 'center',
                        renderer: function(value){
                            if(value == 1){
                                return '<img src="res/img/icons/16/action_check.png" />';
                            }else{
                                return '<img src="res/img/icons/16/action_delete.png" />';
                            }
                        }
                    }],
                    listeners:{
                        rowdblclick: function(grid,row,e){
                            
                            var windowIDeditPlano = 'win_edit_plano';
                            
                            if(Ext.getCmp(windowIDeditPlano)){
                                Ext.getCmp(windowIDeditPlano).show();
                            }else{

                                new Ext.Window({
                                    title: "Editar Plano",
                                    id: windowIDeditPlano,
                                    iconCls:'ico_Essen_product_edit',
                                    height: 600,
                                    width: 600,
                                    //border: false,
                                    modal: true,
                                    buttonAlign: 'center',
                                    buttons:[{
                                        text: 'Editar',
                                        iconCls: 'ico_arrow_refresh',
                                        height: 30,
                                        width: 100,
                                        handler: function(){

                                            if(Ext.getCmp(windowIDeditPlano+'-form').getForm().isValid()){

                                                Ext.Ajax.request({
                                                    url:'ajax/editNossosPlanos.php',
                                                    params:{
                                                        idplano: Ext.getCmp(windowIDeditPlano+'-edit-plano-idplano').getValue(),
                                                        titulo: Ext.getCmp(windowIDeditPlano+'-edit-plano-titulo').getValue(),
                                                        nrposicao: Ext.getCmp(windowIDeditPlano + '-edit-nrposicao').getValue(),
                                                        valor: Ext.getCmp(windowIDeditPlano + '-edit-plano-valor').getValue(),
                                                        descricao: Ext.getCmp(windowIDeditPlano+'-edit-plano-descricao').getValue(),
                                                        informacao: Ext.getCmp(windowIDeditPlano+'-edit-plano-informacao').getValue(),
                                                        instatus: Ext.getCmp(windowIDeditPlano+'-edit-plano-status').getValue().inputValue,
                                                    },
                                                    success:function(){
                                                        Ext.getCmp(windowIDeditPlano).close();
                                                        Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                        Ext.Msg.alert('Aviso','Plano atualizado com sucesso.');
                                                    }
                                                })
                                            }
                                        }
                                    }],
                                    listeners:{
                                        afterrender:function(){

                                            var rec = grid.getStore().getAt(row);

                                            Ext.getCmp(windowIDeditPlano + '-edit-nrposicao').setValue(rec.data.nrposicao);
                                            Ext.getCmp(windowIDeditPlano + '-edit-plano-idplano').setValue(rec.data.idplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-titulo').setValue(rec.data.desplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-valor').setValue(rec.data.vlplano);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-descricao').setValue(rec.data.descricao);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-informacao').setValue(rec.data.desinformacao);
                                            Ext.getCmp(windowIDeditPlano+'-edit-plano-status').setValue(rec.data.instatus);
                                        }
                                    },
                                    items:[{
                                        xtype: 'form',
                                        id: windowIDeditPlano+'-form',
                                        padding: 10,
                                        labelWidth: 70,
                                        border: false,
                                        defaults: {
                                            allowBlank: false,
                                            anchor: '100%'
                                        },
                                        items:[{
                                            xtype: 'hidden',
                                            id: windowIDeditPlano+'-edit-plano-idplano',
                                        },{
                                            xtype: 'textfield',
                                            id: windowIDeditPlano+'-edit-plano-titulo',
                                            fieldLabel: 'T\u00edtulo'
                                        },{
                                            xtype: 'numberfield',
                                            id: windowIDeditPlano+'-edit-plano-valor',
                                            fieldLabel: 'Valor',
                                            decimalSeparator: ',',
                                        }, {
                                            xtype: 'numberfield',
                                            id: windowIDeditPlano + '-edit-nrposicao',
                                            fieldLabel: 'Posição',
                                            decimalSeparator: ',',
                                        }, {
                                            xtype: "htmleditor",
                                            enableFont: false,
                                            enableAlignments: false,
                                            id: windowIDeditPlano+'-edit-plano-descricao',
                                            fieldLabel: 'Descri\u00e7\u00e3o',
                                            height: 200
                                        },{
                                            xtype: "htmleditor",
                                            enableFont: false,
                                            enableAlignments: false,
                                            id: windowIDeditPlano+'-edit-plano-informacao',
                                            fieldLabel: 'Mais Informa\u00e7\u00e3o',
                                            height: 200,
                                            allowBlank: true
                                        },{
                                            xtype: 'radiogroup',
                                            fieldLabel: 'Status',
                                            id: windowIDeditPlano+'-edit-plano-status',
                                            items:[
                                                {boxLabel: 'Ativado', name: 'rd_status', inputValue: 1},
                                                {boxLabel: 'Desativado', name: 'rd_status', inputValue: 0}
                                            ]
                                        }]
                                    }]
                                }).show();
                            }
                        },
                        rowcontextmenu: function (obj, rowIndex, e) {
                            e.stopEvent();
                            obj.getSelectionModel().selectRow(rowIndex);
                            //
                            new Ext.menu.Menu({
                                items: [{
                                    text: 'Excluir Plano',
                                    iconCls: 'ico_action_delete',
                                    handler: function () {
                                        if (Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getSelectionModel().hasSelection()) {
                                            var row = Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getSelectionModel().getSelected();

                                            Ext.Ajax.request({
                                                url: 'ajax/removePlano.php',
                                                params: {
                                                    idplano: row.get('idplano')
                                                },
                                                success: function (a) {
                                                    var json = $.parseJSON(a.responseText);
                                                    if (json.success) {
                                                        Ext.getCmp(windowIDAdmPaginas+'-grid-nossos-planos').getStore().reload();
                                                        Ext.Msg.alert('Aviso', 'O plano foi removido com sucesso.');
                                                    } else {
                                                        Ext.Msg.alert('Aviso', json.msg);
                                                    }

                                                }
                                            });

                                        } else {
                                            Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                                        }
                                    }
                                }]
                            }).showAt(e.xy);
                        }
                    }
                }]
            }]
        }]
    }).show();
}
var windowIDPedidos = "win.pedidos";

if(Ext.getCmp(windowIDPedidos)){
    Ext.getCmp(windowIDPedidos).show();
} else {


    new Ext.Window({
        title: 'Pedidos',
        width:900,
        height:600,
        id: windowIDPedidos + "-tab-pedidos",
        layout: 'border',
        plain: true,
        listeners: {
            activate: function (tab, eOpts) {
                //
                $.getJSON("json/getConteudoPagina.php", {
                    despagina: 'design-online-step1'
                }, function (conteudo, b) {
                    var height = Ext.getCmp(windowIDPedidos + "-tab-pedidos").getHeight() - 25;
                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').setHeight(height);
                });
            }
        },
        items: [{
            region: 'west',
            width: 200,
            split: true,
            title: 'Filtros',
            items: [{
                xtype: 'form',
                id: windowIDPedidos + "-form-pedidos",
                padding: 10,
                border: false,
                items: [{
                    xtype: 'fieldset',
                    title: 'Data Cadastro Pedido',
                    labelWidth: 30,
                    defaults: {
                        anchor: "100%",
                        //allowBlank:false
                    },
                    items: [{
                        xtype: "datefield",
                        fieldLabel: "De",
                        id: windowIDPedidos + "-de-pedidos",
                        value: new Date(),
                        listeners: {
                            change: function () {
                                Ext.getCmp(windowIDPedidos + "-num-pedidos").setValue('');
                                Ext.getCmp(windowIDPedidos + "-cliente-pedidos").setValue('');
                            }
                        }
                    }, {
                        xtype: "datefield",
                        fieldLabel: "At\u00e9",
                        id: windowIDPedidos + "-ate-pedidos",
                        value: new Date(),
                        listeners: {
                            change: function () {
                                Ext.getCmp(windowIDPedidos + "-num-pedidos").setValue('');
                                Ext.getCmp(windowIDPedidos + "-cliente-pedidos").setValue('');
                            }
                        }
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Pedido',
                    labelWidth: 60,
                    defaults: {
                        anchor: "100%",
                    },
                    items: [{
                        xtype: 'numberfield',
                        id: windowIDPedidos + "-num-pedidos",
                        fieldLabel: 'N Pedido',
                        listeners: {
                            focus: function () {
                                Ext.getCmp(windowIDPedidos + "-cliente-pedidos").setValue('');
                            }
                        }
                    }, {
                        xtype: 'textfield',
                        id: windowIDPedidos + "-cliente-pedidos",
                        fieldLabel: 'Cliente',
                        listeners: {
                            focus: function () {
                                Ext.getCmp(windowIDPedidos + "-num-pedidos").setValue('');
                            }
                        }
                    }]
                }, {
                    xtype: 'button',
                    text: 'Pesquisar',
                    iconCls: 'ico_Essen_search',
                    width: 150,
                    height: 30,
                    style: 'margin: 25px 0 0 5px',
                    handler: function () {

                        Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().load({
                            params: {
                                de: Ext.getCmp(windowIDPedidos + "-de-pedidos").getValue().format('Y-m-d') + ' 00:00:00',
                                ate: Ext.getCmp(windowIDPedidos + "-ate-pedidos").getValue().format('Y-m-d') + ' 23:59:59',
                                num_pedido: Ext.getCmp(windowIDPedidos + "-num-pedidos").getValue(),
                                cliente: Ext.getCmp(windowIDPedidos + "-cliente-pedidos").getValue(),
                            },
                        });
                    }
                }]
            }]
        }, {
            region: 'center',
            title: 'Pedidos',
            bbar: ['<b>Qtd Pedidos:</b>', {
                id: 'qtd_pedidos',
                text: '0',
                // },'-','<b>Valor Total Pago:</b>',{
                //     id: 'vltotal_pago_pedidos',
                //     text: 'R$ 0,00',
                // },'-','<b>Valor Total N�o Pago:</b>',{
                //     id: 'vltotal_nao_pago_pedidos',
                //     text: 'R$ 0,00',
            }, '-', '->', '-', {
                text: 'Cancelar Pedido',
                iconCls: 'ico_action_delete',
                handler: function () {

                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();

                        Ext.Ajax.request({
                            url: 'ajax/confirmaPagamentoPedido.php',
                            params: {
                                idpedido: row.get('idpedido'),
                                idpagamento: row.get('idpagamento'),
                                idstatus: 7
                            },
                            success: function (a) {
                                var json = $.parseJSON(a.responseText);
                                if (json.success) {
                                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                    Ext.Msg.alert('Aviso', 'O pedido foi definido como em andamento sucesso.');
                                } else {
                                    Ext.Msg.alert('Aviso', json.msg);
                                }

                            }
                        });
                    } else {
                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                    }


                }
            },{
                text: 'Confirmar Pagamento',
                iconCls: 'ico_Essen_check',
                handler: function () {

                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();
                        if (row.get('idstatus') != 3 && row.get('idstatus') != 4) {

                            Ext.Ajax.request({
                                url: 'ajax/confirmaPagamentoPedido.php',
                                params: {
                                    idpedido: row.get('idpedido'),
                                    idpagamento: row.get('idpagamento'),
                                    idstatus: 3
                                },
                                success: function (a) {
                                    var json = $.parseJSON(a.responseText);
                                    if (json.success) {
                                        Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                        Ext.Msg.alert('Aviso', 'O pedido foi Confirmado com sucesso.');
                                    } else {
                                        Ext.Msg.alert('Aviso', json.msg);
                                    }

                                }
                            });
                        } else {
                            Ext.Msg.alert('Aviso', 'O pedido já está Confirmado ou Cancelado.');
                        }
                    } else {
                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                    }
                }
            }, {
                text: 'Cancelar Pagamento',
                iconCls: 'ico_Essen_check',
                handler: function () {

                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();
                        //if(row.get('idstatus') != 3 && row.get('idstatus') != 4){

                        Ext.Ajax.request({
                            url: 'ajax/confirmaPagamentoPedido.php',
                            params: {
                                idpedido: row.get('idpedido'),
                                idpagamento: row.get('idpagamento'),
                                idstatus: 4
                            },
                            success: function (a) {
                                var json = $.parseJSON(a.responseText);
                                if (json.success) {
                                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                    Ext.Msg.alert('Aviso', 'O pagamento foi cancelado com sucesso.');
                                } else {
                                    Ext.Msg.alert('Aviso', json.msg);
                                }

                            }
                        });
                        //}else{
                        //    Ext.Msg.alert('Aviso','O pedido j� est� Confirmado ou Cancelado.');
                        //}
                    } else {
                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                    }
                }
            }, {
                text: 'Confirmar Entrega',
                iconCls: 'ico_Essen_check',
                handler: function () {

                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();
                        //if(row.get('idstatus') != 3 && row.get('idstatus') != 4){

                        Ext.Ajax.request({
                            url: 'ajax/confirmaPagamentoPedido.php',
                            params: {
                                idpedido: row.get('idpedido'),
                                idpagamento: row.get('idpagamento'),
                                idstatus: 5
                            },
                            success: function (a) {
                                var json = $.parseJSON(a.responseText);
                                if (json.success) {
                                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                    Ext.Msg.alert('Aviso', 'O pedido foi Confirmado para entrega com sucesso.');
                                } else {
                                    Ext.Msg.alert('Aviso', json.msg);
                                }

                            }
                        });
                        //}else{
                        //    Ext.Msg.alert('Aviso','O pedido j� est� Confirmado ou Cancelado.');
                        //}
                    } else {
                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                    }
                }
            }, {
                text: 'Em andamento',
                iconCls: 'ico_Essen_check',
                handler: function () {

                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();

                        Ext.Ajax.request({
                            url: 'ajax/confirmaPagamentoPedido.php',
                            params: {
                                idpedido: row.get('idpedido'),
                                idpagamento: row.get('idpagamento'),
                                idstatus: 6
                            },
                            success: function (a) {
                                var json = $.parseJSON(a.responseText);
                                if (json.success) {
                                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                    Ext.Msg.alert('Aviso', 'O pedido foi definido como em andamento sucesso.');
                                } else {
                                    Ext.Msg.alert('Aviso', json.msg);
                                }

                            }
                        });
                    } else {
                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                    }
                }
            }],
            items: [{
                xtype: 'grid',
                id: windowIDPedidos + '-grid-pedidos',
                loadMask: true,
                height: 320,
                autoExpandColumn: 'column_pedidos_pessoa',
                store: new Ext.data.JsonStore({
                    listeners: {
                        load: function (a, b) {

                            Ext.getCmp('qtd_pedidos').setText(this.getTotalCount());

                            // var total = 0;

                            // Ext.each(this.data.items,function(a,b){
                            //     if(a.get('idstatus') == 3) total = parseFloat(a.get('vlpedido')) + parseFloat(total);
                            // });

                            // Ext.getCmp('vltotal_pago_pedidos').setText((total != '0')?'R$ '+total:'0');
                        }
                    },
                    url: 'json/listPedidos.php',
                    root: 'myData',
                    autoLoad: true,
                    fields: [
                        {name: 'idpedido', type: "int"},
                        {name: 'idpagamento', type: "int"},
                        {name: 'vlpedido', type: "float"},
                        {name: 'despessoa', type: "string"},
                        {name: 'desplano', type: "string"},
                        {name: 'dtcadastro', type: 'date', dateFormat: 'timestamp'},
                        {name: 'dtpagamento', type: 'date', dateFormat: 'timestamp'},
                        {name: 'idstatus', type: "int"},
                        {name: 'desstatus', type: "string"},
                    ]
                }),
                sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
                columns: [{
                    header: '<center><b>Pedido</b></center>',
                    width: 30,
                    dataIndex: 'idpedido',
                    sortable: true
                }, {
                    header: '<center><b>Cliente</b></center>',
                    id: 'column_pedidos_pessoa',
                    width: 110,
                    dataIndex: 'despessoa',
                    sortable: true
                }, {
                    header: '<center><b>Valor Total</b></center>',
                    width: 70,
                    dataIndex: 'vlpedido',
                    renderer: Ext.util.Format.brMoney,
                    sortable: true
                }, {
                    header: '<center><b>Plano</b></center>',
                    width: 170,
                    dataIndex: 'desplano',
                    sortable: true
                }, {
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    header: '<center><b>Data Cadastro</b></center>',
                    width: 100,
                    dataIndex: 'dtcadastro',
                    sortable: true
                }, {
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    header: '<center><b>Data Pagamento</b></center>',
                    width: 100,
                    dataIndex: 'dtpagamento',
                    sortable: true
                }, {
                    header: '<center><b>Status</b></center>',
                    width: 120,
                    dataIndex: 'desstatus',
                    align: 'center',
                    sortable: true
                }],
                listeners: {
                    rowdblclick: function (grid, row, e) {

                        var windowIDPedido = 'win_pedido';

                        if (Ext.getCmp(windowIDPedido)) {
                            Ext.getCmp(windowIDPedido).show();
                        } else {

                            new Ext.Window({
                                title: "Pedido Numero " + grid.getStore().getAt(row).data.idpedido,
                                id: windowIDPedido,
                                iconCls: '',
                                height: 600,
                                width: 700,
                                autoScroll: true,
                                //border: false,
                                modal: true,
                                listeners: {
                                    afterrender: function () {

                                        Ext.Ajax.request({
                                            url: 'json/getPedido.php',
                                            params: {
                                                idpedido: grid.getStore().getAt(row).data.idpedido,
                                            },
                                            success: function (a) {

                                                var rec_pedido = $.parseJSON(a.responseText);
                                                var dtcadastro = dt = "";

                                                if(rec_pedido.pedido[0].dtcadastro) dtcadastro = new Date(rec_pedido.pedido[0].dtcadastro*1000)
                                                if(rec_pedido.pedido[0].dtcadastro) dt = dtcadastro.getDate() + '/' + dtcadastro.getMonth() + "/" + dtcadastro.getFullYear();

                                                Ext.getCmp(windowIDPedido + '-nome-pedido').setValue(rec_pedido.pedido[0].despessoa);
                                                Ext.getCmp(windowIDPedido + '-email-pedido').setValue(rec_pedido.pedido[0].email);
                                                Ext.getCmp(windowIDPedido + '-tel-pedido').setValue(rec_pedido.pedido[0].telefone);
                                                Ext.getCmp(windowIDPedido + '-cel-pedido').setValue(rec_pedido.pedido[0].celular);
                                                Ext.getCmp(windowIDPedido + '-cpf-pedido').setValue(rec_pedido.pedido[0].cpf);
                                                Ext.getCmp(windowIDPedido + '-rg-pedido').setValue(rec_pedido.pedido[0].rg);
                                                Ext.getCmp(windowIDPedido + '-end-pedido').setValue(rec_pedido.pedido[0].desendereco);
                                                Ext.getCmp(windowIDPedido + '-num-pedido').setValue(rec_pedido.pedido[0].desnumero);
                                                Ext.getCmp(windowIDPedido + '-comp-pedido').setValue(rec_pedido.pedido[0].descomplemento);
                                                Ext.getCmp(windowIDPedido + '-cep-pedido').setValue(rec_pedido.pedido[0].descep);
                                                Ext.getCmp(windowIDPedido + '-bairro-pedido').setValue(rec_pedido.pedido[0].desbairro);
                                                Ext.getCmp(windowIDPedido + '-cidade-pedido').setValue(rec_pedido.pedido[0].descidade);
                                                Ext.getCmp(windowIDPedido + '-uf-pedido').setValue(rec_pedido.pedido[0].desestado);
                                                Ext.getCmp(windowIDPedido + '-idpedido-pedido').setValue(rec_pedido.pedido[0].idpedido);
                                                Ext.getCmp(windowIDPedido + '-vlpedido-pedido').setValue('R$ ' + rec_pedido.pedido[0].vlpedido);
                                                Ext.getCmp(windowIDPedido + '-dtcadastro-pedido').setValue(dt);
                                                Ext.getCmp(windowIDPedido + '-plano-pedido').setValue(rec_pedido.pedido[0].desplano);
                                                Ext.getCmp(windowIDPedido + '-formapgto-pedido').setValue(rec_pedido.pedido[0].desformapagamento);
                                                Ext.getCmp(windowIDPedido + '-dtpgto-pedido').setValue(rec_pedido.pedido[0].dtpagamento);
                                                Ext.getCmp(windowIDPedido + '-status-pedido').setValue(rec_pedido.pedido.desstatus);

                                                var html_pedido = '';
                                                var cont_ambiente = 1;
                                                $.each(rec_pedido.ambiente, function (a, b) {

                                                    if (b.desarquivo != '' && b.desarquivo != null) {
                                                        var img = '<img class="img_pedido" style="width: 300px" src="' + SITE_PATH + 'res/img/pedido/' + b.idpedido + '/' + b.desarquivo + '" />';
                                                    } else {
                                                        var img = 'sem imagem';
                                                    }

                                                    html_pedido += '<h3 style="margin-bottom: 10px">Ambiente ' + cont_ambiente + ':</h3>';
                                                    html_pedido += '<div style="margin: 0 0 20px 30px">';
                                                    html_pedido += '<p style="margin-bottom: 10px"><b>Ambiente:</b> ' + b.desambiente + '</p>';
                                                    html_pedido += '<p style="margin-bottom: 10px"><b>Metragem:</b> ' + b.desmetragem + '</p>';
                                                    html_pedido += '<p style="margin-bottom: 10px"><b>Obeservação:</b> ' + b.desobservacao + '</p>';
                                                    html_pedido += '<p style="margin-bottom: 5px"><b>Imagem:</b></p>';
                                                    html_pedido += img;
                                                    html_pedido += '</div>';

                                                    cont_ambiente++;
                                                });

                                                Ext.getCmp(windowIDPedido + '-ambiente-pedido').setValue(html_pedido);
                                            }
                                        });
                                    }
                                },
                                items: [{
                                    xtype: 'form',
                                    id: windowIDPedido + '-form',
                                    padding: 10,
                                    labelWidth: 130,
                                    border: false,
                                    items: [{
                                        xtype: 'fieldset',
                                        title: 'Cliente',
                                        defaults: {
                                            xtype: 'displayfield',
                                            anchor: '100%'
                                        },
                                        items: [{
                                            id: windowIDPedido + '-nome-pedido',
                                            fieldLabel: '<b>Nome</b>'
                                        }, {
                                            id: windowIDPedido + '-email-pedido',
                                            fieldLabel: '<b>Email</b>'
                                        }, {
                                            id: windowIDPedido + '-tel-pedido',
                                            fieldLabel: '<b>Telefone</b>'
                                        }, {
                                            id: windowIDPedido + '-cel-pedido',
                                            fieldLabel: '<b>Celular</b>'
                                        }, {
                                            id: windowIDPedido + '-cpf-pedido',
                                            fieldLabel: '<b>CPF</b>'
                                        }, {
                                            id: windowIDPedido + '-rg-pedido',
                                            fieldLabel: '<b>RG</b>'
                                        }, {
                                            id: windowIDPedido + '-end-pedido',
                                            fieldLabel: '<b>Endereço</b>'
                                        }, {
                                            id: windowIDPedido + '-num-pedido',
                                            fieldLabel: '<b>Número</b>'
                                        }, {
                                            id: windowIDPedido + '-comp-pedido',
                                            fieldLabel: '<b>Complemento</b>'
                                        }, {
                                            id: windowIDPedido + '-cep-pedido',
                                            fieldLabel: '<b>CEP</b>'
                                        }, {
                                            id: windowIDPedido + '-bairro-pedido',
                                            fieldLabel: '<b>Bairro</b>'
                                        }, {
                                            id: windowIDPedido + '-cidade-pedido',
                                            fieldLabel: '<b>Cidade</b>'
                                        }, {
                                            id: windowIDPedido + '-uf-pedido',
                                            fieldLabel: '<b>Estado</b>'
                                        }]
                                    }, {
                                        xtype: 'fieldset',
                                        title: 'Pedido',
                                        defaults: {
                                            xtype: 'displayfield',
                                            anchor: '100%'
                                        },
                                        items: [{
                                            id: windowIDPedido + '-idpedido-pedido',
                                            fieldLabel: '<b>N Pedido</b>'
                                        }, {
                                            id: windowIDPedido + '-vlpedido-pedido',
                                            fieldLabel: '<b>Valor Total</b>'
                                        }, {
                                            id: windowIDPedido + '-dtcadastro-pedido',
                                            fieldLabel: '<b>Data Cadastro</b>'
                                        }, {
                                            id: windowIDPedido + '-plano-pedido',
                                            fieldLabel: '<b>Plano</b>'
                                        }, {
                                            id: windowIDPedido + '-formapgto-pedido',
                                            fieldLabel: '<b>Forma Pagamento</b>'
                                        }, {
                                            id: windowIDPedido + '-dtpgto-pedido',
                                            fieldLabel: '<b>Data Pagamento</b>'
                                        }, {
                                            id: windowIDPedido + '-status-pedido',
                                            fieldLabel: '<b>Status</b>'
                                        }]
                                    }, {
                                        xtype: 'fieldset',
                                        title: 'Ambiente(s)',
                                        defaults: {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            hideLabel: true,
                                        },
                                        items: [{
                                            id: windowIDPedido + '-ambiente-pedido'
                                        }]
                                    }]
                                }]
                            }).show();
                        }
                    },
                    rowcontextmenu: function (obj, rowIndex, e) {
                        e.stopEvent();
                        //
                        new Ext.menu.Menu({
                            items: [{
                                text: 'Excluir Pedido',
                                iconCls: 'ico_action_delete',
                                handler: function () {
                                    if (Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().hasSelection()) {
                                        var row = Ext.getCmp(windowIDPedidos + '-grid-pedidos').getSelectionModel().getSelected();

                                        Ext.Ajax.request({
                                            url: 'ajax/cancelaPedido.php',
                                            params: {
                                                idpedido: row.get('idpedido')
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    Ext.getCmp(windowIDPedidos + '-grid-pedidos').getStore().reload();
                                                    Ext.Msg.alert('Aviso', 'O pedido foi cancelado com sucesso.');
                                                } else {
                                                    Ext.Msg.alert('Aviso', json.msg);
                                                }

                                            }
                                        });

                                    } else {
                                        Ext.Msg.alert('Aviso', 'Selecione 1 registro.');
                                    }
                                }
                            }]
                        }).showAt(e.xy);
                    }
                }
            }]
        }]
    }).show();
    /**
     * Created by BrunoBarbosa on 29/10/2015.
     */

}
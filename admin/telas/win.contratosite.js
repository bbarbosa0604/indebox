var windowContratoSite = "win.contratosite";

if (Ext.getCmp(windowContratoSite)) {
    Ext.getCmp(windowContratoSite).show()
}
else {
    
    $.getJSON("json/getContrato.php", function (data) {
        new Ext.Window({
            id: windowContratoSite,
            title: 'Contrato do Site',
            iconCls: 'ico_add',
            autoScroll: true,
            width: 700,
            height: 500,
            resizable: false,
            layout: 'fit',
            modal: true,
            items: [{
                xtype: "form",
                padding: 10,
                border:false,
                id:windowContratoSite+"-form",
                items: [{
                    xtype: "htmleditor",
                    anchor: "100%",
                    name: "descontrato",
                    hideLabel: true,
                    height: 400,
                    value: data.data,
                    allowBlank:false
                }]
            }],
            buttons: [{
                text: "Salvar",
                iconCls: "ico_save",
                handler: function () {
                    if (Ext.getCmp(windowContratoSite + "-form").getForm().isValid()) {
                        Ext.getCmp(windowContratoSite + "-form").getForm().submit({
                            url: "ajax/saveContrato.php",
                            success: function () {
                                alertSuccess("Contrato salvo com sucesso!");
                            }
                        })
                    }
                }
            }]
        }).show();

    });
}
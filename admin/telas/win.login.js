var windowIDlogin = "win.login";
//
if(Ext.getCmp(windowIDlogin)){
    Ext.getCmp(windowIDlogin).show();
} else {
    //
    new Ext.Window({
        title:"Login",
        id:windowIDlogin,
        width:250,
        height:140,
        modal:true,
        closable:false,
        plain: true,
        buttonAlign:"center",
        items:[{
            xtype:"form",
            id:windowIDlogin+"-form",
            padding: 10,
            labelWidth:60,
            border:false,
            defaults:{
                anchor:"100%",
                allowBlank:false
            },
            items:[{
                xtype:"textfield",
                fieldLabel:"Usuario",
                name:"desusuario",
                value:"adm"
            },{
                xtype:"numberfield",
                fieldLabel:"Senha",
                inputType:"password",
                name:"dessenha"
            }]
        }],
        buttons:[{
            text:"Entrar",
            iconCls:"ico_login",
            handler:function(){
                if(Ext.getCmp(windowIDlogin+"-form").getForm().isValid()){
                    Ext.getCmp(windowIDlogin+"-form").getForm().submit({
                        url:'json/getLogin.php',
                        success:function(){
                            window.location.href = 'ajax/init.php';
                        },
                        failure:function(){
                            Ext.MessageBox.alert("","Login e senha Inválidos");
                        }
                    });
                }
            }
        }]
    }).show();
}
var windowIDGerenciarPaginas = "win.gerenciar-paginas";
if(Ext.getCmp(windowIDGerenciarPaginas)){
    Ext.getCmp(windowIDGerenciarPaginas).show();
} else {
    
    myMask.show();

    var storePaginas = new Ext.data.JsonStore({
        url:"json/getPaginas.php",
        root:"myData",
        fields:['desarquivo'],
        autoLoad:true,
        listeners:{
            load:function(){
                Ext.getCmp(windowIDGerenciarPaginas+"-total").setText(this.getTotalCount());
                myMask.hide();
            }
        }
    });

    new Ext.Window({
        title:"Gerenciar Paginas",
        width: 400,
        height: 400,
        autoScroll:true,
        tbar:[{
            xtype:"textfield",
            emptyText:"Digite o nome da pagina que procura",
            width:380,
            enableKeyEvents:true,
            listeners:{
                keydown:function(obj, e){
                    storePaginas.filter('desarquivo',obj.getValue());
                }
            }
        }],
        bbar:['->','Total de paginas: ',{
            xtype:"tbtext",
            id:windowIDGerenciarPaginas+"-total",
            text:0
        }],
        items:[{
            xtype:"grid",
            store: storePaginas,
            border:false,
            autoExpandColumn:"0",
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    width: 120,
                    sortable: true
                },
                columns: [{
                    header:"Paginas",
                    dataIndex:"desarquivo"
                }]
            }),
            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
            autoWidth: true,
            autoHeight: true,
            listeners:{
                rowcontextmenu:function(obj, rowIndex, e){
                    e.stopEvent();
                    obj.getSelectionModel().selectRow(rowIndex);
                    //
                    var pagina = obj.getStore().getAt(rowIndex);
                    var conteudo = "";
                    //
                    $.getJSON("json/getConteudoPagina.php",{
                        despagina:pagina.get("desarquivo")
                    },function(a,b){
                        conteudo = a;
                    });

                    //
                    new Ext.menu.Menu({
                        items:[{
                            text:'Visualizar',
                            iconCls:"ico_application_put",
                            handler:function(){
                                if (SITE_PATH.indexOf('localhost')){
                                    var despagina = pagina.get("desarquivo")+'.php';
                                }
                                else{
                                    var despagina = pagina.get("desarquivo");
                                }
                                window.open(SITE_PATH+""+despagina.replace(".html",".php").replace(new RegExp(" ", 'g'),"-"),'_blank');
                            }
                        },{
                            text:'Alterar',
                            iconCls:"ico_pencil",
                            handler:function(){
                                new Ext.Window({
                                    title:"Editar pagina "+ pagina.get("desarquivo"),
                                    id:windowIDGerenciarPaginas+"-editar-pagina",
                                    width:700,
                                    height:400,
                                    modal:true,
                                    items:[{
                                        xtype:"form",
                                        padding:10,
                                        id:windowIDGerenciarPaginas+"-form",
                                        border:false,
                                        items:[{
                                            hideLabel:true,
                                            anchor:"100%",
                                            xtype:"htmleditor",
                                            value:conteudo,
                                            height:320,
                                            name:"desconteudo"
                                        }]
                                    }],
                                    buttons:[{
                                        text:"Salvar",
                                        iconCls:"ico_save",
                                        handler:function(){
                                            if(Ext.getCmp(windowIDGerenciarPaginas+"-form").getForm().isValid()){
                                                Ext.getCmp(windowIDGerenciarPaginas+"-form").getForm().submit({
                                                    url:"ajax/editPagina.php",
                                                    params:{
                                                        despagina:pagina.get("desarquivo")
                                                    },
                                                    success:function(){
                                                        alertSuccess("Pagina Editada com sucesso!");
                                                        Ext.getCmp(windowIDGerenciarPaginas+"-editar-pagina").close();
                                                        conteudo = "";
                                                    }
                                                });
                                            }
                                        }
                                    }]
                                }).show();
                            }
                        }]
                    }).showAt(e.xy);
                }
            }
        }],
        modal:true
    }).show();
}
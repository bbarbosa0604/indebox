var windowIDDepoimentos = "win.depoimentos";
if(Ext.getCmp(windowIDDepoimentos)){
    Ext.getCmp(windowIDDepoimentos).show();
} else {
    myMask.show();

    var store = new Ext.data.JsonStore({
        url:"json/getDepoimentos.php",
        root:"myData",
        fields: ['iddepoimento', 'desdepoimento', 'desnome', { name: "dtcadastro", type: "date", dateFormat: "timestamp" }],
        autoLoad:true,
        listeners:{
            load:function(){
                Ext.getCmp(windowIDDepoimentos+"-total").setText(this.getTotalCount());
                myMask.hide();
            }
        }
    });

    new Ext.Window({
        title:"Administrativo de Depoimentos",
        width:600,
        autoScroll:true,
        bbar:[{
            text:"Atualizar",
            iconCls:'ico_arrow_refresh',
            handler:function(){
                store.reload();
            }
        },'->','Total de depoimentos: ',{
            xtype:"tbtext",
            id:windowIDDepoimentos+"-total",
            text:0
        }],
        items:[{
            xtype:"grid",
            store: store,
            border:false,
            autoExpandColumn:"1",
            loadMask: true,
            tbar:[{
                iconCls: "ico_add",
                text: "Adicionar",
                handler: function () {
                    new Ext.Window({
                        id: windowIDDepoimentos + "-adicionar",
                        height: 300,
                        width: 500,
                        title: "Adicionar um novo depoimento",
                        modal: true,
                        items: [{
                            xtype: "form",
                            border:false,
                            id: windowIDDepoimentos + "-form",
                            padding: 10,
                            defaults:{
                                anchor:"100%",
                                allowBlank:false
                            },
                            items: [{
                                fieldLabel: "Nome",
                                xtype: "textfield",
                                name:"desnome"
                            }, {
                                fieldLabel: "Depoimento",
                                xtype: "textarea",
                                name: "desdepoimento",
                                height:170
                            }]
                        }],
                        buttons: [{
                            text: "Salvar",
                            iconCls: "ico_save",
                            handler: function () {
                                if (Ext.getCmp(windowIDDepoimentos + "-form").getForm().isValid()) {
                                    Ext.getCmp(windowIDDepoimentos + "-form").getForm().submit({
                                        url: "ajax/addDepoimento.php",
                                        success: function () {
                                            alertSuccess("Depoimento Inserido com sucesso!");
                                            store.reload();
                                            Ext.getCmp(windowIDDepoimentos + "-adicionar").close();
                                        }
                                    })
                                }
                            }
                        }]
                    }).show();
                }
            }],
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    width: 120,
                    sortable: true
                },
                columns: [{
                    header:"Nome",
                    dataIndex:"desnome"
                },{
                    header:"Depoimento",
                    dataIndex: "desdepoimento"
                }]
            }),
            listeners:{
                rowclick:function(obj, rowIndex, e){
                    Ext.MessageBox.alert("Depoimento",obj.getStore().getAt(rowIndex).get("desdepoimento"))
                },
                rowcontextmenu: function (obj, rowIndex, e) {
                    e.stopEvent();
                    var depoimento = obj.getStore().getAt(rowIndex);
                    //
                    new Ext.menu.Menu({
                        items: [{
                            text: 'Excluir',
                            iconCls: 'ico_delete',
                            handler: function () {
                                Ext.MessageBox.confirm("Aviso!", "Deseja realmente excluir este depoimento?", function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: 'ajax/removeDepoimento.php',
                                            params: {
                                                iddepoimento: depoimento.get("iddepoimento")
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    store.reload();
                                                } else {
                                                    alertError(json.msg);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        }]
                    }).showAt(e.xy);
                }
            },
            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
            autoWidth: true,
            autoHeight: true
        }],
        height:400,
        modal:true
    }).show();
}
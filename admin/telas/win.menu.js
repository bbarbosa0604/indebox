var windowIDmenu = "win.menu";
//
if(Ext.getCmp("win.menu")){
    Ext.getCmp("win.menu").show();
} else {
    //
    this.windowEditMenu = function(node, idmenu, desmenu, desicon){
        myMask.show();
        //
        if(idmenu != null) {
            var desacao = 'edit';
        } else {
            var desacao = 'add';
        }
        //
        $.getJSON("json/getMenuUrl.php",{ idmenu: idmenu },function(data){
            myMask.hide();
            //
            new Ext.Window({
                title:"Adicionar um novo Menu",
                height:190,
                width:400,
                id:windowIDmenu+'-adicionarmenu',
                tbar:['->',{
                    iconCls:"ico_application_add",
                    handler:function(){
                        Ext.MessageBox.prompt("Adicionar tela","Insira o nome da tela",function(btn,text){
                            if(btn=="ok"){
                                Ext.getCmp(windowIDmenu+"-url").setValue("function(){ window.location = '"+text+"' }");
                            }
                        });
                    }
                },{
                    iconCls:"ico_application_cascade",
                    handler:function(){
                        Ext.MessageBox.prompt("Adicionar janela","Insira o nome da janela",function(btn,text){
                            if(btn=="ok"){
                                Ext.getCmp(windowIDmenu+"-url").setValue("openWindowDefault(#"+text+"#)");
                            }
                        });
                    }
                }],
                items:[{
                    xtype:"form",
                    id:windowIDmenu+"-form",
                    padding:10,
                    border:false,
                    defaults:{
                        anchor:"100%"
                    },
                    labelWidth:80,
                    items:[{
                        fieldLabel:"Nome",
                        name:"desmenu",
                        xtype:'textfield',
                        value:desmenu
                    },{
                        fieldLabel:"Url",
                        name:"desurl",
                        id:windowIDmenu+"-url",
                        xtype:'textfield',
                        readOnly:true,
                        value:(data.desurl)?data.desurl:''
                    },{
                        fieldLabel:'Icone',
                        xtype:'button',
                        anchor:'',
                        width:100,
                        text:'Alterar Icone...',
                        id:windowIDmenu+'-icone',
                        handler:openWindowDefault('win.icon.js'),
                        listeners:{
                            afterrender:function(){
                                if(desicon != null && desicon.length > 0){
                                    Ext.getCmp(windowIDmenu+"-icone").setIconClass(desicon);
                                    Ext.getCmp(windowIDmenu+"-icone").setText("");
                                    Ext.getCmp(windowIDmenu+"-icone").setWidth(16);
                                }
                            }
                        }
                    }]
                }],
                buttons:[{
                    text:'Salvar',
                    iconCls:'ico_save',
                    handler:function(){
                        if(Ext.getCmp(windowIDmenu+'-icone').iconCls){
                            Ext.getCmp(windowIDmenu+"-form").getForm().submit({
                                url:"ajax/addMenu.php",
                                params:{
                                    idmenupai:node.id.replace("menu-",""),
                                    desacao:desacao,
                                    desicon:Ext.getCmp(windowIDmenu+'-icone').iconCls
                                },
                                success:function(){
                                    Ext.getCmp(windowIDmenu+'-adicionarmenu').close();
                                    Ext.getCmp(windowIDmenu+'-tree').getRootNode().reload();
                                }
                            });
                        } else {
                            Ext.MessageBox.alert("Aviso!","Por favor, escolha um icone");
                        }
                    }
                }]
            }).show();
        })
    }

    new Ext.Window({
        title:"Gerenciar Menu",
        width:400,
        height:300,
        items:[{
            xtype:"treepanel",
            useArrows: true,
            height:300,
            id:windowIDmenu+'-tree',
            autoScroll: true,
            animate: true,
            enableDD: true,
            containerScroll: true,
            border: false,
            dataUrl: 'json/getMenu.php',
            root: {
                nodeType: 'async',
                text: 'Menu',
                draggable: false,
                id:'menu-0'
            },
            listeners:{
                afterrender:function(){
                    Ext.getCmp(windowIDmenu+"-tree").getRootNode().expand()
                },
                contextmenu:function(node, e){
                    new Ext.menu.Menu({
                        items: [{
                            text: 'Adicionar Submenu',
                            iconCls:'ico_add',
                            handler:function(){
                                windowEditMenu(node, null, null, null);
                            }
                        },'-',{
                            text: 'Editar',
                            iconCls:'ico_pencil',
                            handler:function(){
                                windowEditMenu(node, node.id.replace("menu-",""),node.attributes.text,node.attributes.iconCls);
                            }
                        },'-',{
                            text:'Excluir Item',
                            iconCls:'ico_delete',
                            handler:function(){
                                Ext.MessageBox.confirm("Aviso!","Deseja realmente excluir este menu?",function(btn){
                                    if(btn=='yes'){
                                        Ext.Ajax.request({
                                            url:'ajax/removeMenu.php',
                                            params:{
                                                idmenu:node.id.replace("menu-","")
                                            },
                                            success:function(){
                                                Ext.getCmp(windowIDmenu+'-tree').getRootNode().reload();
                                            }
                                        })
                                    }
                                });
                            }
                        }]
                    }).showAt(e.xy);
                },
                movenode:function(tree, node, oldParent, newParent, index){

                    var mode = {
                        idmenu:[],
                        nrordem:[],
                        parent:[]
                    };

                    var nrordem = 0;

                    newParent.eachChild(function(a){
                        mode.idmenu.push(a.attributes.id.replace("menu-",""));
                        mode.nrordem.push(nrordem++);
                        mode.parent.push(newParent.attributes.id.replace("menu-",""));
                    });

                    Ext.Ajax.request({
                        url:'ajax/alterPositionMenu.php',
                        params:{
                            idmenu:mode.idmenu.toString(),
                            nrordem:mode.nrordem.toString(),
                            parent:mode.parent.toString()
                        },
                        success:function(){
                            Ext.getCmp(windowIDmenu+'-tree').getRootNode().reload();
                        }
                    })
                }
            }
        }]
    }).show();
}
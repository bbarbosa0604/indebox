var windowIDFaleConosco = "win.faleconosco";
if(Ext.getCmp(windowIDFaleConosco)){
    Ext.getCmp(windowIDFaleConosco).show();
} else {
    myMask.show();

    var storeFaleConosco = new Ext.data.JsonStore({
        url:"json/getFaleConosco.php",
        root:"myData",
        fields: ['idfaleconosco', 'desassunto', 'desmensagem', 'desnome', 'desemail', { name: "dtcadastro", type: "date", dateFormat: "timestamp" }, "destelefone","idstatus"],
        autoLoad:true,
        listeners:{
            load:function(){
                Ext.getCmp(windowIDFaleConosco+"-total").setText(this.getTotalCount());
                myMask.hide();
            }
        }
    });

    new Ext.Window({
        title:"Fale Conosco",
        width:800,
        autoScroll:true,
        bbar:[{
            text:"Atualizar",
            iconCls:'ico_arrow_refresh',
            handler:function(){
                storeFaleConosco.reload();
            }
        },'->','Total de fale conosco: ',{
            xtype:"tbtext",
            id:windowIDFaleConosco+"-total",
            text:0
        }],
        tbar:['Filtro',{
            xtype: "datefield",
            id:windowIDFaleConosco + "-dt1"
        }, 'a', {
            xtype: "datefield",
            id: windowIDFaleConosco + "-dt2"
        }, {
            xtype: "button",
            text: "Pesquisar",
            iconCls:"ico_search",
            handler: function () {
                if (Ext.getCmp(windowIDFaleConosco + "-dt1").getValue() && Ext.getCmp(windowIDFaleConosco + "-dt2").getValue()) {
                    storeFaleConosco.reload({
                        params: {
                            dt1: Ext.getCmp(windowIDFaleConosco + "-dt1").getValue().format("Y-m-d"),
                            dt2: Ext.getCmp(windowIDFaleConosco + "-dt2").getValue().format("Y-m-d")
                        }
                    })
                } else {
                    Ext.MessageBox.alert("", "Selecione as duas datas !");
                }
                
            }
        }],
        items:[{
            xtype:"grid",
            store: storeFaleConosco,
            border:false,
            autoExpandColumn:"1",
            loadMask:true,
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    width: 120,
                    sortable: true
                },
                columns: [{
                    width: 50,
                    dataIndex: "idstatus",
                    renderer: function (val) {
                        if (val != 1) {
                            return '<img src= "res/img/icons/16/Essen_check.png"/>';
                        } else {
                            return '<img src= "res/img/icons/16/contact_mail.png"/>';
                        }
                    }
                }, {
                    header:"Titulo",
                    dataIndex:"desassunto"
                },{
                    header:"Nome",
                    dataIndex:"desnome"
                },{
                    header:"Email",
                    dataIndex:"desemail"
                }, {
                    header: "Telefone",
                    dataIndex: "destelefone"
                }, {
                    header: "Data de Cadastro",
                    dataIndex: "dtcadastro",
                    xtype: "datecolumn",
                    format:"d/m/Y H:i"
                }]
            }),
            listeners:{
                rowclick:function(obj, rowIndex, e){
                    Ext.MessageBox.alert(obj.getStore().getAt(rowIndex).get("destitulo"), "<b>Email: </b>" + obj.getStore().getAt(rowIndex).get("desemail") + "<br><br>" + obj.getStore().getAt(rowIndex).get("desmensagem"))
                },
                rowcontextmenu: function (obj, rowIndex, e) {
                    e.stopEvent();
                    
                    obj.getSelectionModel().selectRow(rowIndex);

                    var faleconosco = obj.getStore().getAt(rowIndex);
                    //
                    new Ext.menu.Menu({
                        items: [{
                            text: 'Marcar como Respondido',
                            iconCls: 'ico_Essen_check',
                            handler: function () {
                                Ext.MessageBox.confirm("Aviso!", "Deseja realmente Marcar como respondido?", function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: 'ajax/editStatusFaleConosco.php',
                                            params: {
                                                idfaleconosco: faleconosco.get("idfaleconosco"),
                                                idstatus:2
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    storeFaleConosco.reload();
                                                } else {
                                                    alertError(json.msg);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        }, {
                            text: 'Marcar como não Respondido',
                            iconCls: 'ico_contact_mail',
                            handler: function () {
                                Ext.MessageBox.confirm("Aviso!", "Deseja realmente Marcar como não respondido?", function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: 'ajax/editStatusFaleConosco.php',
                                            params: {
                                                idfaleconosco: faleconosco.get("idfaleconosco"),
                                                idstatus: 1
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    storeFaleConosco.reload();
                                                } else {
                                                    alertError(json.msg);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        },{
                            text: 'Excluir',
                            iconCls: 'ico_action_delete',
                            handler: function () {
                                Ext.Ajax.request({
                                    url: 'ajax/removeFaleConosco.php',
                                    params: {
                                        idfaleconosco: faleconosco.get('idfaleconosco')
                                    },
                                    success: function (a) {
                                        var json = $.parseJSON(a.responseText);
                                        if (json.success) {                                            
                                            Ext.Msg.alert('Aviso', 'Fale conosco removido com sucesso.');
                                            storeFaleConosco.reload();
                                        } else {
                                            Ext.Msg.alert('Aviso', json.msg);
                                        }

                                    }
                                });
                            }
                        }]
                    }).showAt(e.xy);
                }
            },
            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
            autoWidth: true,
            autoHeight: true
        }],
        height:400,
        modal:true
    }).show();
}
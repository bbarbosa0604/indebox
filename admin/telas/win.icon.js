var windowIDicon = "win.icon";
//
if(Ext.getCmp(windowIDicon)){
    Ext.getCmp(windowIDicon).show();
} else {
    var store = new Ext.data.JsonStore({
        url: 'json/getIcon.php',
        root: 'myData',
        fields: ['name', 'url'],
        autoLoad:true
    });
    //
    var tpl = new Ext.XTemplate(
        '<tpl for=".">',
        '<div class="thumb-wrap-16" id="{name}">',
        '<img src="{url}" title="{name}" />',
        '</div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );
    //
    new Ext.Window({
        title:"Gerenciar Menu",
        id:windowIDicon,
        width:400,
        modal:true,
        height:300,
        items:[{
            xtype:'dataview',
            store: store,
            tpl: tpl,
            autoHeight:true,
            multiSelect: false,
            itemSelector:'div.thumb-wrap-16',
            emptyText: 'Nenhuma imagem encontrada',
            listeners:{
                click:function(obj, index){
                    Ext.getCmp(windowIDmenu+"-icone").setIconClass(obj.getStore().getAt(index).get("name"));
                    Ext.getCmp(windowIDmenu+"-icone").setText("");
                    Ext.getCmp(windowIDmenu+"-icone").setWidth(16);
                    Ext.getCmp(windowIDicon).close();
                }
            }
        }]
    }).show();
}
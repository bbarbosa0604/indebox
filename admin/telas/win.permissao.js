var windowIDpermissao = 'win.permissao';
//
if (Ext.getCmp(windowIDpermissao)) {
    Ext.getCmp(windowIDpermissao).show();
} else {
    myMask.show();
    //
    var storeGrupos = new Ext.data.JsonStore({
        url:"json/getGrupos.php",
        root:"myData",
        fields:['idgrupo','desgrupo'],
        autoLoad:true,
        listeners:{
            load:function(){
                myMask.hide();
            }
        }
    });

    var storeUsuarioGrupos = new Ext.data.JsonStore({
        url:"json/getGrupoUsuarios.php",
        root:"myData",
        fields:['idusuario','desusuario'],
        listeners:{
            load:function(){
                myMask.hide();
            }
        }
    });

    var storeUsuarios = new Ext.data.JsonStore({
        url:"json/getUsuarios.php",
        root:"myData",
        fields:['idusuario','desusuario'],
        autoLoad:true
    });

    setTimeout(function(){
        new Ext.Window({
            title: "Gerir Usuários e Permissões",
            width: 700,
            height: 400,
            modal: true,
            id: windowIDpermissao,
            layout:'hbox',
            iconCls:"ico_Essen_lock",
            layoutConfig: {
                align : 'stretch',
                pack  : 'start'
            },
            border:false,
            items:[{
                layout: 'accordion',
                items:[{
                    title: 'Gerir Grupos',
                    iconCls:'ico_Essen_config',
                    items:[{
                        xtype:'listview',
                        store: storeGrupos,
                        multiSelect: true,
                        autoWidth:true,
                        emptyText: 'Nenhum grupo cadastrado',
                        reserveScrollOffset: true,
                        columns: [{
                            header: 'Grupos',
                            dataIndex: 'desgrupo'
                        }],
                        listeners:{
                            contextmenu:function(obj,index,node,e){
                                e.stopEvent();
                                new Ext.menu.Menu({
                                    items: [{
                                        text:'Excluir',
                                        iconCls:'ico_delete',
                                        handler:function(){
                                            Ext.MessageBox.confirm("Aviso!","Deseja realmente excluir este grupo?",function(btn){
                                                if(btn=='yes'){
                                                    Ext.Ajax.request({
                                                        url:'ajax/removeGrupo.php',
                                                        params:{
                                                            idgrupo:obj.getStore().getAt(index).get("idgrupo")
                                                        },
                                                        success:function(){
                                                            storeGrupos.reload();
                                                        }
                                                    })
                                                }
                                            });
                                        }
                                    }]
                                }).showAt(e.xy);
                            },
                            click:function(obj,index){
                                var grupo = obj.getStore().getAt(index);
                                //
                                myMask.show();
                                //
                                storeUsuarioGrupos.reload({
                                    params:{
                                        idgrupo:grupo.get("idgrupo")
                                    }
                                });
                                //
                                Ext.getCmp(windowIDpermissao+'-panel2').removeAll();
                                Ext.getCmp(windowIDpermissao+'-panel2').add({
                                    xtype:'fieldset',
                                    title: 'Gerir permissões do grupo '+grupo.get("desgrupo"),
                                    autoHeight:true,
                                    collapsed: false,
                                    items :[{
                                        xtype:"tabpanel",
                                        defaults:{
                                            autoScroll:true
                                        },
                                        width:400,
                                        height:300,
                                        activeTab: 0,
                                        items:[{
                                            title:'Gerir Usuarios',
                                            items:[{
                                                xtype:'listview',
                                                store: storeUsuarioGrupos,
                                                multiSelect: true,
                                                autoWidth:true,
                                                autoHeight:true,
                                                emptyText: 'Nenhum grupo cadastrado',
                                                reserveScrollOffset: true,
                                                id:windowIDpermissao+"-listusuarios",
                                                columns: [{
                                                    header: 'Usuarios Cadastrados a este grupo',
                                                    dataIndex: 'desusuario'
                                                }]
//                                                listeners:{
//                                                    contextmenu:function(obj,index,node,e){
//                                                        e.stopEvent();
//                                                        new Ext.menu.Menu({
//                                                            items: [{
//                                                                text:'Excluir',
//                                                                iconCls:'ico_delete',
//                                                                handler:function(){
//                                                                    Ext.MessageBox.confirm("Aviso!","Deseja realmente excluir este usuario deste grupo?",function(btn){
//                                                                        if(btn=='yes'){
//                                                                            Ext.Ajax.request({
//                                                                                url:'ajax/removeGrupoUsuario.php',
//                                                                                params:{
//                                                                                    idgrupo:grupo.get("idgrupo"),
//                                                                                    idusuario:obj.getStore().getAt(index).get("idusuario")
//                                                                                },
//                                                                                success:function(){
//                                                                                    storeUsuarioGrupos.reload({
//                                                                                        params:{
//                                                                                            idgrupo:grupo.get("idgrupo")
//                                                                                        }
//                                                                                    });
//                                                                                }
//                                                                            })
//                                                                        }
//                                                                    });
//                                                                }
//                                                            }]
//                                                        }).showAt(e.xy);
//                                                    },
//                                                }
                                            }],
                                            bbar:[{
                                                text:'Excluir',
                                                iconCls:'ico_delete',
                                                handler:function(){
                                                    if(Ext.getCmp(windowIDpermissao+"-listusuarios").getSelectionCount()==0){
                                                        alertError("Selecione um usuário!");
                                                    } else {
                                                        Ext.MessageBox.confirm("Aviso!","Deseja realmente excluir este usuario deste grupo?",function(btn){
                                                            if(btn=='yes'){
                                                                Ext.Ajax.request({
                                                                    url:'ajax/removeGrupoUsuario.php',
                                                                    params:{
                                                                        idgrupo:grupo.get("idgrupo"),
                                                                        idusuario:Ext.getCmp(windowIDpermissao+"-listusuarios").getSelectedRecords()[0].get("idusuario")
                                                                    },
                                                                    success:function(){
                                                                        storeUsuarioGrupos.reload({
                                                                            params:{
                                                                                idgrupo:grupo.get("idgrupo")
                                                                            }
                                                                        });
                                                                    }
                                                                })
                                                            }
                                                        });
                                                    }

                                                }
                                            },{
                                                text:'Adicionar',
                                                iconCls:'ico_add',
                                                handler:function(){
                                                    //
                                                    new Ext.Window({
                                                        title:'Selecione um usuario',
                                                        width:300,
                                                        id:windowIDpermissao+'-window-escolherusuario',
                                                        height:100,
                                                        items:[{
                                                            xtype:"form",
                                                            id:windowIDpermissao+"-form-escolherusuario",
                                                            padding:10,
                                                            items:[{
                                                                hideLabel:true,
                                                                xtype:"combo",
                                                                typeAhead: true,
                                                                triggerAction: 'all',
                                                                lazyRender:true,
                                                                allowBlank:false,
                                                                anchor:"100%",
                                                                mode: 'local',
                                                                store: storeUsuarios,
                                                                valueField: 'idusuario',
                                                                displayField: 'desusuario',
                                                                hiddenName:'idusuario'
                                                            }]
                                                        }],
                                                        buttons:[{
                                                            text:"Salvar",
                                                            iconCls:"ico_save",
                                                            handler:function(){
                                                                if(Ext.getCmp(windowIDpermissao+"-form-escolherusuario").getForm().isValid()){
                                                                    Ext.getCmp(windowIDpermissao+"-form-escolherusuario").getForm().submit({
                                                                        url:"ajax/addGrupoUsuario.php",
                                                                        params:{
                                                                            idgrupo:grupo.get("idgrupo")
                                                                        },
                                                                        success:function(){
                                                                            storeUsuarioGrupos.reload({
                                                                                params:{
                                                                                    idgrupo:grupo.get("idgrupo")
                                                                                }
                                                                            });
                                                                            Ext.getCmp(windowIDpermissao+'-window-escolherusuario').close();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }]
                                                    }).show();
                                                }
                                            }]
                                        },{
                                            title:'Menu',
                                            items:[{
                                                xtype:"treepanel",
                                                useArrows: true,
                                                id:windowIDpermissao+'-tree',
                                                autoScroll: true,
                                                animate: true,
                                                enableDD: false,
                                                containerScroll: true,
                                                border: false,
                                                dataUrl: 'json/getGrupoMenu.php?idgrupo='+grupo.get("idgrupo"),
                                                root: {
                                                    nodeType: 'async',
                                                    text: 'Menu',
                                                    draggable: false,
                                                    id:'menu-0'
                                                },
                                                listeners:{
                                                    afterrender:function(){
                                                        Ext.getCmp(windowIDpermissao+"-tree").getRootNode().expand()
                                                    }
                                                }
                                            }],
                                            tbar:[{
                                                text:'Salvar',
                                                iconCls:'ico_save',
                                                handler:function(){
                                                    var arrMenus = [];
                                                    //
                                                    Ext.each(Ext.getCmp(windowIDpermissao+'-tree').getChecked(),function(a){
                                                        arrMenus.push(a.id.replace("menu-",""))
                                                    });
                                                    //
                                                    Ext.Ajax.request({
                                                        url:"ajax/addGrupoMenu.php",
                                                        params:{
                                                            idmenu:arrMenus.toString(),
                                                            idgrupo:grupo.get("idgrupo")
                                                        },
                                                        success:function(){
                                                            Ext.MessageBox.alert("","Vinculo realizado com sucesso!");
                                                            //
                                                            Ext.getCmp(windowIDpermissao+'-tree').getRootNode().reload();
                                                        }
                                                    });
                                                }
                                            }]
                                        }]
                                    }]
                                });
                                Ext.getCmp(windowIDpermissao+'-panel2').doLayout();
                            }
                        }
                    }],
                    tbar:[{
                        text:'Adicionar',
                        iconCls:'ico_add',
                        handler:function(){
                            Ext.MessageBox.prompt("Adicionar novo Grupo","Insira o nome do grupo",function(btn,text){
                                if(btn=='ok'){
                                    Ext.Ajax.request({
                                        url:'ajax/addGrupo.php',
                                        params:{
                                            desgrupo:text
                                        },
                                        success:function(){
                                            storeGrupos.reload();
                                        }
                                    });
                                }
                            });
                        }
                    }]
                },{
                    title: 'Gerir Usuarios',
                    iconCls:'ico_Essen_user',
                    items:[{
                        xtype:'listview',
                        store: storeUsuarios,
                        multiSelect: true,
                        autoWidth:true,
                        emptyText: 'Nenhum usuario cadastrado',
                        reserveScrollOffset: true,
                        columns: [{
                            header: 'Usuarios',
                            dataIndex: 'desusuario'
                        }],
                        listeners:{
                            click:function(obj,index){
                                var usuario = obj.getStore().getAt(index);
                                //
                                Ext.getCmp(windowIDpermissao+'-panel2').removeAll();
                                Ext.getCmp(windowIDpermissao+'-panel2').add({
                                    xtype:'fieldset',
                                    title: 'Gerir permissões do usuario '+usuario.get("desusuario"),
                                    autoHeight:true,
                                    collapsed: false,
                                    items :[{
                                        xtype:"tabpanel",
                                        defaults:{
                                            autoScroll:true
                                        },
                                        width:400,
                                        height:300,
                                        activeTab: 0,
                                        items:[{
                                            title:'Menu',
                                            items:[{
                                                xtype:"treepanel",
                                                useArrows: true,
                                                id:windowIDpermissao+'-tree',
                                                autoScroll: true,
                                                animate: true,
                                                enableDD: false,
                                                containerScroll: true,
                                                border: false,
                                                dataUrl: 'json/getUsuarioMenu.php?idusuario='+usuario.get("idusuario"),
                                                root: {
                                                    nodeType: 'async',
                                                    text: 'Menu',
                                                    draggable: false,
                                                    id:'menu-0'
                                                },
                                                listeners:{
                                                    afterrender:function(){
                                                        Ext.getCmp(windowIDpermissao+"-tree").getRootNode().expand()
                                                    },
                                                    checkchange: function(node, checked){
                                                        Ext.Ajax.request({
                                                            url:'ajax/addPermissao.php',
                                                            params:{
                                                                idmenu:node.id.replace("menu-",""),
                                                                idusuario:usuario.get("idusuario"),
                                                                inacao:checked
                                                            },
                                                            success:function(){

                                                            }
                                                        })
                                                    }
                                                }
                                            }]
                                        }]
                                    }]
                                });
                                Ext.getCmp(windowIDpermissao+'-panel2').doLayout();
                            }
                        }
                    }]
                }],
                flex:1
            },{
                flex:2,
                id:windowIDpermissao+'-panel2',
                padding:10
            }]
        }).show();
    },100);
}
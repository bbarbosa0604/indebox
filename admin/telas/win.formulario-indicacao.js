var windowIDIndicacao = "win.formulario-indicacao.html";
if(Ext.getCmp(windowIDIndicacao)){
    Ext.getCmp(windowIDIndicacao).show();
} else {
    myMask.show();

    var storeFaleConosco = new Ext.data.JsonStore({
        url:"json/getFormulario-indicacao.php",
        root:"myData",
        fields:['idindicacao', 'idstatus', {name: 'desnome', type:'string' }, {name: 'desemail', type:'string' }, {name: "dtcadastro", type:"date", dateFormat:"timestamp"}, {name: 'desnomeamigo', type:'string' }, {name: 'desemailamigo', type:'string' }],
        autoLoad:true,
        listeners:{
            load:function(){
                Ext.getCmp(windowIDIndicacao+"-total").setText(this.getTotalCount());
                myMask.hide();
            }
        }
    });

    new Ext.Window({
        title:"Formulário de Indicacao",
        width:700,
        autoScroll:true,
        bbar:[{
            text:"Atualizar",
            iconCls:'ico_arrow_refresh',
            handler:function(){
                storeFaleConosco.reload();
            }
        },'->','Total de fale conosco: ',{
            xtype:"tbtext",
            id:windowIDIndicacao+"-total",
            text:0
        }],
        tbar: ['Filtro', {
            xtype: "datefield",
            id: windowIDIndicacao + "-dt1"
        }, 'a', {
            xtype: "datefield",
            id: windowIDIndicacao + "-dt2"
        }, {
            xtype: "button",
            text: "Pesquisar",
            iconCls: "ico_search",
            handler: function () {
                if (Ext.getCmp(windowIDIndicacao + "-dt1").getValue() && Ext.getCmp(windowIDIndicacao + "-dt2").getValue()) {
                    storeFaleConosco.reload({
                        params: {
                            dt1: Ext.getCmp(windowIDIndicacao + "-dt1").getValue().format("Y-m-d"),
                            dt2: Ext.getCmp(windowIDIndicacao + "-dt2").getValue().format("Y-m-d")
                        }
                    })
                } else {
                    Ext.MessageBox.alert("", "Selecione as duas datas !");
                }

            }
        }],
        items:[{
            xtype:"grid",
            store: storeFaleConosco,
            border:false,
            autoExpandColumn:"1",
            loadMask:true,
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    width: 120,
                    sortable: true
                },
                columns: [{
                    width: 50,
                    dataIndex: "idstatus",
                    renderer: function (val) {
                        if (val != 1) {
                            return '<img src= "res/img/icons/16/Essen_check.png"/>';
                        } else {
                            return '<img src= "res/img/icons/16/contact_mail.png"/>';
                        }
                    }
                },{
                    header:"Nome",
                    dataIndex:"desnome",
                    sortable:true
                },{
                    header:"Email",
                    dataIndex:"desemail"
                },{
                    header:"Nome Amigo",
                    dataIndex:"desnomeamigo"
                },{
                    header:"Email Amigo",
                    dataIndex:"desemailamigo"
                }, {
                    header: "Data de Cadastro",
                    dataIndex: "dtcadastro",
                    xtype: "datecolumn",
                    format:"d/m/Y H:i"
                }]
            }),
            listeners:{
                rowclick:function(obj, rowIndex, e){
                    Ext.MessageBox.alert("Mensagem",'<b>Email:</b>' + obj.getStore().getAt(rowIndex).get("desemail") + " <br> <b>Email Amigo</b>:" + obj.getStore().getAt(rowIndex).get("desemailamigo"))
                },
                rowcontextmenu: function (obj, rowIndex, e) {
                    e.stopEvent();
                    var indicacao = obj.getStore().getAt(rowIndex);
                    obj.getSelectionModel().selectRow(rowIndex);
                    //
                    new Ext.menu.Menu({
                        items: [{
                            text: 'Marcar como Respondido',
                            iconCls: 'ico_Essen_check',
                            handler: function () {
                                Ext.MessageBox.confirm("Aviso!", "Deseja realmente Marcar como respondido?", function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: 'ajax/editStatusFormularioIndicacao.php',
                                            params: {
                                                idindicacao: indicacao.get("idindicacao"),
                                                idstatus: 2
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    storeFaleConosco.reload();
                                                } else {
                                                    alertError(json.msg);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        }, {
                            text: 'Marcar como não Respondido',
                            iconCls: 'ico_contact_mail',
                            handler: function () {
                                Ext.MessageBox.confirm("Aviso!", "Deseja realmente Marcar como não respondido?", function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: 'ajax/editStatusFormularioIndicacao.php',
                                            params: {
                                                idindicacao: indicacao.get("idindicacao"),
                                                idstatus: 1
                                            },
                                            success: function (a) {
                                                var json = $.parseJSON(a.responseText);
                                                if (json.success) {
                                                    storeFaleConosco.reload();
                                                } else {
                                                    alertError(json.msg);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        },{
                            text: 'Excluir',
                            iconCls: 'ico_action_delete',
                            handler: function () {
                                Ext.Ajax.request({
                                    url: 'ajax/removeIndicacao.php',
                                    params: {
                                        idindicacao: indicacao.get('idindicacao')
                                    },
                                    success: function (a) {
                                        var json = $.parseJSON(a.responseText);
                                        if (json.success) {                                            
                                            Ext.Msg.alert('Aviso', 'Indicação removido com sucesso.');
                                            storeFaleConosco.reload();
                                        } else {
                                            Ext.Msg.alert('Aviso', json.msg);
                                        }

                                    }
                                });
                            }
                        }]
                    }).showAt(e.xy);
                }
            },
            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
            autoWidth: true,
            autoHeight: true
        }],
        height:400,
        modal:true
    }).show();
}
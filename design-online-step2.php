<?php
	
	require_once("inc/configuration.php");

	$page = new Page(array(
        "data" => array(
            "head_title" => 'Indebox - Design online'
        )
	));

    if(isset($_SESSION["idpedido"])===false){
        header("Location: design-online");
        exit;
    }
    
    $pedido = new Pedido($_SESSION["idpedido"]);    
    $pessoa = new Pessoa($pedido->getidpessoa());
    $descpf = $destelefone = $desemail = $desrg = "";

    foreach($pessoa->getContatos() as $val){
        if($val["idcontatotipo"]==1){
            $destelefone = $val["descontato"];
        } else if($val["idcontatotipo"]==3) {
            $desemail = $val["descontato"];
        }
    }
    
    foreach($pessoa->getDocumentos() as $val){
        if($val["iddocumentotipo"]==1){
            $descpf = $val["desdocumento"];
        } else if($val["iddocumentotipo"]==2) {
            $desrg = $val["desdocumento"];
        }
    }

    $plano = $pedido->getPlano();

	$page->setTpl("design-online-step2",array(
        "idpedido"=>$pedido->getidpedido(),
        "year"=>date("Y"),
        "desplano"=>$plano["desplano"],
        "vlpedido"=>number_format($pedido->getvlpedido(),2,",","."),
        "idpessoa"=>$pessoa->getidpessoa(),
        "despessoa"=>utf8_decode($pessoa->getdespessoa()),
        "desemail"=>$desemail,
        "destelefone"=>$destelefone,
        "desrg"=>$desrg,        
        "descpf"=>$descpf,
        "dtcadastro"=>$pedido->getdtcadastro(),
        "ambientes"=>$pedido->getAmbientes(),
        "ip"=>$_SERVER["REMOTE_ADDR"],
        "contrato"=>file_get_contents("admin/txt/contrato.txt")
    ));

?>
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: indebox
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_banners`
--

DROP TABLE IF EXISTS `tb_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `idpagina` int(11) NOT NULL,
  `desurl` varchar(200) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banners`
--

LOCK TABLES `tb_banners` WRITE;
/*!40000 ALTER TABLE `tb_banners` DISABLE KEYS */;
INSERT INTO `tb_banners` VALUES (1,1,'res/img/banner-indebox.jpg','','2015-10-05 18:50:47'),(2,1,'res/img/banner-indebox-design.jpg','','2015-10-05 19:05:22'),(3,2,'res/img/design_para_todos_indebox.jpg','','2015-10-05 19:07:48'),(4,1,'res/img/Penguins.jpg','\0','2015-10-05 19:08:38');
/*!40000 ALTER TABLE `tb_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_categorias`
--

DROP TABLE IF EXISTS `tb_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categorias` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `descategoria` varchar(100) NOT NULL,
  `desurlcapa` varchar(100) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categorias`
--

LOCK TABLES `tb_categorias` WRITE;
/*!40000 ALTER TABLE `tb_categorias` DISABLE KEYS */;
INSERT INTO `tb_categorias` VALUES (1,'Cozinha','res/img/galeria/cozinha.jpg','','2015-10-05 18:47:54'),(2,'Sala de estar','res/img/galeria/37.jpg','','2015-10-05 18:47:54'),(3,'Quarto','res/img/galeria/41.jpg','','2015-10-05 18:47:54'),(4,'Varanda','res/img/galeria/16.jpg','','2015-10-05 18:47:54'),(5,'Fachada','res/img/galeria/7.jpg','','2015-10-05 18:47:55'),(6,'Banheiro','res/img/galeria/15.jpg','','2015-10-05 18:47:55'),(7,'teste','res/img/galeria/paypal-banner.jpg','','2015-10-08 00:27:20');
/*!40000 ALTER TABLE `tb_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_contatos`
--

DROP TABLE IF EXISTS `tb_contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_contatos` (
  `idcontato` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `descontato` varchar(200) NOT NULL,
  `idcontatotipo` int(11) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontato`),
  KEY `FK_contatos_pessoas_idpessoa` (`idpessoa`),
  KEY `FK_contatos_contatotipos_idcontatotipo` (`idcontatotipo`),
  CONSTRAINT `FK_contatos_contatotipos_idcontatotipo` FOREIGN KEY (`idcontatotipo`) REFERENCES `tb_contatotipos` (`idcontatotipo`),
  CONSTRAINT `FK_contatos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_contatos`
--

LOCK TABLES `tb_contatos` WRITE;
/*!40000 ALTER TABLE `tb_contatos` DISABLE KEYS */;
INSERT INTO `tb_contatos` VALUES (39,2,'(11) 3254-4200',1,'','2015-10-07 21:19:00'),(41,3,'(11) 3254-2200',1,'','2015-10-07 21:21:00'),(42,3,'bbarbosa@impacta.com.br',3,'','2015-10-07 21:21:00');
/*!40000 ALTER TABLE `tb_contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_contatotipos`
--

DROP TABLE IF EXISTS `tb_contatotipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_contatotipos` (
  `idcontatotipo` int(11) NOT NULL AUTO_INCREMENT,
  `descontatotipo` varchar(200) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontatotipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_contatotipos`
--

LOCK TABLES `tb_contatotipos` WRITE;
/*!40000 ALTER TABLE `tb_contatotipos` DISABLE KEYS */;
INSERT INTO `tb_contatotipos` VALUES (1,'Telefone','','2015-10-05 18:45:59'),(2,'Celular','','2015-10-05 18:45:59'),(3,'Email','','2015-10-07 21:20:53');
/*!40000 ALTER TABLE `tb_contatotipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_depoimentos`
--

DROP TABLE IF EXISTS `tb_depoimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `desdepoimento` varchar(2000) DEFAULT NULL,
  `desnome` varchar(200) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_depoimentos`
--

LOCK TABLES `tb_depoimentos` WRITE;
/*!40000 ALTER TABLE `tb_depoimentos` DISABLE KEYS */;
INSERT INTO `tb_depoimentos` VALUES (1,'Adorei. Normalmente esses serviços online são muito impessoais, o que não aconteceu com os profissionais da Indebox. Foram atenciosos, sempre responderam minhas dúvidas com respeito e fizeram me sentir como uma cliente especial.','Camila R.','2015-10-07 22:30:00','\0');
/*!40000 ALTER TABLE `tb_depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_documentos`
--

DROP TABLE IF EXISTS `tb_documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_documentos` (
  `iddocumento` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `desdocumento` varchar(30) NOT NULL,
  `iddocumentotipo` int(11) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddocumento`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_documentos`
--

LOCK TABLES `tb_documentos` WRITE;
/*!40000 ALTER TABLE `tb_documentos` DISABLE KEYS */;
INSERT INTO `tb_documentos` VALUES (1,1,'40210158840',1,'','2015-10-05 18:46:28'),(2,3,'333580698000',1,'','2015-10-07 21:56:00'),(3,3,'285729391',2,'','2015-10-07 21:56:00');
/*!40000 ALTER TABLE `tb_documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_documentotipos`
--

DROP TABLE IF EXISTS `tb_documentotipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_documentotipos` (
  `iddocumentotipo` int(11) NOT NULL AUTO_INCREMENT,
  `desdocumentotipo` varchar(200) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddocumentotipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_documentotipos`
--

LOCK TABLES `tb_documentotipos` WRITE;
/*!40000 ALTER TABLE `tb_documentotipos` DISABLE KEYS */;
INSERT INTO `tb_documentotipos` VALUES (1,'CPF','','2015-10-05 18:46:00'),(2,'RG','','2015-10-05 18:46:00');
/*!40000 ALTER TABLE `tb_documentotipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_endereco` (
  `idendereco` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) DEFAULT NULL,
  `desendereco` varchar(500) DEFAULT NULL,
  `desnumero` varchar(45) DEFAULT NULL,
  `descomplemento` varchar(200) DEFAULT NULL,
  `desbairro` varchar(200) DEFAULT NULL,
  `descidade` varchar(200) DEFAULT NULL,
  `desestado` varchar(2) DEFAULT NULL,
  `descep` varchar(9) DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  `dtcadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`idendereco`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_endereco`
--

LOCK TABLES `tb_endereco` WRITE;
/*!40000 ALTER TABLE `tb_endereco` DISABLE KEYS */;
INSERT INTO `tb_endereco` VALUES (7,3,'Rua Jesuíno Rabello','367','','Vila Milton','Guarulhos','SP','07063150','','2015-10-07 18:56:00');
/*!40000 ALTER TABLE `tb_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_faleconosco`
--

DROP TABLE IF EXISTS `tb_faleconosco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_faleconosco` (
  `idfaleconosco` int(11) NOT NULL AUTO_INCREMENT,
  `desnome` varchar(200) DEFAULT NULL,
  `desemail` varchar(200) DEFAULT NULL,
  `destelefone` varchar(45) DEFAULT NULL,
  `desassunto` varchar(200) DEFAULT NULL,
  `desmensagem` varchar(2000) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`idfaleconosco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_faleconosco`
--

LOCK TABLES `tb_faleconosco` WRITE;
/*!40000 ALTER TABLE `tb_faleconosco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_faleconosco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_formapagamento`
--

DROP TABLE IF EXISTS `tb_formapagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_formapagamento` (
  `idformapagamento` int(11) NOT NULL AUTO_INCREMENT,
  `desformapagamento` varchar(50) DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idformapagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_formapagamento`
--

LOCK TABLES `tb_formapagamento` WRITE;
/*!40000 ALTER TABLE `tb_formapagamento` DISABLE KEYS */;
INSERT INTO `tb_formapagamento` VALUES (1,'PagSeguro',''),(2,'PayPal','');
/*!40000 ALTER TABLE `tb_formapagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fotos`
--

DROP TABLE IF EXISTS `tb_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fotos` (
  `idfoto` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `desurl` varchar(100) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idfoto`),
  KEY `FK_categorias_fotos_idcategoria` (`idcategoria`),
  CONSTRAINT `FK_categorias_fotos_idcategoria` FOREIGN KEY (`idcategoria`) REFERENCES `tb_categorias` (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fotos`
--

LOCK TABLES `tb_fotos` WRITE;
/*!40000 ALTER TABLE `tb_fotos` DISABLE KEYS */;
INSERT INTO `tb_fotos` VALUES (1,1,'res/img/galeria/1.jpg','','2015-10-05 18:49:01'),(2,1,'res/img/galeria/2.jpg','','2015-10-05 18:49:01'),(3,1,'res/img/galeria/17.jpg','','2015-10-05 18:49:01'),(4,1,'res/img/galeria/18.jpg','','2015-10-05 18:49:01'),(5,2,'res/img/galeria/3.jpg','','2015-10-05 18:49:02'),(6,2,'res/img/galeria/4.jpg','','2015-10-05 18:49:02'),(7,2,'res/img/galeria/5.jpg','','2015-10-05 18:49:02'),(8,2,'res/img/galeria/6.jpg','','2015-10-05 18:49:02'),(9,2,'res/img/galeria/12.jpg','','2015-10-05 18:49:02'),(10,2,'res/img/galeria/24.jpg','','2015-10-05 18:49:03'),(11,2,'res/img/galeria/26.jpg','','2015-10-05 18:49:03'),(12,2,'res/img/galeria/27.jpg','','2015-10-05 18:49:03'),(13,2,'res/img/galeria/28.jpg','','2015-10-05 18:49:03'),(14,2,'res/img/galeria/29.jpg','','2015-10-05 18:49:03'),(15,3,'res/img/galeria/8.jpg','','2015-10-05 18:49:04'),(16,3,'res/img/galeria/9.jpg','','2015-10-05 18:49:04'),(17,3,'res/img/galeria/10.jpg','','2015-10-05 18:49:04'),(18,3,'res/img/galeria/11.jpg','','2015-10-05 18:49:04'),(19,3,'res/img/galeria/19.jpg','','2015-10-05 18:49:04'),(20,3,'res/img/galeria/20.jpg','','2015-10-05 18:49:04'),(21,3,'res/img/galeria/21.jpg','','2015-10-05 18:49:05'),(22,3,'res/img/galeria/22.jpg','','2015-10-05 18:49:05'),(23,3,'res/img/galeria/23.jpg','','2015-10-05 18:49:05'),(24,4,'res/img/galeria/13.jpg','','2015-10-05 18:49:05'),(25,4,'res/img/galeria/16.jpg','','2015-10-05 18:49:05'),(26,4,'res/img/galeria/25.jpg','','2015-10-05 18:49:05'),(27,5,'res/img/galeria/7.jpg','\0','2015-10-05 19:09:32'),(28,6,'res/img/galeria/15.jpg','','2015-10-05 19:09:33'),(29,6,'res/img/galeria/Desert.jpg','\0','2015-10-05 20:09:29'),(30,7,'res/img/galeria/paypal-banner.jpg','','2015-10-08 00:27:20');
/*!40000 ALTER TABLE `tb_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_grupo`
--

DROP TABLE IF EXISTS `tb_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `desgrupo` varchar(50) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_grupo`
--

LOCK TABLES `tb_grupo` WRITE;
/*!40000 ALTER TABLE `tb_grupo` DISABLE KEYS */;
INSERT INTO `tb_grupo` VALUES (5,'Administradores','2014-07-15 18:27:15',''),(6,'Teste','2014-12-15 16:32:36','\0'),(7,'teste','2015-02-08 20:43:18','\0'),(8,'abc','2015-02-08 20:46:34','\0');
/*!40000 ALTER TABLE `tb_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_grupotipo`
--

DROP TABLE IF EXISTS `tb_grupotipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grupotipo` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `destipo` varchar(50) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  `instatus` bit(1) NOT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_grupotipo`
--

LOCK TABLES `tb_grupotipo` WRITE;
/*!40000 ALTER TABLE `tb_grupotipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_grupotipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_indicacao`
--

DROP TABLE IF EXISTS `tb_indicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_indicacao` (
  `idindicacao` int(11) NOT NULL AUTO_INCREMENT,
  `desnome` varchar(45) DEFAULT NULL,
  `desemail` varchar(45) DEFAULT NULL,
  `desnomeamigo` varchar(45) DEFAULT NULL,
  `desemailamigo` varchar(45) DEFAULT NULL,
  `desmensagem` varchar(45) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`idindicacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_indicacao`
--

LOCK TABLES `tb_indicacao` WRITE;
/*!40000 ALTER TABLE `tb_indicacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_indicacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_menu`
--

DROP TABLE IF EXISTS `tb_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_menu` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT COMMENT '	',
  `desmenu` varchar(50) DEFAULT NULL,
  `desicon` varchar(50) NOT NULL,
  `desurl` varchar(200) NOT NULL,
  `idmenupai` int(11) NOT NULL,
  `nrordem` int(11) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_menu`
--

LOCK TABLES `tb_menu` WRITE;
/*!40000 ALTER TABLE `tb_menu` DISABLE KEYS */;
INSERT INTO `tb_menu` VALUES (23,'Sistema','ico_application_home','',0,3,'0000-00-00 00:00:00',''),(24,'Gerir Menu','ico_Essen_config','openWindowDefault(#win.menu.js#)',23,1,'0000-00-00 00:00:00',''),(25,'','ico_Essen_refresh','function(){ window.location = #ajax/init.php# }',0,1,'0000-00-00 00:00:00',''),(26,'Permissoes','ico_key','openWindowDefault(#win.permissao.js#)',23,1,'2014-07-15 18:26:51',''),(27,'Clientes','ico_Essen_business-contact','',0,4,'2014-07-15 18:28:19','\0'),(28,'Imóveis','ico_contact_home','',0,5,'2014-07-15 18:28:40','\0'),(29,'Financeiro','ico_money_add','',0,6,'2014-07-15 18:29:06','\0'),(30,'Contas a Receber','ico_money_add','openWindowDefault(#win.contasareceber.js#)',29,0,'2014-07-15 18:29:51',''),(31,'Contas a Pagar','ico_money_add','openWindowDefault(#win.contasapagar.js#)',29,1,'2014-07-15 18:30:07',''),(32,'Gerenciar Locações','ico_file','openWindowDefault(#win.locacao-gerenciar.js#)',28,0,'2014-07-15 18:30:38',''),(33,'Gerenciar Imóveis','ico_Essen_home','openWindowDefault(#win.imovel-gerenciar.js#)',28,1,'2014-07-15 18:31:00',''),(34,'Gerenciar Clientes','ico_Essen_customers','openWindowDefault(#win.clientes.js#)',27,0,'2014-07-15 18:31:28',''),(35,'','ico_key','openWindowDefault(#win.senha.js#)',0,2,'2014-07-15 18:32:02',''),(36,'','ico_application_home','function(){ window.location = #default.php# }',0,0,'2014-07-15 18:32:32',''),(37,'Gerenciar Usuarios','ico_account-more','openWindowDefault(#win.usuario.js#)',23,1,'2014-07-15 18:33:26',''),(38,'Processar Retorno','ico_Essen_ticket','openWindowDefault(#win.retorno.js#)',29,1,'2014-07-27 11:27:40',''),(39,'Site','ico_Essen_world','',0,5,'2014-12-14 18:12:02','\0'),(40,'Gerir Paginas','ico_Essen_config','openWindowDefault(#win.gerenciar-paginas.js#)',39,1,'2014-12-14 18:12:20',''),(41,'Gerir Fale Conosco','ico_contact_mail','openWindowDefault(#win.faleconosco.js#)',39,2,'2014-12-14 20:24:14',''),(42,'Site','ico_Essen_world','',0,4,'2015-10-07 13:17:12',''),(43,'Gerenciar Paginas','ico_Essen_config','openWindowDefault(#win.adm-paginas.js#)',42,0,'2015-10-07 13:17:40',''),(44,'Gerenciar Fale Conosco','ico_Essen_hire-me','openWindowDefault(#win.faleconosco.js#)',42,1,'2015-10-07 13:18:43',''),(45,'Gerenciar Indicações','ico_Essen_donate','openWindowDefault(#win.relatorio.vendassite.js#)',42,1,'2015-10-07 13:19:05','\0'),(46,'Contrato','ico_Essen_archives','openWindowDefault(#win.contratosite.js#)',42,1,'2015-10-07 13:28:10',''),(47,'Formulario de Indicacao','ico_Essen_donate','openWindowDefault(#win.formulario-indicacao.js#)',42,1,'2015-10-07 22:06:58',''),(48,'Gerenciar Depoimentos','ico_Essen_billing','openWindowDefault(#win.depoimentos.js#)',42,1,'2015-10-07 22:26:59','');
/*!40000 ALTER TABLE `tb_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pagamentos`
--

DROP TABLE IF EXISTS `tb_pagamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pagamentos` (
  `idpagamento` int(11) NOT NULL AUTO_INCREMENT,
  `idpedido` int(11) DEFAULT NULL,
  `idformapagamento` int(11) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `idstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pagamentos`
--

LOCK TABLES `tb_pagamentos` WRITE;
/*!40000 ALTER TABLE `tb_pagamentos` DISABLE KEYS */;
INSERT INTO `tb_pagamentos` VALUES (1,29,1,'2015-10-02 16:51:00',2),(2,29,1,'2015-10-02 16:54:00',1),(3,30,1,'2015-10-02 17:15:00',1),(4,30,1,'2015-10-02 17:16:00',1),(5,30,1,'2015-10-02 17:20:00',1),(6,43,1,'2015-10-07 18:56:00',1);
/*!40000 ALTER TABLE `tb_pagamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paginas`
--

DROP TABLE IF EXISTS `tb_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paginas` (
  `idpagina` int(11) NOT NULL AUTO_INCREMENT,
  `despagina` varchar(100) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpagina`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paginas`
--

LOCK TABLES `tb_paginas` WRITE;
/*!40000 ALTER TABLE `tb_paginas` DISABLE KEYS */;
INSERT INTO `tb_paginas` VALUES (1,'home','','2015-10-05 18:50:16'),(2,'sobre-nos','','2015-10-05 18:50:16'),(3,'contato','','2015-10-05 18:50:16');
/*!40000 ALTER TABLE `tb_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pedidoambiente`
--

DROP TABLE IF EXISTS `tb_pedidoambiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pedidoambiente` (
  `idpedidoambiente` int(11) NOT NULL AUTO_INCREMENT,
  `idpedido` int(11) NOT NULL,
  `desambiente` varchar(200) DEFAULT NULL,
  `desmetragem` varchar(200) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  `desobservacao` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`idpedidoambiente`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pedidoambiente`
--

LOCK TABLES `tb_pedidoambiente` WRITE;
/*!40000 ALTER TABLE `tb_pedidoambiente` DISABLE KEYS */;
INSERT INTO `tb_pedidoambiente` VALUES (1,0,'1','1',NULL,'',NULL),(2,0,'2','2',NULL,'',NULL),(3,1,'1','1',NULL,'',NULL),(4,1,'2','2',NULL,'',NULL),(5,2,'1','1','2015-09-22 23:31:00','',NULL),(6,3,'asd','sad','2015-09-22 23:33:00','',NULL),(7,4,'Sala','100','2015-09-23 08:52:00','',NULL),(8,5,'Sala','10m²','2015-09-23 16:53:00','',NULL),(9,5,'Cozinha','5m²','2015-09-23 16:53:00','',NULL),(10,6,'Sala','10','2015-09-23 20:15:00','',NULL),(11,7,'teste','1','2015-09-26 20:42:00','',NULL),(12,8,'sala','7','2015-09-26 22:31:00','',NULL),(13,9,'Sala','10','2015-09-26 22:34:00','',NULL),(14,19,'sala','10','2015-09-26 22:49:00','',NULL),(15,19,'Cozinha','20','2015-09-26 22:49:00','',NULL),(16,20,'sala','10','2015-09-26 22:49:00','',NULL),(17,20,'Cozinha','20','2015-09-26 22:49:00','',NULL),(18,21,'Sala','10','2015-09-27 22:15:00','',NULL),(19,22,'Sala','20','2015-09-27 23:32:00','',NULL),(20,22,'varanda','5','2015-09-27 23:32:00','',NULL),(21,23,'sala','10','2015-09-27 23:35:00','',NULL),(22,23,'sala','12','2015-09-27 23:35:00','',NULL),(23,24,'sala','20','2015-09-28 21:14:00','',NULL),(24,25,'sala','20','2015-09-28 21:36:00','',NULL),(25,25,'cozinha','15','2015-09-28 21:36:00','',NULL),(26,26,'sala','20','2015-09-28 22:04:00','',NULL),(27,27,'sala','20','2015-09-28 22:16:00','',NULL),(28,28,'sala','10','2015-10-02 12:13:00','',''),(29,29,'sala','10','2015-10-02 16:48:00','','teste'),(30,30,'sala','20','2015-10-02 17:15:00','',''),(31,0,'ad','12','2015-10-03 22:39:00','',NULL),(32,0,'ad','12','2015-10-03 22:42:00','',NULL),(33,0,'ad','12','2015-10-03 22:45:00','',NULL),(34,31,'ad','12','2015-10-03 22:48:00','',NULL),(35,32,'SALA','1','2015-10-04 00:11:00','',NULL),(36,33,'sala','10','2015-10-04 00:13:00','',NULL),(37,34,'sala','10','2015-10-04 00:13:00','',NULL),(38,35,'sala','10','2015-10-04 00:13:00','',NULL),(39,36,'sala','10','2015-10-04 00:14:00','',NULL),(40,37,'sala','10','2015-10-04 00:14:00','',NULL),(41,38,'sala','10','2015-10-04 00:16:00','',NULL),(42,39,'sala','10','2015-10-04 00:19:00','',NULL),(43,40,'sals','10','2015-10-04 00:23:00','',NULL),(44,41,'sala','10','2015-10-07 18:19:00','',NULL),(45,42,'sala','10','2015-10-07 18:21:00','',NULL),(46,43,'sala','10','2015-10-07 18:55:00','','sad'),(47,44,'sala','20','2015-10-07 21:17:00','',NULL);
/*!40000 ALTER TABLE `tb_pedidoambiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pedidoarquivos`
--

DROP TABLE IF EXISTS `tb_pedidoarquivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pedidoarquivos` (
  `idarquivo` int(11) NOT NULL AUTO_INCREMENT,
  `idpedido` int(11) DEFAULT NULL,
  `idambiente` int(11) DEFAULT NULL,
  `desarquivo` varchar(200) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idarquivo`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pedidoarquivos`
--

LOCK TABLES `tb_pedidoarquivos` WRITE;
/*!40000 ALTER TABLE `tb_pedidoarquivos` DISABLE KEYS */;
INSERT INTO `tb_pedidoarquivos` VALUES (9,25,24,'paypal-banner.jpg','2015-09-28 21:40:00',''),(10,25,24,'paypal-logo.png','2015-09-28 21:40:00',''),(12,28,28,'paypal-logo.png','2015-10-02 12:41:00',''),(14,29,29,'paypal-logo.png','2015-10-02 16:49:00','');
/*!40000 ALTER TABLE `tb_pedidoarquivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pedidos`
--

DROP TABLE IF EXISTS `tb_pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pedidos` (
  `idpedido` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  `idplano` int(11) DEFAULT NULL,
  `vlpedido` decimal(10,2) DEFAULT NULL,
  `idendereco` int(11) DEFAULT NULL,
  `desip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpedido`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pedidos`
--

LOCK TABLES `tb_pedidos` WRITE;
/*!40000 ALTER TABLE `tb_pedidos` DISABLE KEYS */;
INSERT INTO `tb_pedidos` VALUES (1,15,NULL,'',1,NULL,NULL,NULL),(2,16,'2015-09-22 23:31:00','',1,NULL,NULL,NULL),(3,17,'2015-09-22 23:33:00','',1,NULL,NULL,NULL),(4,15,'2015-09-23 08:52:00','',1,NULL,NULL,NULL),(5,18,'2015-09-23 16:53:00','',3,NULL,NULL,NULL),(6,19,'2015-09-23 20:15:00','',1,NULL,NULL,NULL),(7,15,'2015-09-26 20:42:00','',2,NULL,NULL,NULL),(8,15,'2015-09-26 22:31:00','',1,0.00,NULL,NULL),(9,15,'2015-09-26 22:34:00','',1,95.00,NULL,NULL),(10,15,'2015-09-26 22:41:00','',1,255.00,NULL,NULL),(11,15,'2015-09-26 22:45:00','',1,255.00,NULL,NULL),(12,15,'2015-09-26 22:45:00','',1,255.00,NULL,NULL),(13,15,'2015-09-26 22:46:00','',1,255.00,NULL,NULL),(14,15,'2015-09-26 22:47:00','',1,255.00,NULL,NULL),(15,15,'2015-09-26 22:48:00','',1,255.00,NULL,NULL),(16,15,'2015-09-26 22:48:00','',1,255.00,NULL,NULL),(17,15,'2015-09-26 22:48:00','',1,255.00,NULL,NULL),(18,15,'2015-09-26 22:49:00','',1,255.00,NULL,NULL),(19,15,'2015-09-26 22:49:00','',1,255.00,NULL,NULL),(20,15,'2015-09-26 22:49:00','',1,255.00,NULL,NULL),(21,15,'2015-09-27 22:15:00','',1,95.00,NULL,NULL),(22,15,'2015-09-27 23:32:00','',2,510.00,NULL,NULL),(23,15,'2015-09-27 23:35:00','',2,360.00,NULL,NULL),(24,15,'2015-09-28 21:14:00','',1,180.00,NULL,NULL),(25,15,'2015-09-28 21:36:00','',2,680.00,NULL,NULL),(26,15,'2015-09-28 22:04:00','',2,360.00,NULL,NULL),(27,15,'2015-09-28 22:16:00','',1,180.00,NULL,NULL),(28,15,'2015-10-02 12:13:00','',1,95.00,4,'::1'),(29,20,'2015-10-02 16:48:00','',1,95.00,5,'::1'),(30,20,'2015-10-02 17:15:00','',1,180.00,6,'::1'),(31,21,'2015-10-03 22:48:00','',1,100.00,NULL,'::1'),(32,0,'2015-10-04 00:11:00','',1,100.00,NULL,'::1'),(33,22,'2015-10-04 00:13:00','',1,100.00,NULL,'::1'),(34,22,'2015-10-04 00:13:00','',1,100.00,NULL,'::1'),(35,22,'2015-10-04 00:13:00','',1,100.00,NULL,'::1'),(36,22,'2015-10-04 00:14:00','',1,100.00,NULL,'::1'),(37,22,'2015-10-04 00:14:00','',1,100.00,NULL,'::1'),(38,22,'2015-10-04 00:16:00','',1,100.00,NULL,'::1'),(39,22,'2015-10-04 00:19:00','',1,100.00,NULL,'::1'),(40,22,'2015-10-04 00:23:00','',1,100.00,NULL,'::1'),(41,2,'2015-10-07 18:19:00','',4,100.00,NULL,'::1'),(42,3,'2015-10-07 18:21:00','',4,100.00,NULL,'::1'),(43,3,'2015-10-07 18:55:00','',4,100.00,7,'::1'),(44,3,'2015-10-07 21:17:00','',4,190.00,NULL,'::1');
/*!40000 ALTER TABLE `tb_pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_permissao`
--

DROP TABLE IF EXISTS `tb_permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permissao` (
  `idpermissao` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idpermissao`),
  KEY `idmenu_idx` (`idmenu`),
  KEY `idusuario_idx` (`idusuario`),
  CONSTRAINT `fk_permissao_menu` FOREIGN KEY (`idmenu`) REFERENCES `tb_menu` (`idmenu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permissao_usuario` FOREIGN KEY (`idusuario`) REFERENCES `tb_usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_permissao`
--

LOCK TABLES `tb_permissao` WRITE;
/*!40000 ALTER TABLE `tb_permissao` DISABLE KEYS */;
INSERT INTO `tb_permissao` VALUES (42,23,13),(43,24,13),(44,25,13),(45,26,13),(48,37,13),(49,27,13),(50,34,13),(51,28,13),(52,32,13),(53,33,13),(54,29,13),(55,30,13),(56,31,13),(57,38,13),(58,39,13),(59,40,13),(60,41,13),(62,35,13),(63,36,13),(64,42,13),(65,43,13),(66,44,13),(67,45,13),(68,46,13),(69,47,13),(70,48,13);
/*!40000 ALTER TABLE `tb_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoas`
--

DROP TABLE IF EXISTS `tb_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoas` (
  `idpessoa` int(11) NOT NULL AUTO_INCREMENT,
  `despessoa` varchar(200) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descidade` varchar(200) DEFAULT NULL,
  `desestado` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`idpessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoas`
--

LOCK TABLES `tb_pessoas` WRITE;
/*!40000 ALTER TABLE `tb_pessoas` DISABLE KEYS */;
INSERT INTO `tb_pessoas` VALUES (1,'Luiz Magno','','2015-10-05 18:46:27',NULL,NULL),(2,'bruno','','2015-10-07 21:19:00','Guaurlhos','SP'),(3,'bruno','','2015-10-07 21:21:00','Guaurlhos','SP');
/*!40000 ALTER TABLE `tb_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_planos`
--

DROP TABLE IF EXISTS `tb_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_planos` (
  `idplano` int(11) NOT NULL AUTO_INCREMENT,
  `desplano` varchar(45) DEFAULT NULL,
  `vlplano` decimal(10,2) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `desinformacao` varchar(1000) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idplano`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_planos`
--

LOCK TABLES `tb_planos` WRITE;
/*!40000 ALTER TABLE `tb_planos` DISABLE KEYS */;
INSERT INTO `tb_planos` VALUES (4,'Plano 1',100.00,'teste','teste 2',NULL,''),(5,'Plano 2',200.00,'teste','teste',NULL,''),(6,'Plano 3',300.00,'teste','teste',NULL,'');
/*!40000 ALTER TABLE `tb_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_relacao_grupomenu`
--

DROP TABLE IF EXISTS `tb_relacao_grupomenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_relacao_grupomenu` (
  `idrelacao` int(11) NOT NULL AUTO_INCREMENT,
  `idgrupo` int(11) NOT NULL,
  `idmenu` int(11) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  PRIMARY KEY (`idrelacao`),
  KEY `fk_relacao_menu_idx` (`idmenu`),
  KEY `fk_relacao_grupo_idx` (`idgrupo`),
  CONSTRAINT `fk_relacaogrupomenu_grupo` FOREIGN KEY (`idgrupo`) REFERENCES `tb_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_relacaogrupomenu_menu` FOREIGN KEY (`idmenu`) REFERENCES `tb_menu` (`idmenu`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_relacao_grupomenu`
--

LOCK TABLES `tb_relacao_grupomenu` WRITE;
/*!40000 ALTER TABLE `tb_relacao_grupomenu` DISABLE KEYS */;
INSERT INTO `tb_relacao_grupomenu` VALUES (52,5,36,'2015-10-07 00:00:00'),(53,5,25,'2015-10-07 00:00:00'),(54,5,35,'2015-10-07 00:00:00'),(55,5,23,'2015-10-07 00:00:00'),(56,5,42,'2015-10-07 00:00:00'),(57,5,43,'2015-10-07 00:00:00'),(58,5,44,'2015-10-07 00:00:00'),(59,5,46,'2015-10-07 00:00:00'),(60,5,47,'2015-10-07 00:00:00'),(61,5,48,'2015-10-07 00:00:00');
/*!40000 ALTER TABLE `tb_relacao_grupomenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_relacao_grupousuario`
--

DROP TABLE IF EXISTS `tb_relacao_grupousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_relacao_grupousuario` (
  `idrelacao` int(11) NOT NULL AUTO_INCREMENT,
  `idgrupo` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  PRIMARY KEY (`idrelacao`),
  KEY `fk_relacao_usuario_idx` (`idusuario`),
  KEY `fk_relacaogrupousuario_grupo_idx` (`idgrupo`),
  CONSTRAINT `fk_relacaogrupousuario_grupo` FOREIGN KEY (`idgrupo`) REFERENCES `tb_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_relacaogrupousuario_usuario` FOREIGN KEY (`idusuario`) REFERENCES `tb_usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_relacao_grupousuario`
--

LOCK TABLES `tb_relacao_grupousuario` WRITE;
/*!40000 ALTER TABLE `tb_relacao_grupousuario` DISABLE KEYS */;
INSERT INTO `tb_relacao_grupousuario` VALUES (6,5,13,'2015-01-04 00:00:00');
/*!40000 ALTER TABLE `tb_relacao_grupousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_sitecontatos`
--

DROP TABLE IF EXISTS `tb_sitecontatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sitecontatos` (
  `idsitecontato` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `idpessoaindicacao` int(11) DEFAULT NULL,
  `desmensagem` varchar(500) NOT NULL,
  `desassunto` varchar(200) DEFAULT NULL,
  `idsitecontatotipo` int(11) NOT NULL,
  `iscopia` bit(1) NOT NULL DEFAULT b'0',
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idsitecontato`),
  KEY `FK_sitecontatos_pessoas_idpessoa` (`idpessoa`),
  KEY `FK_sitecontatos_pessoas_idpessoaindicacao` (`idpessoaindicacao`),
  KEY `FK_sitecontatos_sitecontatotipos_idsitecontatotipo` (`idsitecontatotipo`),
  CONSTRAINT `FK_sitecontatos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`),
  CONSTRAINT `FK_sitecontatos_pessoas_idpessoaindicacao` FOREIGN KEY (`idpessoaindicacao`) REFERENCES `tb_pessoas` (`idpessoa`),
  CONSTRAINT `FK_sitecontatos_sitecontatotipos_idsitecontatotipo` FOREIGN KEY (`idsitecontatotipo`) REFERENCES `tb_sitecontatotipos` (`idsitecontatotipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_sitecontatos`
--

LOCK TABLES `tb_sitecontatos` WRITE;
/*!40000 ALTER TABLE `tb_sitecontatos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_sitecontatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_sitecontatotipos`
--

DROP TABLE IF EXISTS `tb_sitecontatotipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sitecontatotipos` (
  `idsitecontatotipo` int(11) NOT NULL AUTO_INCREMENT,
  `dessitecontatotipo` varchar(200) NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idsitecontatotipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_sitecontatotipos`
--

LOCK TABLES `tb_sitecontatotipos` WRITE;
/*!40000 ALTER TABLE `tb_sitecontatotipos` DISABLE KEYS */;
INSERT INTO `tb_sitecontatotipos` VALUES (1,'Contato','','2015-10-05 18:46:00'),(2,'Indicação','','2015-10-05 18:46:00');
/*!40000 ALTER TABLE `tb_sitecontatotipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_status`
--

DROP TABLE IF EXISTS `tb_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `desstatus` varchar(45) DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_status`
--

LOCK TABLES `tb_status` WRITE;
/*!40000 ALTER TABLE `tb_status` DISABLE KEYS */;
INSERT INTO `tb_status` VALUES (1,'Em Andamento',''),(2,'Em Analise',''),(3,'Confirmado',''),(4,'Cancelado','');
/*!40000 ALTER TABLE `tb_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_transacoes`
--

DROP TABLE IF EXISTS `tb_transacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_transacoes` (
  `idtransacao` int(11) NOT NULL AUTO_INCREMENT,
  `idpagamento` varchar(45) DEFAULT NULL,
  `transactionid` varchar(200) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`idtransacao`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_transacoes`
--

LOCK TABLES `tb_transacoes` WRITE;
/*!40000 ALTER TABLE `tb_transacoes` DISABLE KEYS */;
INSERT INTO `tb_transacoes` VALUES (1,'1','1','2015-10-07 17:39:00'),(2,'1','1','2015-10-07 17:39:00'),(3,'1','1','2015-10-07 17:40:00'),(4,'1','1','2015-10-07 17:41:00'),(5,'1','1','2015-10-07 17:42:00'),(6,'1','1','2015-10-07 17:42:00'),(7,'1','1','2015-10-07 17:43:00');
/*!40000 ALTER TABLE `tb_transacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `desusuario` varchar(30) NOT NULL,
  `desemail` varchar(100) NOT NULL,
  `dessenha` varchar(10) NOT NULL,
  `dtcadastro` datetime NOT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` VALUES (13,'adm','adm@indeboxadmin.com','123456','2014-07-15 00:00:00',''),(14,'bruno','bruno2@fastcode.com.br','123','2015-01-06 20:47:26','\0');
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-07 22:35:18

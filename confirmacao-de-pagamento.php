<?php
require_once("inc/configuration.php");

if(!isset($_SESSION["idpagamento"])){
    header("Location: design-online.php");
}

$idpedido = $_SESSION["idpedido"];
$idpagamento = $_SESSION["idpagamento"];

$pedido = new Pedido((int)$idpedido);
$pessoa = new Pessoa($pedido->getidpessoa());

if(get("transaction_id")){
    $transaction_id = get("transaction_id");
    $gateway = "PagSeguro";
} else {
    $transaction_id = get("token");
    $gateway = "PayPal";
}


$pagamento = new Pagamento(array(
    "idpagamento"=>$idpagamento,
    "idstatus"=>2,
    "transactionid"=>$transaction_id
));

$pagamento->saveStatus();
$pagamento->saveTransactId();

$page = new Page(array(
    "css" => true,
    "data" => array(
        "head_title" => 'Indebox - Consultoria técnica'
    )
));

$page->setTpl("confirmacao-de-pagamento",array(
    "descodigo"=>$transaction_id,
    "despessoa"=>utf8_encode($pessoa->getdespessoa()),
    "vlpedido"=>number_format($pedido->getvlpedido(),2,",","."),
    "year"=>date("Y",strtotime($pedido->getdtcadastro())),
    "idpedido"=>$idpedido,
    "desoperadora"=>$gateway,
));

?>
<?php

class Contato {
    private $idcontato;
    private $idpessoa;
    private $descontato;
    private $idcontatotipo;
    private $instatus;

    public function setIdcontato($idcontato) {
        $this->idcontato = $idcontato;
    }

    public function getIdcontato() {
        return $this->idcontato;
    }

    public function setIdpessoa($idpessoa) {
        $this->idpessoa = $idpessoa;
    }

    public function getIdpessoa() {
        return $this->idpessoa;
    }

    public function setDescontato($descontato) {
        $this->descontato = $descontato;
    }

    public function getDescontato() {
        return $this->descontato;
    }

    public function setIdcontatotipo($idcontatotipo) {
        $this->idcontatotipo = $idcontatotipo;
    }

    public function getIdcontatotipo() {
        return $this->idcontatotipo;
    }

    public function setInstatus($instatus) {
        $this->instatus = $instatus;
    }

    public function getInstatus() {
        return $this->instatus;
    }

    public function __construct($contato) {
        if ( is_array($contato) ) {
            $this->setIdcontato($contato['idcontato']);
            $this->setIdpessoa($contato['idpessoa']);
            $this->setDescontato($contato['descontato']);
            $this->setIdcontatotipo($contato['idcontatotipo']);
            $this->setInstatus($contato['instatus']);
        }
        else if ( is_numeric((int)$contato) && (int)$contato > 0 ){
            $Sql = new Sql();

            $contato = $Sql->arrays("call sp_contato_get($contato)", true);

            if ( count($contato) != 0 ) {
                $this->setIdcontato($contato['idcontato']);
                $this->setIdpessoa($contato['idpessoa']);
                $this->setDescontato($contato['descontato']);
                $this->setIdcontatotipo($contato['idcontatotipo']);
                $this->setInstatus($contato['instatus']);
            }
        }
    }

    public function save() {
        $Sql = new Sql();

        $idcontato = $Sql->arrays("call sp_contato_save(".$this->getIdcontato().",".$this->getIdpessoa().",'".$this->getDescontato()."',".$this->getIdcontatotipo().",".$this->getInstatus().")", true);

        return $idcontato;
    }

    public function saveSiteContato( $idpessoa, $idpessoaindicacao, $desmensagem, $desassunto, $idsitecontatotipo, $iscopia, $instatus ) {
        $Sql = new Sql();

        $idsitecontato = $Sql->arrays("call sp_sitecontato_save($idpessoa, $idpessoaindicacao, '$desmensagem', '$desassunto', $idsitecontatotipo, $iscopia, $instatus)", true);

        return $idsitecontato;
    }
}
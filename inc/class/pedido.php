<?php
class Pedido extends Model{

    public function __construct($data = array()){
        if(gettype($data) == "integer" || gettype($data) == "string"){
            $this->setidpedido($data);
            return $this->get();
        } else if(gettype($data) == "array") {
            return parent::__construct($data);
        } else {
            return false;
        }
    }

    public function get(){
        $sql = new Sql();        
        return parent::__construct($sql->select("select * from tb_pedidos where idpedido = ".$this->getidpedido()));
    }
    public function remove(){}

    public function save(){
    	$sql = new Sql();
    	$sql->insert("insert into tb_pedidos(idpessoa, idplano, dtcadastro, vlpedido, desip) values(?, ?, ?, ?, ?)",array(
            $this->getidpessoa(),
            $this->getidplano(),
            date("Y-m-d H:i"),
            $this->getvlpedido(),
            $_SERVER["REMOTE_ADDR"]
        ));
        $this->setidpedido($sql->last_id());
    }

    public function editPedido(){
        $sql = new Sql();
        $sql->query("update tb_pedidos set idendereco = ".$this->getidendereco()." where idpedido = ".$this->getidpedido());
    }

    public function editPedidoAmbiente(){
        $sql = new Sql();
        $sql->query("update tb_pedidoambiente set desobservacao = '".nl2br($this->getdesobservacao())."' where idpedidoambiente = ".$this->getidambiente());
    }

    public function getPlano(){
        $sql = new Sql();
        return $sql->select("select * from tb_planos where idplano = ".$this->getidplano());
    }

    public function saveAmbientes(){
        foreach ($this->getambiente() as $val) {
            $sql = new Sql();
    		$sql->insert("insert into tb_pedidoambiente(idpedido, desambiente, desmetragem, dtcadastro) values(?, ?, ?, ?)",array(
		        $this->getidpedido(),
                utf8_encode($val["desambiente"]),
		        $val["desmetragem"],
            	date("Y-m-d H:i")
		    ));		
    	}    	
    }

    public function getAmbientes(){
        $sql = new Sql();
        $data = array();
        foreach($sql->arrays("select * from tb_pedidoambiente where idpedido = ".$this->getidpedido()) as $val){
            array_push($data, array(
                "idpedidoambiente"=>$val["idpedidoambiente"],
                "desambiente"=>utf8_decode($val["desambiente"]),
                "desmetragem"=>$val["desmetragem"],
                "fotos"=>$this->getArquivo($val["idpedidoambiente"])
            ));
        }
        return $data;
    }

    public function saveArquivo(){
        $sql = new Sql();
        $sql->insert("insert into tb_pedidoarquivos(idpedido, desarquivo, dtcadastro) values(?, ?, ?)",array(
            $this->getidpedido(),
            $this->getdesarquivo(),
            date("Y-m-d H:i")
        ));
        //
        $this->setidarquivo($sql->last_id());
    }

    public function editArquivo(){
        $sql = new Sql();
        $sql->query("update tb_pedidoarquivos set idambiente = ".$this->getidambiente()." where idarquivo = ".$this->getidarquivo());
    }

    public function getArquivo($idambiente){
        $sql = new Sql();
        return $sql->arrays("select * from tb_pedidoarquivos where idambiente = ".$idambiente);
    }

    public function removeArquivo(){
        $sql = new Sql();
        $sql->query("delete from tb_pedidoarquivos where idarquivo = ".$this->getidarquivo());
    }


}

?>
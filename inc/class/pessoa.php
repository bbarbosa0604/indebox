<?php

    class Pessoa extends Model {
        public function __construct($data = array()){
            if(gettype($data) == "integer" || gettype($data) == "string"){
                $this->setidpessoa($data);
                return $this->get();
            } else if(gettype($data) == "array") {
                return parent::__construct($data);
            } else {
                return false;
            }
        }

        public function get(){ 
            $sql = new Sql();        
            return parent::__construct($sql->select("select * from tb_pessoas where idpessoa = ".$this->getidpessoa()));
        }

        public function getByEmail(){
//
//            $sql = new Sql();
//            $query = $sql->select("select * from tb_pessoas a inner join tb_contatos b on a.idpessoa = b.idpessoa where descontato = '".$this->getdesemail()."'");
//
//            if(count($query) > 0){
//
//                $this->setidpessoa($query["idpessoa"]);
//
//                return true;
//
//            } else {
//
//                return false;
//
//            }
            return false;
        }

        public function remove(){}
        public function save(){
            $sql = new Sql();
            $sql->insert("insert into tb_pessoas(despessoa, descidade, desestado, dtcadastro) values(?, ?, ?, ?)",array(
                $this->getdespessoa(),
                $this->getdescidade(),
                $this->getdesestado(),
                date("Y-m-d H:i")
            ));
            //
            $this->setidpessoa($sql->last_id());
        }

        public function isExistsContato($descontato, $idcontatotipo){
            $sql = new Sql();
            $a = $sql->select("select * from tb_contatos where idpessoa = ".$this->getidpessoa()." and descontato = '".$descontato."' and idcontatotipo = ".$idcontatotipo);            
            //
            if(count($a) > 0){
                $this->setidcontato($a["idcontato"]);
                return true;
            } else {
                return false;
            }
        }

        public function savecontatos(){
            foreach ($this->getcontato() as $val) {
                $sql = new Sql();
                
                if($this->isExistsContato($val["descontato"], $val["idcontatotipo"])){
                    $sql->query("update tb_contatos set descontato = '".$val["descontato"]."' where idcontato = ".$this->getidcontato());
                } else {
                    $sql->insert("insert into tb_contatos(idpessoa, descontato, idcontatotipo, dtcadastro) values(?, ?, ?, ?)",array(
                        $this->getidpessoa(),
                        $val["descontato"],
                        $val["idcontatotipo"],
                        date("Y-m-d H:i")
                    ));
                }
                
            }
            
        }

        public function getContatos(){
            $sql = new Sql();
            return $sql->arrays("select * from tb_contatos where idpessoa = ".$this->getidpessoa());
        }

        public function isExistsDocumento($desdocumento, $iddocumentotipo){
            $sql = new Sql();
            $a = $sql->select("select * from tb_documentos where idpessoa = ".$this->getidpessoa()." and desdocumento = '".$desdocumento."' and iddocumentotipo = ".$iddocumentotipo);            
            //
            if(count($a) > 0){
                $this->setiddocumento($a["iddocumento"]);
                return true;
            } else {
                return false;
            }
        }

        public function saveDocumentos(){
            
            foreach ($this->getdocumento() as $val) {
                $sql = new Sql();
                if($this->isExistsDocumento($val["desdocumento"], $val["iddocumentotipo"])){
                    $sql->query("update tb_documentos set desdocumento = '".$val["desdocumento"]."' where iddocumento = ".$this->getiddocumento());
                } else {
                    $sql->insert("insert into tb_documentos(idpessoa, desdocumento, iddocumentotipo, dtcadastro) values(?, ?, ?, ?)",array(
                        $this->getidpessoa(),
                        $val["desdocumento"],
                        $val["iddocumentotipo"],
                        date("Y-m-d H:i")
                    ));
                }
            }
            
        }

        public function getDocumentos(){
            $sql = new Sql();
            return $sql->arrays("select * from tb_documentos where idpessoa = ".$this->getidpessoa());
        }

        public function isExistsEndereco(){
            $sql = new Sql();
            $a = $sql->select("select * from tb_endereco where idpessoa = ".$this->getidpessoa()." and descep = '".$this->getdescep()."'");            
            //
            if(count($a) > 0){
                $this->setidendereco($a["idendereco"]);
                return true;
            } else {
                return false;
            }
        }

        public function saveEndereco(){
            $sql = new Sql();
            
            if($this->isExistsEndereco()==false){
                $sql->insert("insert into tb_endereco(idpessoa, desendereco, desnumero, descomplemento, desbairro, descidade, desestado, descep, dtcadastro) values(?, ?, ?, ?, ?, ?, ?, ?, ?)",array(
                    $this->getidpessoa(),
                    $this->getdesendereco(),
                    $this->getdesnumero(),
                    $this->getdescomplemento(),
                    $this->getdesbairro(),
                    $this->getdescidade(),
                    $this->getdesestado(),
                    $this->getdescep(),
                    date("Y-m-d H:i")
                ));
                $this->setidendereco($sql->last_id());
            } else {
                $sql->query("update tb_endereco set desendereco = '".$this->getdesendereco()."', desnumero = '".$this->getdesnumero()."', descomplemento = '".$this->getdescomplemento()."', desbairro = '".$this->getdesbairro()."', descidade = '". $this->getdescidade()."', desestado = '".$this->getdesestado()."', descep = '".$this->getdescep()."' where idendereco = ".$this->getidendereco());
            }
        }
    }
?>
<?php
class Pagamento extends Model{

    public function __construct($data = array()){
        if(gettype($data) == "integer" || gettype($data) == "string"){
            $this->setidpagamento($data);
            return $this->get();
        } else if(gettype($data) == "array") {
            return parent::__construct($data);
        } else {
            return false;
        }
    }

    public function get(){
        $sql = new Sql();        
        return parent::__construct($sql->select("select * from tb_pagamentos where idpedido = ".$this->getidpagamento()));
    }
    public function remove(){}

    public function save(){
    	$sql = new Sql();
    	$sql->insert("insert into tb_pagamentos(idpedido, idformapagamento, dtcadastro, idstatus) values(?, ?, ?, ?)",array(
            $this->getidpedido(),
            $this->getidformapagamento(),
            date("Y-m-d H:i"),
            $this->getidstatus()
        ));
        $this->setidpagamento($sql->last_id());
    }

    public function saveStatus(){
        $sql = new Sql();
    	$sql->query("update tb_pagamentos set idstatus = ".$this->getidstatus()." where idpagamento = ".$this->getidpagamento());
    }

    public function saveTransactId(){
        $sql = new Sql();

        $sql->insert("insert into tb_transacoes(idpagamento, transactionid, dtcadastro) values(?, ?, ?)",array(
            $this->getidpagamento(),
            $this->gettransactionid(),
            date("Y-m-d H:i")
        ));
    }

    public function getTransactId(){
        $sql = new Sql();
        $query = $sql->select("select * from tb_transacoes where transactionid = '".$this->gettransactionid()."'");

        if(count($query) > 0){
            
            $this->setidpagamento($query["idpagamento"]);

            return true;

        } else {

            return false;

        }
    }
}

?>
<?php
class Page {
  	
  	private $Tpl;
	public $options = array(
		"strings"=>"pt-br",//Idioma padrão res/strings/pt-br.xml
		"data"=>array(
			"js"=>array(),
			"head_title"=>"Indebox",
			"meta_description"=>"",
			"meta_author"=>"Luiz Magno"
		)
	);
 
	public function __construct($options = array()){

		$rootdir = PATH;

		raintpl::configure("base_url", $rootdir );
		raintpl::configure("tpl_dir", $rootdir."/res/tpl/" );
		raintpl::configure("cache_dir", $rootdir."/res/tpl/tmp/" );
		raintpl::configure("path_replace", false );

		$options = array_merge($this->options, $options);

		if(isset($_SESSION["lang"])) $options["strings"] = $_SESSION["lang"];
 	
		$options['data']['string'] = $this->loadString($options["strings"]);

		$options['data']['css'] = (isset($options['css'])) ? $this->setCss() : '';

		$tpl = $this->getTpl();
		$this->options = $options;

		if(gettype($this->options['data'])=='array'){
			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
		
		$tpl->draw("header", false);
 
	}

	public function loadString($lang){

		$file_string = PATH."/res/strings/$lang.xml";

		$strings = array();

		if(file_exists($file_string)){

			$xml = simplexml_load_file($file_string);

			foreach($xml->children() as $string){

				$strings[(string)$string->attributes()[0]] = (string)$string;

			}

		}

		return $strings;

	}
	
	public function getString($name){

		$strings = $this->loadString($options["strings"]);
		return $strings[$name];

	}
 
	public function __destruct(){
 
		$tpl = $this->getTpl();
 		
		$this->options["data"]["servicos"] = $this->getServicos();

		if(gettype($this->options['data'])=='array'){
			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 
		$tpl->draw("footer", false);
 
	}
 
	public function setTpl($tplname, $data = array(), $returnHTML = false){
 
		$tpl = $this->getTpl();

		$data["servicos"] = $this->getServicos();
 
		if(gettype($data)=='array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 
		return $tpl->draw($tplname, $returnHTML);
 
	}
  
	public function getTpl(){
 
		return ($this->Tpl)?$this->Tpl:$this->Tpl = new RainTPL;
 
	}

	private function setCss(){
		$linkcss = "res/css/".str_replace('.php','.css',basename($_SERVER['PHP_SELF']));
		echo '<link href="'.$linkcss.'" rel="stylesheet">';
	} 

	private function getServicos() {
		$Sql = new Sql();

		$s = $Sql->arrays("select idservico, desservico, desdescricao, desurl, desicon, instatus from tb_servicos where instatus = 1;");

		$servicos = array();

		foreach ($s as $servico) {

			array_push($servicos, array(
				"idservico" => $servico['idservico'],
				"desservico" => utf8_encode($servico['desservico']),
				"desdescricao" => utf8_encode($servico['desdescricao']),
				"desurl" => utf8_encode($servico['desurl']),
				"desicon" => utf8_encode($servico['desicon']),
				"instatus" => $servico['instatus']
			));
		}

		return $servicos;
	}
}
?>

<?php
/**
 * 2007-2014 [PagSeguro Internet Ltda.]
 *
 * NOTICE OF LICENSE
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 *  @author    PagSeguro Internet Ltda.
 *  @copyright 2007-2014 PagSeguro Internet Ltda.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 */

$PagSeguroConfig = array();

$PagSeguroConfig['environment'] = "production"; // production, sandbox

$PagSeguroConfig['credentials'] = array();
$PagSeguroConfig['credentials']['email'] = "design@indebox.com.br";
$PagSeguroConfig['credentials']['token']['production'] = "65F83A03516D4618B5467203F94CEC61";
$PagSeguroConfig['credentials']['token']['sandbox'] = "9D5842ECD0094D54A7C18B0CFB4E5C65";
$PagSeguroConfig['credentials']['appId']['production'] = "indebox";
$PagSeguroConfig['credentials']['appId']['sandbox'] = "app0406655871";
$PagSeguroConfig['credentials']['appKey']['production'] = "21FDED472727180DD492AFADFD16A2ED";
$PagSeguroConfig['credentials']['appKey']['sandbox'] = "4759CEB30B0BF3477475EF926686E498";

$PagSeguroConfig['application'] = array();
$PagSeguroConfig['application']['charset'] = "UTF-8"; // UTF-8, ISO-8859-1

$PagSeguroConfig['log'] = array();
$PagSeguroConfig['log']['active'] = false;
// Informe o path completo (relativo ao path da lib) para o arquivo, ex.: ../PagSeguroLibrary/logs.txt
$PagSeguroConfig['log']['fileLocation'] = "";

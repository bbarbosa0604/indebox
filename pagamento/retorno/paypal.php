<?
require_once("../../inc/configuration.php");

if ( isset( $_GET[ 'token' ] ) ) {
	$token = $_GET[ 'token' ];
	$nvp = array(
		'TOKEN'								=> $token,
		'METHOD'							=> 'GetExpressCheckoutDetails',
		'VERSION'							=> '84',
		'PWD'								=> 'RR5N89APLN6Y3ZE7',
		'USER'								=> 'design_api1.indebox.com.br',
		'SIGNATURE'							=> 'A2u9W-Ta6WmZbLqUhSN.UHMdA6v4AWUsS4Y3PhXWCt8nmMflD9olGNZk'
	);
	$curl = curl_init();
	curl_setopt( $curl , CURLOPT_URL , 'https://api-3t.paypal.com/nvp' ); //Link para ambiente de teste: https://api-3t.sandbox.paypal.com/nvp
	curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );
	curl_setopt( $curl , CURLOPT_RETURNTRANSFER , 1 );
	curl_setopt( $curl , CURLOPT_POST , 1 );
	curl_setopt( $curl , CURLOPT_POSTFIELDS , http_build_query( $nvp ) );
	$response = urldecode( curl_exec( $curl ) );
	$responseNvp = array();
	if ( preg_match_all( '/(?<name>[^\=]+)\=(?<value>[^&]+)&?/' , $response , $matches ) ) {
		foreach ( $matches[ 'name' ] as $offset => $name ) {
			$responseNvp[ $name ] = $matches[ 'value' ][ $offset ];
		}
	}
	if ( isset( $responseNvp[ 'TOKEN' ] ) && isset( $responseNvp[ 'ACK' ] ) ) {
		if ( $responseNvp[ 'TOKEN' ] == $token && $responseNvp[ 'ACK' ] == 'Success' ) {
			$nvp[ 'PAYERID' ]							= $responseNvp[ 'PAYERID' ];
			$nvp[ 'PAYMENTREQUEST_0_AMT' ]				= $responseNvp[ 'PAYMENTREQUEST_0_AMT' ];
			$nvp[ 'PAYMENTREQUEST_0_CURRENCYCODE' ]		= $responseNvp[ 'PAYMENTREQUEST_0_CURRENCYCODE' ];
			$nvp[ 'METHOD' ]							= 'DoExpressCheckoutPayment';
			$nvp[ 'PAYMENTREQUEST_0_PAYMENTACTION' ]	= 'Sale';
			curl_setopt( $curl , CURLOPT_POSTFIELDS , http_build_query( $nvp ) );
			$response = urldecode( curl_exec( $curl ) );
			$responseNvp = array();
			if ( preg_match_all( '/(?<name>[^\=]+)\=(?<value>[^&]+)&?/' , $response , $matches ) ) {
				foreach ( $matches[ 'name' ] as $offset => $name ) {
					$responseNvp[ $name ] = $matches[ 'value' ][ $offset ];
				}
			}
			if ( $responseNvp[ 'PAYMENTINFO_0_PAYMENTSTATUS' ] == 'Completed' ) {
				
                $pagamento = new Pagamento(array(
                    "idpagamento"=>$_SESSION["idpagamento"],
                    "transactionid"=>$_GET[ 'token' ],
                    "idstatus"=>3
                ));

                $pagamento->saveTransactId();
                //$pagamento->saveStatus();

				$pedido = new Pedido((int)$_SESSION["idpedido"]);
				$pessoa = new Pessoa((int)$pedido->getidpessoa());

				send_email(array(
					"to"=>"bruno@fastcode.com.br",
					"subject"=>"Pagamento confirmado do pedido ".$_SESSION["idpedido"]
				), "pgto-confirmado",array(
					"idpedido"=>$pedido->getidpedido(),
					"despessoa"=>$pessoa->getdespessoa(),
					"vlpedido"=>number_format($pedido->getvlpedido(),2,",","."),
					"formapagamento"=>"PayPal"
				));

                header("Location: http://www.indebox.com.br/confirmacao-de-pagamento.php?token=".$_GET[ 'token' ]);
			
            } else {
				
                header("Location: pagamento-nao-confirmado.php");
                send_email(array(
                        "to"=>"bruno@fastcode.com.br",
                        "subject"=>"Pagamento com bug"
                ), "pagamento-nao-confirmado");

			}
		} else {
			header("Location: pagamento-nao-confirmado.php");
            send_email(array(
                    "to"=>"bruno@fastcode.com.br",
                    "subject"=>"Pagamento com bug"
            ), "pagamento-nao-confirmado");

		}
	} else {
        header("Location: pagamento-nao-confirmado.php");
		send_email(array(
                "to"=>"bruno@fastcode.com.br",
                "subject"=>"Pagamento com bug"
        ), "pagamento-nao-confirmado");
	}
	curl_close( $curl );
}
?>
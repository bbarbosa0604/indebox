<?php
require_once("../../inc/configuration.php");
require_once "../../inc/class/PagSeguroLibrary/PagSeguroLibrary.php";

//$_POST['notificationCode'] = 'A50823-4792F292F260-2444F37FACAE-FCEBA8';
//$_POST['notificationType'] = 'transaction';

if(isset($_POST['notificationType']) && $_POST['notificationType'] == 'transaction'){
    //Todo resto do c�digo iremos inserir aqui.

    $email = 'design@indebox.com.br';
    $token = '65F83A03516D4618B5467203F94CEC61';

    $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $_POST['notificationCode'] . '?email=' . $email . '&token=' . $token;

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $transaction = curl_exec($curl);
    curl_close($curl);

    

    if($transaction == 'Unauthorized'){
        
        send_email(array(
                "to"=>"bruno@fastcode.com.br",
                "subject"=>"Pagamento com bug"
        ), "pagamento-nao-confirmado");

        exit;//Mantenha essa linha
    }

    $transaction = simplexml_load_string($transaction);

    $pagamento = new Pagamento(array(
        "transactionid"=>$transaction->code,
        "idstatus"=>3
    ));

    $pagamento->saveTransactId();
    //$pagamento->saveStatus();

    $pedido = new Pedido((int)str_replace("Design Online - Pedido: ", "", $transaction->items->item->description));
    $pessoa = new Pessoa((int)$pedido->getidpessoa());

   send_email(array(
       "to"=>"bruno@fastcode.com.br",
       "subject"=>"Pagamento confirmado do pedido ".$_SESSION["idpedido"]       
   ), "pgto-confirmado",array(
       "idpedido"=>$pedido->getidpedido(),
       "vlpedido"=>number_format($pedido->getvlpedido(),2,",","."),
       "despessoa"=>$pessoa->getdespessoa(),
       "formapagamento"=>"Pag Seguro"
   ));

}

?>
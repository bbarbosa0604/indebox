Aguarde...
<?php
require_once("../inc/configuration.php");
require_once("../inc/class/PagSeguroLibrary/PagSeguroLibrary.php");

$paymentrequest = new PagSeguroPaymentRequest();

$pedido = new Pedido((int)$_SESSION["idpedido"]);

$itens = array(
    Array(
        'id' => $_SESSION["idpagamento"], // identificador
        'description' => 'Design Online - Pedido: '.$_SESSION["idpedido"], // descrição
        'quantity' => 1, // quantidade
        'amount' => $pedido->getvlpedido(), // valor unitário
        'weight' => 0 // peso em gramas
    )
);
foreach($itens as $data){
    $item = new PagSeguroItem($data);
    $paymentrequest->addItem($item);
}

$paymentrequest->setCurrency('BRL');

$paymentrequest->setShipping(3);

//Url de redirecionamento
$paymentrequest->setRedirectURL("http://www.indebox.com.br/confirmacao-de-pagamento.php");// Url de retorno

$credentials = PagSeguroConfig::getAccountCredentials();//credenciais do vendedor

//$compra_id = App_Lib_Compras::insert($produto);
//$paymentrequest->setReference($compra_id);//Referencia;

$url = $paymentrequest->register($credentials);

header("Location: $url");
?>
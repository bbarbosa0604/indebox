Aguarde...
<?php
require_once("../inc/configuration.php");

$pedido = new Pedido((int)$_SESSION["idpedido"]);

$Paypalrequest = new Paypalrequest();

//Vai usar o Sandbox, ou produção?
$sandbox = false;

//Baseado no ambiente, sandbox ou produção, definimos as credenciais
//e URLs da API.
if ($sandbox) {
    //credenciais da API para o Sandbox
    $user = 'conta-business_api1.test.com';
    $pswd = '1365001380';
    $signature = 'AiPC9BjkCyDFQXbSkoZcgqH3hpacA-p.YLGfQjc0EobtODs.fMJNajCx';

    //URL da PayPal para redirecionamento, não deve ser modificada
    $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
} else {
    //credenciais da API para produção
    $user = 'design_api1.indebox.com.br';
    $pswd = 'RR5N89APLN6Y3ZE7';
    $signature = 'A2u9W-Ta6WmZbLqUhSN.UHMdA6v4AWUsS4Y3PhXWCt8nmMflD9olGNZk';

    //URL da PayPal para redirecionamento, não deve ser modificada
    $paypalURL = 'https://www.paypal.com/cgi-bin/webscr';
}

//Campos da requisição da operação SetExpressCheckout, como ilustrado acima.
$requestNvp = array(
    'USER' => $user,
    'PWD' => $pswd,
    'SIGNATURE' => $signature,

    'VERSION' => '108.0',
    'METHOD'=> 'SetExpressCheckout',

    'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE',
    'PAYMENTREQUEST_0_AMT' => $pedido->getvlpedido(),
    'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL',
    'PAYMENTREQUEST_0_ITEMAMT' => $pedido->getvlpedido(),
    'PAYMENTREQUEST_0_INVNUM' => $_SESSION["idpagamento"],
    'PAYMENTREQUEST_0_DESC' => 'Design Online - Pedido: '.$_SESSION["idpedido"],

    'L_PAYMENTREQUEST_0_NAME0' => 'Design Online - Pedido: '.$_SESSION["idpedido"],
    'L_PAYMENTREQUEST_n_DESC0' => 'Design Online - Pedido: '.$_SESSION["idpedido"],
    'L_PAYMENTREQUEST_0_AMT0' => $pedido->getvlpedido(),
    'L_PAYMENTREQUEST_0_QTY0' => '1',
    'L_PAYMENTREQUEST_0_ITEMAMT' => $pedido->getvlpedido(),
    'RETURNURL' => 'http://www.indebox.com.br/pagamento/retorno/paypal.php',
    'CANCELURL' => 'http://www.indebox.com.br/design-online-step2.php',
    'BUTTONSOURCE' => 'BR_EC_EMPRESA'
);

//Envia a requisição e obtém a resposta da PayPal
$responseNvp =  $Paypalrequest->sendNvpRequest($requestNvp, $sandbox);

//Se a operação tiver sido bem sucedida, redirecionamos o cliente para o
//ambiente de pagamento.
if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {
    $query = array(
        'cmd' => '_express-checkout',
        'token' => $responseNvp['TOKEN']
    );

    $redirectURL = sprintf('%s?%s', $paypalURL, http_build_query($query));

    header('Location: ' . $redirectURL);
} else {
    //Opz, alguma coisa deu errada.
    //Verifique os logs de erro para depuração.
    var_dump($responseNvp);
}
?>
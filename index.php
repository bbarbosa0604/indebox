
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

	require_once("inc/configuration.php");


	$page = new Page(array(
		"css" => true,
        "data" => array(
            "head_title" => 'Indebox - Design de interiores'
        )
	));

	$Sql = new Sql();

	$banners = $Sql->arrays("CALL sp_banners_list(1);");

	$page->setTpl("index", array(
		"banners" => $banners
	));
?>
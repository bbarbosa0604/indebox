<?php
	
require_once("inc/configuration.php");

$page = new Page(array(
	"css" => true,
    "data" => array(
        "head_title" => 'Indebox - Design online'
    )
));

$sql = new Sql();
$planos = $sql->arrays("select * from tb_planos where instatus = 1 and indeletado = 0 order by nrposicao");
$depoimentos = $sql->arrays("select * from tb_depoimentos where instatus = 1 order by rand() limit 3 ");

$page->setTpl("design-online",array(
    "planos"=>$planos,
    "depoimentos"=>$depoimentos
));

?>
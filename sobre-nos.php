<?php

require_once("inc/configuration.php");

	$page = new Page(array(
		"css" => true,
        "data" => array(
            "head_title" => 'Indebox - Sobre nós'
        )
	));

	$Sql = new Sql();

	$banner = $Sql->arrays("CALL sp_banners_list(2);", false);
	
	$page->setTpl("sobre-nos", array(
		"banner" => $banner
	));

	?>
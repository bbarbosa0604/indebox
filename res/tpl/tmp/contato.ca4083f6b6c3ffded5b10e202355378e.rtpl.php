<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Fale Conosco</h1>
			<div class="form-contato col-md-12 col-sm-12 col-xs-12">
                
                <form id="form-contato" method="POST">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input id="input-nome" type="text" class="form-control" placeholder="Nome" name="desnome" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input id="input-email" type="email" class="form-control" placeholder="E-mail" name="desemail" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input id="input-telefone" type="text" class="form-control" placeholder="Telefone" name="destelefone" tabindex="3">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input id="input-assunto" type="text" class="form-control" placeholder="Assunto" name="desassunto" tabindex="4">
                            </div>
                        </div>
                        <button id="form-submit-contato" type="button" class="btn button-indebox-pink" tabindex="6">ENVIAR</button>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea id="textarea-mensagem" name="desmensagem" class="form-control" rows="3" placeholder="Mensagem" tabindex="5"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<ul class="iconsContato col-md-12 col-sm-12 col-xs-12">	
					<li class="col-md-4 col-sm-4 col-xs-12">
						<img src="res/img/icon_telefone.png" alt="" /><br />(11) 44320142
					</li>
					<li class="col-md-4 col-sm-4 col-xs-12">
						<img src="res/img/icon_email.png" alt="" /><br />contato@indebox.com.br
					</li>
					<li class="col-md-4 col-sm-4 col-xs-12">
						<img src="res/img/icon_relogio.png" alt="" /><br />De segunda a sexta-feira das 9:00 às 18:00
					</li>
				</ul>

			</div>
			<div class="indique-indebox col-md-12 col-sm-12 col-xs-12">
				<h1>Indique um amigo</h1>
				<div class="row">
					<div class="col-md-12">
						<div id="boxIndique"><img src="res/img/banner-indique-um-amigo.jpg" alt="Indique um Amigo" class="img"></div>
					</div>
					<div class="col-md-6 text-right indique" style="position:relative; bottom: 50px; cursor: pointer" data-toggle="modal" data-target=".modal-indique-amigo">
						<img src="res/img/b1.jpg" alt="Indique um Amigo">
					</div>
				</div>
				<!-- <div class="boxIndique" >
				</div> -->
			</div>
			<div class="redes-sociais col-md-12 col-sm-12 col-xs-12">
				<h1>Nossas redes sociais</h1>

				<div class="boxInstagram boxRedesContato col-md-4 col-xs-12">
					<a href="https://www.instagram.com/indebox/" target="_blank"><img src="res/img/instagram_white.png" alt="" /></a>
				</div>
				<div class="boxPinterest boxRedesContato col-md-4 col-xs-12" data-toggle="modal" data-target=".modal-pinterest">
					<img src="res/img/pinterest_white.png" alt="" />
				</div>
				<div class="boxFacebook boxRedesContato col-md-4 col-xs-12" data-toggle="modal" data-target=".modal-facebook">
					<img src="res/img/facebook_white.png" alt="" />
				</div>

				
			
			</div>
		</div>
	</div>
</div>

<!-- modal facebook -->
<div class="modal fade modal-facebook" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="width:340px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Nossa página do Facebook</h4>
			</div>
			<div class="modal-body">

				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				
				<div class="fb-like-box" data-href="https://www.facebook.com/Indebox/" data-height="488" data-show-faces="true" data-stream="true" data-show-border="false" data-header="false"></div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  -->

<!-- modal pinterest -->
<div class="modal fade modal-pinterest" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="width:340px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Nossa página do Pinterest</h4>
			</div>
			<div class="modal-body">

				<a data-pin-do="embedUser" href="http://br.pinterest.com/indebox/" data-pin-scale-width="290" data-pin-scale-height="320" data-pin-board-width="400">Visite o perfil da Indebox no Pinterest.</a>

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  -->

<style type="text/css">
	.indique img{
		-webkit-transform: scale(1);
		transform: scale(1);
		-webkit-transition: .3s ease-in-out;
		transition: .3s ease-in-out;
	}

	/* Zoom In #1 */
	.indique img {
		-webkit-transform: scale(1);
		transform: scale(1);
		-webkit-transition: .3s ease-in-out;
		transition: .3s ease-in-out;
	}
	.indique:hover img {
		-webkit-transform: scale(1.3);
		transform: scale(1.3);
	}
</style>
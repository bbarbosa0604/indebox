<?php if(!class_exists('raintpl')){exit;}?> <div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-5 col-sm-5 col-xs-12">
			<h1>Nossa Visão</h1>
			
			<p>
				<span>
					Proporcionar acesso à arquitetura e design em suas mais variadas formas a todas as pessoas, acabando com o estigma de que esses serviços são inacessíveis e privilégios de uma pequena parcela da população.
				</span>
			</p>
			<!-- - - - - - - - - - - -->
			<h1>Nossa Missão</h1>
			
			<p>
				<span>
					Buscar novas maneiras de desenhar espaços e executá-los, para assim oferecer sempre soluções customizadas e contribuir para melhoria da qualidade de vida.
				</span>
			</p>
			<!-- - - - - - - - - - - -->
			<h1>Nossos Valores</h1>
			
			<ul class="lista">
<li> Busca pelo novo</li>
				<li> Honestidade e transparência</li>
				<li> Assistência constante</li>
				<li> Tratar cada cliente como único</li>
			</ul>
<!-- - - - - - - - - - - --><h1>Equipe</h1>
			
			<p>
				<span>
					Nossa equipe é formada por membros especialistas em diversas áreas do design e da arquitetura. Acreditamos que um escritório multidisciplinar cumpre as demandas de forma mais completa e criativa.
				</span>
			</p>
		</div>
		<div class="container-center col-md-2 col-sm-2 col-xs-2">
			<div class="border-center"></div>
		</div>
		<div class="container-right col-md-5 col-sm-5 col-xs-12">
			<?php $counter1=-1; if( isset($banner) && is_array($banner) && sizeof($banner) ) foreach( $banner as $key1 => $value1 ){ $counter1++; ?>
				<?php if( $key1 == 0 ){ ?>
					<img src="<?php echo $value1["desurl"];?>" alt="">
				<?php } ?>
			<?php } ?>
			<a href="nossos-servicos.php">
				<button class="btn button-indebox-pink">
					<i class="fa fa-wrench"></i>
					<span>Conheça nossos serviços</span>					
				</button>
			</a>
		</div>
	</div>	
</div>

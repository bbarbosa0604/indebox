<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
    <div id="carousel-banner-indebox" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php $counter1=-1; if( isset($banners) && is_array($banners) && sizeof($banners) ) foreach( $banners as $key1 => $value1 ){ $counter1++; ?>
                <?php if( $key1==0 ){ ?>
                    <li data-target="#carousel-banner-indebox" data-slide-to="<?php echo $key1;?>" class="active"></li>
                <?php }else{ ?>
                    <li data-target="#carousel-banner-indebox" data-slide-to="<?php echo $key1;?>"></li>
                <?php } ?>                
            <?php } ?>
        
        </ol>
    <div class="carousel-inner">
            <?php $counter1=-1; if( isset($banners) && is_array($banners) && sizeof($banners) ) foreach( $banners as $key1 => $value1 ){ $counter1++; ?>
                <?php if( $key1==0 ){ ?>
                    <div class="item active">
                        <img src="<?php echo $value1["desurl"];?>" alt="">
                    </div>
                    <?php }else{ ?>
                    <div class="item">
                        <img src="<?php echo $value1["desurl"];?>" alt="">
                    </div>
                <?php } ?>                
            <?php } ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-banner-indebox" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-banner-indebox" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div> <!-- Carousel -->
    <div class="container-body col-md-12 col-sm-12 col-xs-12">
        <div class="container-left col-md-5 col-sm-5 col-xs-12">
            <h1>A Indebox</h1>

            <p>A
      <span style=" color: rgb(237, 60, 151);">
        <strong>Indebox </strong>
      </span>
      <span>
        traz um serviço inovador na área de arquitetura e design. Trabalhamos com uma visão holística, o que só é possível graças à reunião de profissionais de diferentes especialidades.</span></p>

            <p>
      <span>
        Essa multidisciplinaridade nos permite atender qualquer demanda de forma rápida e eficiente, visando sempre surpreender as expectativas dos clientes.
      </span>
            </p>
        </div>
        <div class="container-center col-md-2 col-sm-2 col-xs-2">
            <div class="border-center"></div>
        </div>
        <div class="container-right col-md-5 col-sm-5 col-xs-12">
            <ul class="lista">
<li> Conceito inovador em arquitetura e design de interiores</li>
                <li> Profissionais especializados</li>
                <li> Atendimento Online</li>
                <li> Maior conveniência</li>
                <li> Gerenciamento de obra: seu projeto seguido à risca</li>
                <li> Excelente custo-benefício</li>
            </ul>
</div>
    </div>
    <div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
                <?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
                <li>
                    <a href="<?php echo $value1["desurl"];?>.php">
                        <i class="fa <?php echo $value1["desicon"];?>"></i>
                        <span><?php echo $value1["desservico"];?></span>
                    </a>
                </li>
                <?php } ?>
            </ul>
</div>
    </div>
</div>
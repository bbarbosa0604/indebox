<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Maquete Eletrônica</h1>
			
			<p>
				<span> 
					A Maquete Eletrônica ou 3D, é a modelagem tridimensional feita por computação gráfica. Nossos projetos preveem a apresentação gráfica dos ambientes a serem trabalhados, o que pode variar de uma simples sala de jantar a uma edificação inteira.
				</span>
			</p>
			
			<p>
				<span>
					Nosso portfólio de maquete eletrônica atende os que necessitam tanto de um estudo básico quanto aqueles que desejam uma apresentação sofisticada.
				</span>
			</p>

			<p>
				<span>
					Quais as vantagens de ter uma maquete eletrônica do seu projeto?
				</span>
			</p>
			
			<p>
				</p>
<ul class="lista">
<li> Auxilia no desenvolvimento da obra pois permite aos arquitetos e designers uma percepção completa do espaço.</li>
					<li> Permite um melhor entendimento do projeto durante sua concepção por parte do cliente, tornando a visualização das ideias mais prática e lúdica do que quando apresentadas em planta baixa.</li>
					<li> O 3D proporciona uma melhor apresentação e seu projeto, servindo também como excelente ferramenta para seu material de vendas.</li>
					<li> Possibilita a visualização prévia do resultado final da obra, evitando possíveis discordâncias na entrega da obra. </li>
				</ul>
<a href="contato.php">
				<button class="btn button-indebox-pink">
					<i class="fa fa-phone"></i>
					<span>Entre em contato</span>
				</button>
			</a>
		</div>
	</div>	
	<div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
                <?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <a href="<?php echo $value1["desurl"];?>.php">
                            <i class="fa <?php echo $value1["desicon"];?>"></i>
                            <span><?php echo $value1["desservico"];?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
</div>
	</div>
</div>
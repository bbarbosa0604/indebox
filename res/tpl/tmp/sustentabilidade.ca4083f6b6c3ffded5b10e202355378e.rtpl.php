<?php if(!class_exists('raintpl')){exit;}?> <div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Sustentabilidade</h1>
			
			<p>
				<span>
					Este serviço é direcionado a clientes que desejam um diferencial em seu projeto, levando em conta o cuidado com o meio ambiente e acreditando na responsabilidade social. Ele pode ser incorporado tanto nos projetos de construção quanto reformas. 
				</span>
			</p>
			
			<p>
				<span>
					Esse trabalho adota como base os conceitos de sustentabilidade das principais certificações existentes no mercado (LEED, AQUA, Procel Edifica e BREEN) e é elaborado por profissionais qualificados e com certificados nacionais e internacionais (Procel Edifica - Residencial e LEED-AP).
				</span>
			</p>

			<p>
				<span>
					O serviço de consultoria em sustentabilidade contém especificações de projeto que levam em conta o tripé da sustentabilidade: economia, meio ambiente e responsabilidade social.
				</span>
			</p>
			
			<a href="contato.php">
				<button class="btn button-indebox-pink">
					<i class="fa fa-phone"></i>
					<span>Entre em contato</span>
				</button>
			</a>
		</div>
	</div>	
	<div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
                <?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <a href="<?php echo $value1["desurl"];?>.php">
                            <i class="fa <?php echo $value1["desicon"];?>"></i>
                            <span><?php echo $value1["desservico"];?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
</div>
	</div>
</div>

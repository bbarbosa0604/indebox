<?php if(!class_exists('raintpl')){exit;}?><html>
<head>
    <title><?php echo $head_title;?></title>
    <!-- metas-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#53C5D0">
    <meta name="description" content="Site Indebox">
    <meta name="author" content="FastCode">

    <!-- favicon-->
    <link rel="icon" type="image/x-icon" href="res/img/favicon.ico" />
    <link rel="icon" type="image/png" href="res/img/favicon.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="res/img/favicon.ico" />

    <!-- requests-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="res/vendors/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="res/vendors/animate.css/animate.min.css" rel="stylesheet">
    <link href="res/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="res/vendors/plugins/slicknav/slicknav.css" rel="stylesheet" />
    <link href="res/vendors/alertify/css/alertify.min.css" rel="stylesheet" />
    <link href="res/vendors/alertify/css/themes/default.min.css" rel="stylesheet" />
    <link href="res/vendors/alertify/css/themes/semantic.min.css" rel="stylesheet" />

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/themes/bootstrap.min.css"/>



    <link href="res/css/default.css" rel="stylesheet">
    <link href="res/css/header.css" rel="stylesheet">
    <link href="res/css/footer.css" rel="stylesheet">
    <!-- adiciona o Css da página se Existir-->
    <?php echo $css;?>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
</head>
<body>
	<div class="header animated">
    	<div class="header-container">
            <div class="logo">
                <a href="index.php">
                	<img class="img-logo" src="res/img/indebox-logo.png" title="Indebox" alt="Indebox" />
    				<img class="img-logo-small" src="res/img/indebox-logo-small.png" title="Indebox" alt="Indebox" />
                </a>
            </div>
            <div class="main-menu">
                <ul>
                    <li><div><a href="index.php" id="menu-home">Home</a></div></li>
                    <li><div><a href="sobre-nos.php" id="menu-sobre-nos">Sobre Nós</a></div></li>
                    <li><div><a href="nossos-servicos.php" id="menu-nossos-servicos">Nossos Serviços</a></div></li>
                    <li><div><a href="design-online.php" id="menu-design-online-info">Design Online</a></div></li>
                    <li><div><a href="galeria.php" id="menu-galeria">Galeria</a></div></li>
                    <li><div><a href="contato.php" id="menu-contato">Contato</a></div></li>
                </ul>
            </div>
    	</div>
    </div>
	<div id="slicknav"></div>
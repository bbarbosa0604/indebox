<?php if(!class_exists('raintpl')){exit;}?><div class="container-body col-md-12 col-sm-12 col-xs-12">
    <ol class="breadcrumb">
      <li><a href="design-online.php">Design Online</a></li>
      <li class="active"><a href="#">Passo 2 - Informações Complementares</a></li>
    </ol>
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title container-left">
                <h2>Design Online - Descrição do Pedido para o plano: <?php echo $desplano;?></h2>
            </div>
        </div>
    </div>

    <section id="page">
        <style>
            

        </style>
        <main id="orcamento">
            <form id="form-orcamento" method="post">
                <div class="container">
                    <div class="col-md-12 page-theme">
                        <div class="alert alert-info">
                            Poderemos entrar em contato para solicitar mais informações
                        </div>
                        <div class="alert alert-success alert-dismissible" role="alert">
                            O orçamento no valor de <Strong>R$ <?php echo $vlpedido;?></Strong> foi enviado ao seu email com sucesso! <strong>Para prosseguir com o pagamento preencha todos os dados abaixo.</strong>
                        </div>
                        <div class="row">
                            <h1><img src="res/img/indebox-logo.png" alt="Logo Impacta"><h2 class="pull-right"><small>Orçamento N°</small><?php echo $idpedido;?>/<?php echo $year;?></h2></h1>
                        </div>
                        <div class="row hidden-xs">
                            
                        </div>

                        <div class="row">
                            <div class="col-md-6 mtb50">

                                <h6>Orçamento para</h6>
                                <h2><?php echo $despessoa;?> <input type="hidden" id="idpessoa" value="<?php echo $idpessoa;?>" /></h2>

                            </div>
                            <div class="col-md-12">
                                <p>Confira atentamente todas as informações de seu orçamento e preencha o restante das informações para prosseguir o pedido.</p>
                            </div>
                        </div>

                        <div class="row box-pagante">

                            <div class="col-md-12">

                                <h3>Informações Adicionais</h3>

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Email do Pagante</th>
                                            <th>Telefone do Pagente</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><input required="required" type="text" name="desemail" id="desemail" tabindex="1" value="<?php echo $desemail;?>" ></td>
                                            <td><input required="required" type="text" name="destelefone" id="destelefone" tabindex="2" value="<?php echo $destelefone;?>" ></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>RG do Pagante</th>
                                            <th>CPF do Pagante</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><input required="required" type="text" name="desrg" id="desrg" tabindex="3" ></td>
                                            <td><input required="required" type="text" name="descpf" id="descpf" tabindex="4" ></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>

                        </div>

                        <div class="row box-endereco" id="form-endereco">

                            <div class="col-md-12" >

                                <h3>Endereço</h3>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Informe o CEP" name="descep" id="descep" tabindex="5" required="required" style="margin-bottom:10px; padding-left:0px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Endereço" name="desendereco" id="desendereco" tabindex="6" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" placeholder="Número" name="nrnumero" id="nrnumero" tabindex="7" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Complemento" name="descomplemento" id="descomplemento" tabindex="8" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Bairro" name="desbairro" id="desbairro"  tabindex="9" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Cidade" name="descidade" id="descidade" tabindex="10" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Estado" name="desestado" id="desestado" tabindex="11" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input type="checkbox" value="" id="inenderecoentrega" checked> Mesmo endereço de entrega
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="endereco-entrega">

                                <h3>Endereço de Entrega</h3>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Informe o CEP" name="descep-entrega" id="descep-entrega" tabindex="20" required="required" style="margin-bottom:10px; padding-left:0px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Endereço" name="desendereco-entrega" id="desendereco-entrega" tabindex="21" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" placeholder="Número" name="nrnumero-entrega" id="nrnumero-entrega" tabindex="22" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Complemento" name="descomplemento-entrega" id="descomplemento-entrega" tabindex="23" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Bairro" name="desbairro-entrega" id="desbairro-entrega" tabindex="24" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Cidade" name="descidade-entrega" id="descidade-entrega" tabindex="25" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Estado" name="desestado-entrega" id="desestado-entrega" tabindex="26" required="required" style="margin-bottom:10px;">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row box-ambientes">

                            <div class="col-md-12">

                                <h3>Ambientes</h3>
                                <p>Preecha os dados complementares do ambiente para prosseguir o pagamento.</p>

                                <table class="table">
                                    <tbody>
                                        <?php $counter1=-1; if( isset($ambientes) && is_array($ambientes) && sizeof($ambientes) ) foreach( $ambientes as $key1 => $value1 ){ $counter1++; ?>
                                            <tr id="ambiente-<?php echo $value1["idpedidoambiente"];?>" >
                                                <td class="col-sm-10"><?php echo $value1["desambiente"];?>, metragem: <?php echo $value1["desmetragem"];?></td>
                                                <td class="col-sm-2"><a href="#">Detalhes</a></td>
                                            </tr>
                                            <tr id="detalhes-ambiente-<?php echo $value1["idpedidoambiente"];?>" name="<?php echo $value1["desambiente"];?>" class="detalhes" style="display:none;">
                                                <td colspan="2">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" placeholder="Fotos do ambiente, Planta do Ambiente, etc... (Apenas arquivos com extensão .PNG, .JPG, .GIF)">
                                                            <img src="res/img/bg_file.png" class="inputfile" />
                                                            <input class="fileupload" id="fileupload-<?php echo $value1["idpedidoambiente"];?>" type="file" name="files[]" multiple tabindex="20" />
                                                        </div>
                                                        <div class="col-sm-12 ambiente-fotos">                                                                
                                                            <?php $counter2=-1; if( isset($value1["fotos"]) && is_array($value1["fotos"]) && sizeof($value1["fotos"]) ) foreach( $value1["fotos"] as $key2 => $value2 ){ $counter2++; ?>
                                                                <div id="img-<?php echo $value2["idarquivo"];?>" class="col-sm-3 box-img" idarquivo="<?php echo $value2["idarquivo"];?>" pedido='<?php echo $value2["idpedido"];?>' file="<?php echo $value2["desarquivo"];?>">
                                                                    <img src="res/img/pedido/<?php echo $value2["idpedido"];?>/<?php echo $value2["desarquivo"];?>" class="img" />
                                                                    <img class="close" src="res/img/close.png" />
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <textarea placeholder="Observações gerais" rows="5"></textarea>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>

                        </div>

                        <div class="row box-forma-de-pagamento">

                            <div class="col-md-12">

                                <h3>Escolha um meio de pagamento</h3>

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="formaPagamento" value="1">
                                        <img src="res/img/logo-pagseguro.png" alt="PagSeguro" />
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="formaPagamento" value="2">
                                        <img src="res/img/logo-paypal.png" alt="PayPal" />
                                    </label>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="container-sm-height col-md-12 box-total">
                                <div class="row row-sm-height b-a b-grey">
                                    <div class="col-sm-7 col-sm-height col-middle clearfix sm-p-b-15 p-b-25 p-t-25"></div>
                                    <div class="col-sm-5 text-right bg-menu col-sm-height padding-15 total">
                                        <h5 class=" all-caps small no-margin hint-text text-white bold">Total</h5>
                                        <h1 class="no-margin text-white real">R$ <?php echo $vlpedido;?></h1>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="alert alert-info alert-dismissible" role="alert">                                
                                <input type="checkbox" id="inaceite" checked /> Ao clicar em aceitar você confirma que leu e aceita o <a href="#" data-toggle="modal" data-target="#modalContrato">Contrato de Prestação de Serviço</a>. Por segurança seu endereço de IP <?php echo $ip;?> será registrado para confirmar o aceite. <Strong>Caso não possua alguma dessas informações no momento, elas poderão ser enviadas por e-mail.</Strong>
                            </div>
                        </div>

                        <div class="row text-center">
                            <button id="addPagamento" type="button" class="btn button-indebox-pink-add btn-small">Efetuar Pagamento</button>
                        </div>
                    </div>    
                </div>
            </form>
        </main>
        <div class="modal" id="modalContrato" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Contrato de Prestação de Serviço</h4>
                    </div>
                    <div class="modal-body" style="overflow: auto; margin: 25px 0px; height: 598px;">
                        <?php echo $contrato;?>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="margin: 0 auto; float: none; display: block;">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
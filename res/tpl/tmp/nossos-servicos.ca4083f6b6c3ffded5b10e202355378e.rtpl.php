<?php if(!class_exists('raintpl')){exit;}?> <div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1><i class="fa fa-wrench"></i> Nossos Serviços</h1>
			<div class="row">
				<a href="arquitetura-design.php">					
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-paint-brush"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Arquitetura e Design</h1>
						<p>Para o desenvolvimento de qualquer projeto são utilizados conhecimentos ligados às áreas de arquitetura e design, já que seus objetivos são complementares. Sendo assim, são avaliadas desde a estrutura até o mobiliário mais adequado para o imóvel ou ambiente. Nosso objetivo é oferecer soluções que otimizem o espaço, sejam sustentáveis, promovam conforto e contribuam para o bem-estar.</p>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="consultoria-tecnica.php">
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-pencil-square-o"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Consultoria Técnica</h1>
						<p>A consultoria técnica pode ser contratada independentemente da contratação de projeto de arquitetura ou design de interiores. Neste caso, ela visa esclarecer dúvidas do cliente durante a execução de obra, podendo incluir orientações arquitetônicas, de design de interiores e de decoração. A consultoria é realizada com a presença do profissional especializado.</p>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="sustentabilidade.php">
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-leaf"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Sustentabilidade</h1>
						<p>Este serviço é direcionado a clientes que desejam um diferencial em seu projeto, levando em conta o cuidado com o meio ambiente e acreditando na responsabilidade social. Ele pode ser incorporado tanto nos projetos de construção quanto reformas.</p>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="gerenciamento-de-obra.php">
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-gears"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Gerenciamento de Obra</h1>
						<p>O gerenciamento de obra, ou conhecido por alguns como administração de obra, existe para alinhar todos os componentes de uma obra para que ao final a estimativa de custo e cronograma tenham sido respeitados.</p>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="maquete-eletronica.php">
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-laptop"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Maquete Eletrónica</h1>
						<p>A Maquete Eletrônica ou 3D, é a modelagem tridimensional feita por computação gráfica. Nossos projetos preveem a apresentação gráfica dos ambientes a serem trabalhados, o que pode variar de uma simples sala de jantar a uma edificação inteira.</p>
					</div>
				</a>
			</div>
			 <div class="row">
				<a href="design-online.php">
					<div class="icon-servicos col-md-2 col-sm-2 col-xs-2">
						<i class="fa fa-magic"></i>
					</div>
					<div class="descricao-servicos col-md-10 col-sm-10 col-xs-10">
						<h1>Design Online</h1>
						<p>
							Design de interiores online &eacute; uma das muitas formas pensadas pela&nbsp;<strong>Indebox</strong> para atingir nosso principal valor: DESIGN PARA TODOS. Defendemos a ideia de que sempre &eacute; poss&iacute;vel viver em uma ambiente bem projetado sem fazer grandes mudan&ccedil;as. O design de interiores online &eacute; um servi&ccedil;o inovador e exclusivo desenvolvido por nossa equipe como uma alternativa pr&aacute;tica e r&aacute;pida para desenhar e decorar seu espa&ccedil;o. Ele pode ser considerado uma consultoria em design de interiores, por&eacute;m possui alguns diferenciais que agregam valor e destacam esse servi&ccedil;o do restante.</p>
						<p>Vantagens do Design Online em rela&ccedil;&atilde;o a servi&ccedil;os semelhantes:</p>
						<ul style="color: #777; margin-left: 20px;">	<li>Solu&ccedil;&atilde;o mais barata que os projetos e consultorias convencionais em design de interiores.</li>	<li>Economia de tempo, sem necessidade de reuni&otilde;es para defini&ccedil;&atilde;o de projeto.</li>	<li>Fim da necessidade da presen&ccedil;a f&iacute;sica do designer para discutir o projeto ou prestar consultoria.</li>	<li>Possibilidade de vivenciar o processo e ter controle sobre a transforma&ccedil;&atilde;o de seu ambiente, incluindo flexibilidade de cronograma e or&ccedil;amento, com tranquilidade garantida por um estudo realizado por profissionais, diminuindo a possibilidade de erros essenciais.</li>	<li>Atendimento online para tirar d&uacute;vidas.</li>	<li>Atendimento feito por uma equipe de designers associados &agrave; ABD (Associa&ccedil;&atilde;o Brasileira de Designers de Interiores).</li>	<li>Op&ccedil;&atilde;o de redesenhar sua casa por partes, de acordo com a sua vontade.</li></ul><p>&nbsp;</p></p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Arquitetura e Design</h1>
			
			<p>
				<span>
					Para o desenvolvimento de qualquer projeto são utilizados conhecimentos ligados às áreas de arquitetura e design, já que seus objetivos são complementares. Sendo assim, são avaliadas desde a estrutura até o mobiliário mais adequado para o imóvel ou ambiente. Nosso objetivo é oferecer soluções que otimizem o espaço, sejam sustentáveis, promovam conforto e contribuam para o bem-estar.
				</span>
			</p>
			
			<p>
				<span>
					Para atingirmos estes objetivos oferecemos duas opções para nosso cliente:
				</span>
			</p>
			
			<p>
				<span>
					<b>Projeto completo:</b> trabalhamos desde o levantamento da necessidade do cliente, criação e desenvolvimento da ideia até a execução final do projeto com o acompanhamento feito pelo profissional especializado (arquiteto e/ou designer de interior).
				</span>
			</p>

			<p>
				<span>
					<b>Projeto de criação:</b> composto pelo levantamento da necessidade do cliente, criação e desenvolvimento da ideia, sendo o acompanhamento e execução de responsabilidade do cliente. Neste caso, oferecemos a opção de ser contratada nossa consultoria para auxiliar em decisões pontuais, sendo aplicado o sistema de cobrança por hora técnica.
				</span>
			</p>
			<a href="contato.php">
				<button class="btn button-indebox-pink">
					<i class="fa fa-phone"></i>
					<span>Entre em contato</span>					
				</button>
			</a>
		</div>
<!-- 
		<div class="container-right col-md-5 col-sm-5 col-xs-12">
			<img class="banner-servicos" src="res/img/arquitetura-design.jpg" alt="Arquitetura e Design">
		</div> -->
	</div>	
	<div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
				<?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
	                <li>
	                    <a href="<?php echo $value1["desurl"];?>.php">
	                        <i class="fa <?php echo $value1["desicon"];?>"></i>
	                        <span><?php echo $value1["desservico"];?></span>
	                    </a>
	                </li>
                <?php } ?>
            </ul>
</div>
	</div>
</div>
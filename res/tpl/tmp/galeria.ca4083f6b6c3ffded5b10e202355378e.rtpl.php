<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
    <div class="container-body col-md-12 col-sm-12 col-xs-12">
        <div class="container-left col-md-12 col-sm-12 col-xs-12">
            <?php $counter1=-1; if( isset($categorias) && is_array($categorias) && sizeof($categorias) ) foreach( $categorias as $key1 => $value1 ){ $counter1++; ?>
                <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="<?php echo $value1["idcategoria"];?>">
                    <div class="album" data-galeria="<?php echo $value1["idcategoria"];?>">
                        <img src="<?php echo $value1["desurlcapa"];?>" alt="<?php echo $value1["descategoria"];?>" title="<?php echo $value1["descategoria"];?>" />
                        <div class="description">
                            <span><?php echo $value1["descategoria"];?></span>
                            <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="1">
                <div class="album" data-galeria="13">
                    <img src="res/img/galeria/cozinha.jpg" alt="Cozinha" title="Cozinha" />
                    <div class="description">
                        <span>Cozinha</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div>
            <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="2">
                <div class="album" data-galeria="14">
                    <img src="res/img/galeria/37.jpg" alt="Sala de estar" title="Sala de estar">
                    <div class="description">
                        <span>Sala de Estar</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div>
            <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="3">
                <div class="album" data-galeria="15">
                    <img src="res/img/galeria/41.jpg" alt="Quarto" title="Quarto">
                    <div class="description">
                        <span>Quarto</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div>
            <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="4">
                <div class="album" data-galeria="16">
                    <img src="res/img/galeria/16.jpg" alt="Varanda" title="Varanda">
                    <div class="description">
                        <span>Varanda</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div>
            <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="5">
                <div class="album" data-galeria="17">
                    <img src="res/img/galeria/7.jpg" alt="">
                    <div class="description">
                        <span>Fachada</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div>
            <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="6">
                <div class="album" data-galeria="18">
                    <img src="res/img/galeria/15.jpg" alt="">
                    <div class="description">
                        <span>Banheiro</span>
                        <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                    </div>
                </div>
            </div> -->

            <div class="fotorama-wrap">
                <button class="btn-close-slideshow"><i class="fa fa-times"></i></button>
                <div id="fotorama" class="fotorama" data-auto="false">
                </div>
            </div>

        </div>
    </div>
</div>

<!-- <?php $counter1=-1; if( isset($categorias) && is_array($categorias) && sizeof($categorias) ) foreach( $categorias as $key1 => $value1 ){ $counter1++; ?>
                <div class="grupo-galeria col-md-4 col-sm-4 col-xs-12" data-grupo-img="<?php echo $value1["idcategoria"];?>">
                    <div class="album" data-galeria="<?php echo $value1["idcategoria"];?>">
                        <img src="<?php echo $value1["desurlcapa"];?>" alt="<?php echo $value1["descategoria"];?>" title="<?php echo $value1["descategoria"];?>" />
                        <div class="description">
                            <span><?php echo $value1["descategoria"];?></span>
                            <span>Clique aqui para visualizar imagens de projetos realizados pela Indebox</span>
                        </div>
                    </div>
                </div>
            <?php } ?> -->
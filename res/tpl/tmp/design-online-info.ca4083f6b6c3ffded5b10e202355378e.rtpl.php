<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Design Online</h1>
			<p></p><p>Design de interiores online é uma das muitas formas pensadas pela&nbsp;<strong>Indebox</strong> para atingir nosso principal valor: DESIGN PARA TODOS. Defendemos a ideia de que sempre é possível viver em uma ambiente bem projetado sem fazer grandes mudanças. O design de interiores online é um serviço inovador e exclusivo desenvolvido por nossa equipe como uma alternativa prática e rápida para desenhar e decorar seu espaço. Ele pode ser considerado uma consultoria em design de interiores, porém possui alguns diferenciais que agregam valor e destacam esse serviço do restante.</p><p>Vantagens do Design Online em relação a serviços semelhantes:</p><ul style="margin-left:20px;">	<li>Solução mais barata que os projetos e consultorias convencionais em design de interiores.</li>	<li>Economia de tempo, sem necessidade de reuniões para definição de projeto.</li>	<li>Fim da necessidade da presença física do designer para discutir o projeto ou prestar consultoria.</li>	<li>Possibilidade de vivenciar o processo e ter controle sobre a transformação de seu ambiente, incluindo flexibilidade de cronograma e orçamento, com tranquilidade garantida por um estudo realizado por profissionais, diminuindo a possibilidade de erros essenciais.</li>	<li>Atendimento online para tirar dúvidas.</li>	<li>Atendimento feito por uma equipe de designers associados à ABD (Associação Brasileira de Designers de Interiores).</li>	<li>Opção de redesenhar sua casa por partes, de acordo com a sua vontade.</li></ul><p>&nbsp;</p><p></p>
			<a href="design-online.php">
				<button class="btn button-indebox-pink">
					<span>Comece Agora</span>
				</button>
			</a>
		</div>

	</div>	
	<div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
                <?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <a href="<?php echo $value1["desurl"];?>.php">
                            <i class="fa <?php echo $value1["desicon"];?>"></i>
                            <span><?php echo $value1["desservico"];?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
</div>
	</div>
</div>
<?php if(!class_exists('raintpl')){exit;}?><div class="container-main-page">
	<div class="container-body col-md-12 col-sm-12 col-xs-12">
		<div class="container-left col-md-12 col-sm-12 col-xs-12">
			<h1>Consultoria Técnica</h1>
			
			<p>
				<span>
					A consultoria técnica pode ser contratada independentemente da contratação de projeto de arquitetura ou design de interiores. 
				</span>
			</p>
			
			<p>
				<span>
					Neste caso, ela visa esclarecer dúvidas do cliente durante a execução de obra, podendo incluir orientações arquitetônicas, de design de interiores e de decoração. A consultoria é realizada com a presença do profissional especializado.
				</span>
			</p>
			
			<a href="contato.php">
				<button class="btn button-indebox-pink">
					<i class="fa fa-phone"></i>
					<span>Entre em contato</span>
				</button>
			</a>
		</div>
	</div>	
	<div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="servicosLista">

            <h1>
<i class="fa fa-wrench"></i> Nossos Serviços</h1>

            <ul class="servicos">
                <?php $counter1=-1; if( isset($servicos) && is_array($servicos) && sizeof($servicos) ) foreach( $servicos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <a href="<?php echo $value1["desurl"];?>.php">
                            <i class="fa <?php echo $value1["desicon"];?>"></i>
                            <span><?php echo $value1["desservico"];?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
</div>
	</div>
</div>
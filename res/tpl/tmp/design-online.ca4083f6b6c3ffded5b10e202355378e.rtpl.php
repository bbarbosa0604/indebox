<?php if(!class_exists('raintpl')){exit;}?><div class="modal fade" id="moreInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Mais Informa&ccedil;&otilde;es</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>  
<div class="container-main-page">
    <div id="carousel-banner-design" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel-banner-indebox" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-banner-indebox" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="res/img/banner-design-online.jpg" alt="" />
            </div>
            <div class="item">
                <img src="res/img/banner-design-online.jpg" alt="" />
            </div>
        </div>

        <a class="left carousel-control" href="#carousel-banner-indebox" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-banner-indebox" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div> <!-- Carousel -->
    <div class="container-body col-md-12 col-sm-12 col-xs-12">
        <div class="container-left col-md-12 col-sm-12 col-xs-12">
            <h1>Nossos Planos</h1>
            <ul class="descricaoPlanos">
                <?php $counter1=-1; if( isset($planos) && is_array($planos) && sizeof($planos) ) foreach( $planos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <div class="border">
                            <div class="planos" data-plano="<?php echo $value1["idplano"];?>" url-plano="<?php echo $value1["desplano"];?>" >
                                <span class="title-plano"><?php echo $value1["desplano"];?></span>
                                <p>
                                    <?php echo $value1["descricao"];?>
                                </p>
                                <h3 class="startPlano">Escolher este plano</h3>
                            </div>
                        </div>

                        <a class="btn" href="#" id="moreInfo" data-toggle="modal" data-target="#moreInfo" data-info='<?php echo $value1["desinformacao"];?>' > Mais Informa&ccedil;&otilde;es</a>

                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-bottom col-md-12 col-sm-12 col-xs-12">
        <div class="depoimentos">
            <h1>O que nossos clientes dizem</h1>

            <ul class="listaDepoimentos">
                <?php $counter1=-1; if( isset($depoimentos) && is_array($depoimentos) && sizeof($depoimentos) ) foreach( $depoimentos as $key1 => $value1 ){ $counter1++; ?>
                    <li>
                        <div class="depoimentoTexto" arrow-id='0'>
                            <div class="aspasOpen"></div>
                            <p><?php echo $value1["desdepoimento"];?></p>
                            <div class="aspasClose"></div>
                        </div>
                        <p class="assinatura"><?php echo $value1["desnome"];?></p>
                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
</div>
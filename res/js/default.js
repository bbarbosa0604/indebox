jQuery(document).ready(function($) {
    function pinterest () {
        (function(d){
            var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
            p.type = 'text/javascript';
            p.async = true;
            p.src = '//assets.pinterest.com/js/pinit_main.js';
            f.parentNode.insertBefore(p, f);
        }(document, 'script'));
    }

    $('div.boxRedesContato.boxPinterest').on('click', function(){
        pinterest();
    });

    $("#boxIndique").on("click",function(){
            $(".modal-indique-amigo").modal("show");
        });

    function inputVerify(input, msg) {
        var inputValue = $.trim($(input).val());

        if ( !inputValue || inputValue == '' ) {
            alertify.alert('Atenção', msg);

            return false;
        }
        else if ( $(input).attr('type') == 'email' ) {
            var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if(!filtro.test(inputValue)) {

                alertify.alert('Atenção', "Este endereço de email não é válido!");

                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }

	$( window ).resize(function() {
	  	$('.border-center').height($('.container-left').height() - 20);

	  	if ( $( this ).width() > 767 && $( this ).scrollTop() > 40 ) { 
	  		$('.header').addClass("menu-fixo");
			$('.header-container .logo .img-logo').addClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').addClass('menu-fixo-margin');
	  	}
	  	else {
			$('.header').removeClass("menu-fixo");
			$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
		}
	});	

	$( window ).scroll(function () { 
		if ( $( window ).width() > 767 ) {
			if ( $( this ).scrollTop() > 40 ) { 
				$('.container-main-page').addClass('margin-view-body');
				$('.header').addClass("fadeInDown menu-fixo"); 
				$('.header-container .logo .img-logo').addClass('menu-fixo-logo-small');
				$('.header .header-container .main-menu').addClass('menu-fixo-margin');
			} 
			else { 
				$('.container-main-page').removeClass('margin-view-body');
				$('.header').removeClass("fadeInDown menu-fixo"); 
				$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
				$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
			}					
		}
		else {
			$('.container-main-page').removeClass('margin-view-body');
			$('.header').removeClass("menu-fixo");
			$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
		}
	});

	$('#accordion .panel-default .panel-heading > a').on('click', function(){
		if( $(this).hasClass('collapsed') ) {
			$('i.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
			$(this).find('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');			
		}
		else {
			$(this).find('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
		}
	});

	$('div.album').on('click', function( ) {
		$('#loading').show();
		
		$.ajax({
			url: 'res/json/getGaleriaFotos.php',
			method: 'POST',
			data: {
				galeria: $(this).data('galeria')
			},
			dataType: 'JSON',
			success: function( response ) {
				var fotos = Array();

				$.each(response.fotos, function(index, foto) {
					fotos.push({img: foto.desurl});
				});
				
				$('#loading').hide();

				$('.fotorama-wrap').show();

                var $fotoramaDiv = $('.fotorama').fotorama({
                    minwidth: '100%',
                    width: '100%',
                    ratio: 3/2,
                    nav: 'thumbs',
                    thumbheight: 48,
                    allowfullscreen: true,
                    keyboard: true,
                    data: fotos
                });

                var fotorama = $fotoramaDiv.data('fotorama');
                
                fotorama.show(0);

                if ( $( window ).width() < 767 ) {
                    fotorama.requestFullScreen();
                }

                $('.fotorama').on('fotorama:fullscreenexit ',
                    function (e, fotorama, extra) {
                        if ( $( window ).width() < 767 ) {
                            $('.fotorama').data('fotorama').destroy();
                            $('.fotorama-wrap').hide();
                        }
                    }
                ).fotorama();
			}
		});
	});

	$(".fotorama-wrap").on("click", function (event) {
	    if ($(event.toElement).hasClass("fotorama-wrap")) {
	        $('.fotorama').data('fotorama').destroy();
	        $('.fotorama-wrap').hide();
	    }
	})

    $('#addambiente').on('click', function(){
        if( inputVerify($('#form-ambiente input[name=desambiente]'), 'Digite o nome do ambiente') == false ){
            return false;
        }

        if( inputVerify($('#form-ambiente input[name=desmetragem]'), 'Digite a metragem') == false ){
            return false;
        }

        if( inputVerify($('#form-ambiente input[name=desatividadeambiente]'), 'Digite a atividade do ambiente') == false ){
            return false;
        }

        var ambiente = '<div class="item-estilo"><div class="btn-editar"><i class="fa fa-pencil"></i></div><div class="btn-fechar">X</div>';
        ambiente += '<span><b>Ambiente:</b>'+$('#form-ambiente input[name=desambiente]').val()+'</span><span class="preco">R$ 1.000,00</span></div>';

        alertify.alert('Sucesso','Ambiente cadastrado com sucesso').set('onok', function(closeEvent){

            $('div.itens').append(ambiente).show();
            $('button#finalizar-compra').show();

            $('.item-estilo .btn-fechar').off().on('click', function(){
                $( this ).parent().remove();
                if($('.itens .item-estilo').length == 0) {
                    $('button#finalizar-compra').hide();
                }
            });
        });

    });

    $('button#finalizar-compra').on('click', function(){
        $.redirect('finalizar-compra.php', {
            idpessoa: 1
        });
    });

	$('.container-left .fotorama-wrap .btn-close-slideshow').on('click', function() {
		$('#fotorama').data('fotorama').destroy();
		$('.fotorama-wrap').hide();
	});

	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
		  field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};


    if( $('#input-telefone').length ) {
        $('#input-telefone').mask(SPMaskBehavior, spOptions);
    }

    if ( $('#form-cadastro').length == 1 || $('#form-contato').length == 1 || $("#form-step1").length == 1) {
        $('form input[name=destelefone]').mask(SPMaskBehavior, spOptions);
        $('form input[name=descelular]').mask(SPMaskBehavior, spOptions);
        $('form input[name=descpf]').mask('000.000.000-00', {reverse: true});
        $('form input[name=descep], #form-endereco input[name=descep]').mask('00000-000');
        $('form input[name=descep-entrega], #form-endereco input[name=descep-entrega]').mask('00000-000');
        $('form input[name=desestado], #form-endereco input[name=desestado]').mask('AA');
    }

	/*Send form contato*/
    $('#form-submit-contato').on('click', function() {
		$(this).attr({'disabled':'disabled'});
		if( inputVerify($('#input-nome'), 'Digite um nome') == false ){
			$(this).removeAttr('disabled');

			return false;
		}
		if( inputVerify($('#input-email'), 'Digite um email') == false ){
			$(this).removeAttr('disabled');

			return false;
		}

		if( inputVerify($('#input-telefone'), 'Digite um telefone') == false ){
			$(this).removeAttr('disabled');

			return false;
		}

		if( inputVerify($('#input-assunto'), 'Digite um assunto') == false ){
			$(this).removeAttr('disabled');

            return false;
        }

        if( inputVerify($('#textarea-mensagem'), 'Digite uma mensagem') == false ){
            $(this).removeAttr('disabled');

            return false;
        }

        $('#loading').show();

		$.ajax({
			url: 'res/action/formcontatosave.php',
			method: 'POST',
			data: {
				nome: $.trim($('#input-nome').val()),
				email: $.trim($('#input-email').val()),
				telefone: $.trim($('#input-telefone').val()),
				assunto: $.trim($('#input-assunto').val()),
				mensagem: $.trim($('#textarea-mensagem').val())
			},
			dataType: 'JSON',
			success: function( response ) {
				$('#loading').hide();
				if( response.success ) {
					alertify.alert('Sucesso', 'Sua mensagem foi enviada com sucesso!');

					$('#input-nome').val('');
					$('#input-email').val('');
					$('#input-telefone').val('');
					$('#input-assunto').val('');
					$('#textarea-mensagem').val('');

					$('#form-submit-contato').removeAttr('disabled');
				} else {
				    alertify.error(response.msg);
				    $('#form-submit-contato').removeAttr('disabled');
				}
			}
		});
	});

	/*Send form indique um amigo*/
	$('#modal-form-submit').on('click', function() {
		$(this).attr({'disabled':'disabled'});
		if( inputVerify($('#inputNome'), 'Digite um nome') == false ){
			$(this).removeAttr('disabled');

			return false;
		}
		if( inputVerify($('#inputEmail'), 'Digite um email') == false ){
			$(this).removeAttr('disabled');

			return false;
		}
		if( inputVerify($('#inputNomeAmigo'), 'Digite o nome do seu amigo') == false ){
			$(this).removeAttr('disabled');

			return false;
		}
		if( inputVerify($('#inputEmailAmigo'), 'Digite o email do seu amigo') == false ){
			$(this).removeAttr('disabled');

			return false;
		}

		if( inputVerify($('#textareaMensagem'), 'Digite uma mensagem') == false ){
			$(this).removeAttr('disabled');

			return false;
		}

		$('#loading').show();

		$.ajax({
			url: 'res/action/formcontatoindicacaosave.php',
			method: 'POST',
			data: {
				nome: $.trim($('#inputNome').val()),
				email: $.trim($('#inputEmail').val()),
				nomeamigo: $.trim($('#inputNomeAmigo').val()),
				emailamigo: $.trim($('#inputEmailAmigo').val()),
				mensagem: $.trim($('#textareaMensagem').val()),
				copia: ($('#checkbox-copia:checked').length)? 1 : 0
			},
			dataType: 'JSON',
			success: function( response ) {
				$('#loading').hide();
				if( response.success ) {
					alertify.alert('Obrigado', 'A Indebox agradece a sua indicação!', function() {
						$('#inputNome').val('');
						$('#inputEmail').val('');
						$('#inputNomeAmigo').val('');
						$('#inputEmailAmigo').val('');
						$('#textareaMensagem').val('');
						$('#checkbox-copia').removeAttr('checked');

						$('#modal-form-submit').removeAttr('disabled');

						//$('.modal-indique-amigo').modal('hide');
					});
				}
			}
		});
	});

	//Mosta o Slicknav
	$('.main-menu ul').slicknav({
		prependTo: '#slicknav',
		label: ''
	}).slicknav('open');

    function limpa_formulario_cep() {
        // Limpa valores do formulário de cep.
        $("#desendereco").val("");
        $("#desbairro").val("");
        $("#descidade").val("");
        $("#desestado").val("");
    }

    //
    function limpa_formulario_cep_entrege() {
        // Limpa valores do formulário de cep.
        $("#descep-entrega").val("");
        $("#desendereco-entrega").val("");
        $("#desbairro-entrega").val("");
        $("#descidade-entrega").val("");
        $("#desestado-entrega").val("");
    }

    //Quando o campo cep perde o foco.
    $("#form-cadastro input[name=descep], #form-endereco input[name=descep]").blur(function() {
        //pega o formulário do do input
        var form = $( this ).closest('form');

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $( form ).find('input[name=desendereco]').val("...");
                $( form ).find('input[name=desbairro]').val("...");
                $( form ).find('input[name=descidade]').val("...");
                $( form ).find('input[name=desestado]').val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $( form ).find('input[name=desendereco]').val(dados.logradouro);
                        $( form ).find('input[name=desbairro]').val(dados.bairro);
                        $( form ).find('input[name=descidade]').val(dados.localidade);
                        $( form ).find('input[name=desestado]').val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulario_cep();
                        alertify.error("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulario_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulario_cep();
        }
    });

    //Quando o campo cep perde o foco.
    $("#endereco-entrega input[name=descep-entrega]").blur(function () {
        //pega o formulário do do input
        var form = $(this).closest('form');

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $(form).find('input[name=desendereco-entrega]').val("...");
                $(form).find('input[name=desbairro-entrega]').val("...");
                $(form).find('input[name=descidade-entrega]').val("...");
                $(form).find('input[name=desestado-entrega]').val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $(form).find('input[name=desendereco-entrega]').val(dados.logradouro);
                        $(form).find('input[name=desbairro-entrega]').val(dados.bairro);
                        $(form).find('input[name=descidade-entrega]').val(dados.localidade);
                        $(form).find('input[name=desestado-entrega]').val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulario_cep_entregue();
                        alertify.error("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulario_cep_entregue();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulario_cep_entregue();
        }
    });

    $('.descricaoPlanos li .planos').on('click', function(){
        $.redirect('design-online-step1.php', {
            idplano: $( this ).attr('data-plano'),
            desplano: $( this ).attr('url-plano')
        });
    });

    var removeAmbiente = function(){

        var idambiente = $(this).attr("id");
        
        for (var i = arrAmbientes.length - 1; i >= 0; i--) {
            if(arrAmbientes[i].id == idambiente){
                //remover do array
                arrAmbientes.splice(i,1);
            };
        };
            
        $(this).remove();
    };
  
    $("#btn-print").on("click", function () {
        var popupWin = window.open();
        popupWin.window.focus();
        popupWin.document.write('<link href="res/css/default.css" rel="stylesheet">');
        popupWin.document.write($("#orcamento").html());
        popupWin.print()
    });

    var arrAmbientes = [];

    $('#addAmbiente').on("click",function(){
        var desambiente = $("#desambiente").val();
        var desmetragem = $("#desmetragem").val();
        //
        if(desambiente==''){
            alertify.error('Digite um conteúdo para ambiente!');
            return false;
        }
        if(desmetragem==''){
            alertify.error('Digite um conteúdo para metragem!');
            return false;
        }

        arrAmbientes.push({
            id:'ambiente-'+(parseInt(arrAmbientes.length)+1),
            desambiente:desambiente,
            desmetragem:desmetragem
        })
        //
        $("#ambientes").append(function(){
            return $('<div id="ambiente-'+arrAmbientes.length+'" class="well well-sm"> <p>Ambiente: '+desambiente+', metragem: '+desmetragem+' <a class="btn button-indebox-pink-remove btn-small" id="removeAmbiente">X</a></p></div> ').click(removeAmbiente);
        });
        //
        $("#desambiente").val('')
        $("#desmetragem").val('')
        //
        $("#modal-adicionarambiente").modal("hide");
    });



    $("#addOrcamento").on("click",function(){
        
        var idplano = $("#idplano").val();
        if(idplano==''){
            alertify.alert('Nenhum plano foi selecionado');
            //
            window.location.href = "design-online.php";
            //
            return false;
        }
        var desnome = $("#desnome").val();
        if(desnome==''){
            alertify.error('Digite um nome!');
            $("#desnome").focus();
            return false;
        }
        var desemail = $("#desemail").val();
        if(desemail==''){
            alertify.error('Digite um válido!');
            $("#desemail").focus();
            return false;
        }
        var destelefone = $("#destelefone").val();

        if(destelefone==''){
            alertify.error('Digite um telefone!');
            $("#destelefone").focus();
            return false;
        }
        var descidade = $("#descidade").val();
        if(descidade==''){
            alertify.error('Digite uma cidade!');
            $("#descidade").focus();
            return false;
        }
        var desestado = $("#desestado").val();
        if(desestado==''){
            alertify.error('Escolha um estado!');
            $("#desestado").focus();
            return false;
        }
        //
        if(arrAmbientes.length==0){
            alertify.error('Obrigatório enviar pelo menos um ambiente!');
            return false;   
        }
        //
        $("#addOrcamento").attr("disabled","disabled");
        $("#addOrcamento").html("Salvando...");
        //
        $.ajax({
            type: "POST",
            url: "res/action/addStep1.php",
            data: {
                idplano:idplano,
                desnome:desnome,
                desemail:desemail,
                destelefone:destelefone,
                descidade:descidade,
                desestado:desestado,
                ambientes:arrAmbientes
            },
            success: function(a){
                var json = $.parseJSON(a);
                if(json.success){
                    alertify.alert("Foi enviado um e-mail de seu orçamento, verifique sua caixa de e-mail ou seu lixo eletrônico");
                    //
                    setTimeout(function () { window.location.href = "design-online-step2.php"; }, 2000);
                } else {
                    alertify.error(json.msg);
                    //
                    switch (json.code) {
                        case 1:
                            $("#desemail").focus();
                            break;
                        case 2:
                            $("#destelefone").focus();
                            break;

                    }
                    //
                    $("#addOrcamento").html("Receba seu Orçamento");
                    $("#addOrcamento").removeAttr("disabled");
                }
            }
        });
    });

    $(".box-ambientes table a").on("click", function () {
        var id = $(this).parent().parent().attr("id");
        //
        $("#detalhes-" + id).toggle();
        //
        return false;
    });

    $('.box-img').hover(function () {
        $(this).children('.close').show();
    }, function () {
        $(this).children('.close').hide();
    });

    $("#form-endereco input[type=checkbox]").on("change", function (a, b, c) {
        $("#endereco-entrega").toggle();
        if ($(this).prop('checked') == true) {
            limpa_formulario_cep_entrege();
        }
    });

    var img = 0;

    function removeAcento(strToReplace) {
        str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
        str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
        var nova = "";
        for (var i = 0; i < strToReplace.length; i++) {
            if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
                nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
            } else {
                nova += strToReplace.substr(i, 1);
            }
        }
        return nova;
    }


    $('.fileupload').fileupload({
        url: "res/action/upload.php",
        dataType: 'json',
        done: function (e, data) {
            var json = $.parseJSON(data.jqXHR.responseText);
            var id = $(this).attr("id").replace("fileupload-", "");
            //
            $.ajax({
                type: "POST",
                url: "res/action/editArquivo.php",
                data: {
                    idambiente: id,
                    idarquivo: json.idarquivo
                }
            });
            //
            if (json.success) {
                $.each(data.files, function (index, file) {
                    img++;
                    $("#detalhes-ambiente-"+id+" .ambiente-fotos").append(function () {
                        return $('<div id="img-'+img+'" class="col-sm-3 box-img" idarquivo='+json.idarquivo+' pedido='+json.idpedido+' file="'+removeAcento(file.name)+'">' +
                            '<img src="res/img/pedido/' + json.idpedido + '/'+removeAcento(file.name)+'" class="img" />' +
                            '<img class="close" src="res/img/close.png" />' +
                        '</div>').click(removeImage);
                    });
                });
            } else {
                alertify.error("Esta extensão de arquivo não é valida. Por favor, inserir arquivos com extensão .JPG, .PNG ou .GIF");
            }
            
        }
    });

    var removeImage = function () {                
        //
        var idpedido = $(this).attr("pedido");
        var file = $(this).attr("file");
        var idarquivo = $(this).attr("idarquivo");
        //
        $.ajax({
            type: "POST",
            url: "res/action/removeImage.php",
            data: {
                idpedido: idpedido,
                file: file,
                idarquivo: idarquivo,
            }
        });
        //
        $(this).remove();
    }

    function validafield($campo) {
        if ($campo.val() == "") {
            return false;
        }
    }

    $("#addPagamento").on("click", function () {

        if ($("#inaceite").prop("checked") == false) {
            alertify.error("Falta marcar o aceite do contrato!");
            return false;
        }
        //

        var idpessoa = $("#idpessoa").val();
        var desemail = $("#desemail").val();
        var destelefone = $("#destelefone").val();
        var desrg = $("#desrg").val();
        var descpf = $("#descpf").val();
        //
        var descep = $("#descep").val();
        var desendereco = $("#desendereco").val();
        var desnumero = $("#nrnumero").val();
        var descomplemento = $("#descomplemento").val();
        var desbairro = $("#desbairro").val();
        var descidade = $("#descidade").val();
        var desestado = $("#desestado").val();
        //
        var inendereco_entrega = $("#inenderecoentrega").prop("checked");
        var descep_entrega = $("#descep_entrega").val();
        var desendereco_entrega = $("#desendereco_entrega").val();
        var desnumero_entrega = $("#nrnumero_entrega").val();
        var descomplemento_entrega = $("#descomplemento_entrega").val();
        var desbairro_entrega = $("#desbairro_entrega").val();
        var descidade_entrega = $("#descidade_entrega").val();
        var desestado_entrega = $("#desestado_entrega").val();
        //
        var formapagamento = $("input[name=formaPagamento]:checked").val();
        //
        var detalhes = [];
        var valid = 0;
        $(".box-ambientes .detalhes").each(function (a, b) {
            if ($(b).find("textarea").val() == "") {
                alertify.error("Digite as observações do ambiente <Strong>" + $(b).attr("name") + "</strong>. Este campo é obrigatório ser preenchido.");
                valid = 1;
                return false;
            } else {
                detalhes.push({
                    idambiente: $(b).attr("id").replace("detalhes-ambiente-", ""),
                    desobservacao: $(b).find("textarea").val()
                })
            }            
        });
        if (valid == 1) return false;
        //
        $("#addPagamento").attr("disabled","disabled");
        $("#addPagamento").html("Salvando...");
        //
        $.ajax({
            type: "POST",
            url: "res/action/addStep2.php",
            data: {
                idpessoa: idpessoa,
                desemail: desemail,
                destelefone: destelefone,
                desrg: desrg,
                descpf: descpf,
                descep: descep,
                desendereco: desendereco,
                desnumero: desnumero,
                descomplemento: descomplemento,
                desbairro: desbairro,
                descidade: descidade,
                desestado: desestado,
                inendereco_entrega: inendereco_entrega,
                descep_entrega: descep_entrega,
                desendereco_entrega: desendereco_entrega,
                desnumero_entrega: desnumero_entrega,
                descomplemento_entrega: descomplemento_entrega,
                desbairro_entrega: desbairro_entrega,
                descidade_entrega: descidade_entrega,
                desestado_entrega: desestado_entrega,
                detalhes: detalhes,
                formapagamento: formapagamento
            },
            success: function (a) {
                var json = $.parseJSON(a);
                if (json.success) {
                    //falta programar integração com meios de pagamento
                    if (formapagamento == 1) {
                        window.location.href = "pagamento/pagseguro.php"
                    } else {
                        window.location.href = "pagamento/paypal.php"
                    }
                    
                } else {
                    //
                    $("#addPagamento").html("Efetuar Pagamento");
                    $("#addPagamento").removeAttr("disabled");
                    //
                    alertify.error(json.msg);
                    //
                    $("#desemail").removeClass("error-input");
                    $("#destelefone").removeClass("error-input");
                    $("#desrg").removeClass("error-input");
                    $("#descpf").removeClass("error-input");
                    $("#descep").removeClass("error-input");
                    $("#desendereco").removeClass("error-input");
                    $("#nrnumero").removeClass("error-input");
                    $("#desbairro").removeClass("error-input");
                    $("#descidade").removeClass("error-input");
                    $("#desestado").removeClass("error-input");
                    $("#descep_entrega").removeClass("error-input");
                    $("#desendereco_entrega").removeClass("error-input");
                    $("#desbairro_entrega").removeClass("error-input");
                    $("#descidade_entrega").removeClass("error-input");
                    $("#desestado_entrega").removeClass("error-input");
                    $("#nrnumero_entrega").removeClass("error-input");
                    $(".radio").removeClass("error-input");
                    //
                    switch (json.code) {
                        case 1:
                            $("#desemail").addClass("error-input");
                            $("#desemail").focus();
                            break;
                        case 2:
                            $("#destelefone").addClass("error-input");
                            $("#destelefone").focus();
                            break;
                        case 3:
                            $("#desrg").addClass("error-input");
                            $("#desrg").focus();
                            break;
                        case 4:
                            $("#descpf").addClass("error-input");
                            $("#descpf").focus();
                            break;
                        case 5:
                            $("#descep").addClass("error-input");
                            $("#descep").focus();
                            break;
                        case 6:
                            $("#desendereco").addClass("error-input");
                            $("#desendereco").focus();
                            break;
                        case 7:
                            $("#desbairro").addClass("error-input");
                            $("#desbairro").focus();
                            break;
                        case 8:
                            $("#descidade").addClass("error-input");
                            $("#descidade").focus();
                            break;
                        case 9:
                            $("#desestado").addClass("error-input");
                            $("#desestado").focus();
                            break;
                        case 10:
                            $("#descep_entrega").addClass("error-input");
                            $("#descep_entrega").focus();
                            break;
                        case 11:
                            $("#desendereco_entrega").addClass("error-input");
                            $("#desendereco_entrega").focus();
                            break;
                        case 12:
                            $("#desbairro_entrega").addClass("error-input");
                            $("#desbairro_entrega").focus();
                            break;
                        case 13:
                            $("#descidade_entrega").addClass("error-input");
                            $("#descidade_entrega").focus();
                            break;
                        case 14:
                            $("#desestado_entrega").addClass("error-input");
                            $("#desestado_entrega").focus();
                            break;
                        case 15:
                            $(".radio").addClass("error-input");
                            break;
                        case 16:
                            $("#nrnumero").addClass("error-input");
                            $("#nrnumero").focus()
                            break;
                        case 17:
                            $("#nrnumero_entrega").addClass("error-input");
                            $("#nrnumero_entrega").focus();
                            break;

                    }
                }
            }
        });
    });

    $(".box-img .close").on("click", function () {
        var idpedido = $(this).parent().attr("pedido");
        var file = $(this).parent().attr("file");
        var idarquivo = $(this).parent().attr("idarquivo");
        //
        $.ajax({
            type: "POST",
            url: "res/action/removeImage.php",
            data: {
                idpedido: idpedido,
                file: file,
                idarquivo: idarquivo,
            }
        });
        //
        $(this).parent().remove();
    });

    if( $('.listaDepoimentos').length ) {
        var height = 0;

        $('.listaDepoimentos li').each(function(index, element){
            if ( height < $(element).height() ) {
                height =  $(element).height();
            }
        });

        $('.listaDepoimentos li').height(height);
    }

    $('#moreInfo').on('show.bs.modal', function (event) {
        var a = $(event.relatedTarget) // Button that triggered the modal

        $('.modal-body').html(a.attr("data-info"));
    })

});
jQuery(document).ready(function($) {
	function pinterest () {
		(function(d){
			var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
			p.type = 'text/javascript';
			p.async = true;
			p.src = '//assets.pinterest.com/js/pinit_main.js';
			f.parentNode.insertBefore(p, f);
		}(document, 'script'));
	}

	$('div.boxRedesContato.boxPinterest').on('click', function(){
		pinterest();
	});

	$( window ).resize(function() {
	  	$('.border-center').height($('.container-left').height() - 20);

	  	if ( $( this ).width() > 767 && $( this ).scrollTop() > 40 ) { 
	  		$('.header').addClass("menu-fixo");
			$('.header-container .logo .img-logo').addClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').addClass('menu-fixo-margin');
	  	}
	  	else {
			$('.header').removeClass("menu-fixo");
			$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
		}
	});	

	$( window ).scroll(function () { 
		if ( $( window ).width() > 767 ) {
			if ( $( this ).scrollTop() > 40 ) { 
				$('.container-main-page').addClass('margin-view-body');
				$('.header').addClass("fadeInDown menu-fixo"); 
				$('.header-container .logo .img-logo').addClass('menu-fixo-logo-small');
				$('.header .header-container .main-menu').addClass('menu-fixo-margin');
			} 
			else { 
				$('.container-main-page').removeClass('margin-view-body');
				$('.header').removeClass("fadeInDown menu-fixo"); 
				$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
				$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
			}					
		}
		else {
			$('.container-main-page').removeClass('margin-view-body');
			$('.header').removeClass("menu-fixo");
			$('.header-container .logo .img-logo').removeClass('menu-fixo-logo-small');
			$('.header .header-container .main-menu').removeClass('menu-fixo-margin');
		}
	});

	$('#accordion .panel-default .panel-heading > a').on('click', function(){
		if( $(this).hasClass('collapsed') ) {
			$('i.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
			$(this).find('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');			
		}
		else {
			$(this).find('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
		}
	});

	$('.container-left .grupo-galeria').on('click', function() {

		switch(parseInt($(this).attr('data-grupo-img'))){
			case 1:
				var fotos = Array('1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg', '9.jpg', '10.jpg');
			break;
			case 2:
				var fotos = Array('11.jpg', '12.jpg', '13.jpg', '14.jpg', '15.jpg', '16.jpg', '17.jpg', '18.jpg', '19.jpg', '20.jpg');
			break;
			case 3:
				var fotos = Array('21.jpg', '22.jpg', '23.jpg', '24.jpg', '25.jpg', '26.jpg', '27.jpg', '28.jpg', '29.jpg', '30.jpg');
			break;
			case 4:
				var fotos = Array('31.jpg', '32.jpg', '33.jpg', '34.jpg', '35.jpg', '36.jpg', '37.jpg', '38.jpg', '39.jpg', '40.jpg');
			break;
			case 5:
				var fotos = Array('1.jpg', '12.jpg', '3.jpg', '14.jpg', '5.jpg', '16.jpg', '7.jpg', '18.jpg', '9.jpg', '20.jpg');
			break;
			case 6:
				var fotos = Array('21.jpg', '32.jpg', '23.jpg', '34.jpg', '25.jpg', '36.jpg', '27.jpg', '38.jpg', '29.jpg', '40.jpg');
			break;
		}

		var html = '';
		/*$.each(fotos, function(index, el) {
			html += '<a href="res/img/galeria/"'+el+'><img src="" width="144" height="96"></a>';
		});

		$('.fotorama-wrap .fotorama').append(html);*/
		
		$('.fotorama-wrap').show();
	});

	$('.container-left .fotorama-wrap .btn-close-slideshow').on('click', function() {
		$('.fotorama-wrap').hide();
	});

	$.fn.albumPreviews = function() {
	
		return this.each(function(){

			var img = $(this).find('img');

			switch(parseInt($(this).attr('data-grupo-img'))){
				case 1:
					var fotos = Array('1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg', '9.jpg', '10.jpg');
				break;
				case 2:
					var fotos = Array('11.jpg', '12.jpg', '13.jpg', '14.jpg', '15.jpg', '16.jpg', '17.jpg', '18.jpg', '19.jpg', '20.jpg');
				break;
				case 3:
					var fotos = Array('21.jpg', '22.jpg', '23.jpg', '24.jpg', '25.jpg', '26.jpg', '27.jpg', '28.jpg', '29.jpg', '30.jpg');
				break;
				case 4:
					var fotos = Array('31.jpg', '32.jpg', '33.jpg', '34.jpg', '35.jpg', '36.jpg', '37.jpg', '38.jpg', '39.jpg', '40.jpg');
				break;
				case 5:
					var fotos = Array('1.jpg', '12.jpg', '3.jpg', '14.jpg', '5.jpg', '16.jpg', '7.jpg', '18.jpg', '9.jpg', '20.jpg');
				break;
				case 6:
					var fotos = Array('21.jpg', '32.jpg', '23.jpg', '34.jpg', '25.jpg', '36.jpg', '27.jpg', '38.jpg', '29.jpg', '40.jpg');
				break;
			}
			
			var album = $(this).find('div.album'),
				loop = null, images = $();			
			
			album.on('mouseenter', function(){

				if(!images.length){
					// The images have not been loaded yet
					
					$.each(fotos,function(){
						images = images.add('<img style="opacity:0;" src="res/img/galeria/' + this + '" />');
					});;

					// Start the animation after the first photo is loaded
					images.first().load(function() {
						album.trigger('startAnimation');
					});
					
					album
						.append(images)
						.addClass('loading');
				}
				else{
					// Start the animation directly
					album.trigger('startAnimation');
				}

				
			}).on('mouseleave', function(){
				album.trigger('stopAnimation');
			});
			
			
			// Custom events:
			
			album.on('startAnimation',function(){
				
				var iteration = 0;
				
				// Start looping through the photos
				(function animator(){
					
					album.removeClass('loading');

					// Hide the currently visible photo,
					// and show the next one:
					
					album.find('img').filter(function(){
						return ($(this).css('opacity') == 1);
					}).animate({
						'opacity' : 0
					}).nextFirst('img').animate({
						'opacity' : 1
					});

					loop = setTimeout(animator, 1000);	// Once per second

				})();
				
			});
			
			album.on('stopAnimation',function(){
				
				album.removeClass('loading');
				// stop the animation
				clearTimeout(loop);
			});
			
		});

	};
	
	// This jQuery method will return the next
	// element of the specified type, or the
	// first one if it doesn't exist
	
	$.fn.nextFirst = function(e) {
		var next = this.nextAll(e).first(); 
		return (next.length) ? next : this.prevAll(e).last();
	};

	$('.grupo-galeria').albumPreviews();

	//Mosta o Slicknav
	$('.main-menu ul').slicknav({
		prependTo: '#slicknav',
		label: ''
	}).slicknav('open');
});
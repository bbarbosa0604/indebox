<?php
require_once("../../inc/configuration.php");

try{

    if(!preg_match("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+.([a-zA-Z]{2,4})$^",post("desemail"))){
        throw new Exception("Email Inv�lido! Digite um email v�lido!", 1);
    }

    if(!preg_match('/^(\(0?\d{2}\)\s?|0?\d{2}[\s.-]?)\d{4,5}[\s.-]?\d{4}$/', post("destelefone"))){
        throw new Exception("Telefone Inv�lido! Digite um telefone v�lido!", 2);
    }

	$pessoa = new Pessoa(array(
    	"despessoa"=>utf8_encode(post("desnome")),
    	"desemail"=>post("desemail"),
        "descidade"=>utf8_encode(post("descidade")),
    	"desestado"=>post("desestado"),
    	"contato"=>array(
    		array(
    			"descontato"=>post("destelefone"),
    			"idcontatotipo"=>1
    		),
    		array(
    			"descontato"=>post("desemail"),
    			"idcontatotipo"=>3
    		)
    	)
    ));

    if(!$pessoa->getByEmail()){
    	$pessoa->save();
    	$pessoa->saveContatos();
    } else {
        $pessoa->edit();
    }

	$pedido = new Pedido(array(
		"idpessoa"=>$pessoa->getidpessoa(),
		"idplano"=>post("idplano"),
		"ambiente"=>post("ambientes")        
	));

    $amb = 0;

    $plano = $pedido->getPlano();

    foreach(post("ambientes") as $val){
        $m = (int)str_replace(",", ".", $val["desmetragem"]);
        if($m <= 12.99){
            $amb++;        
        } else {
            $temp = $m;
            $amb++;
            while($temp > 12.99){
                $temp = $temp - 12.99;
                $amb++;
            }
        }
    }
    $vlpedido = $plano["vlplano"] * $amb;
    $vldesconto = 0;
    //
    //Calculando desconto
    if($amb == 1){
        $vldesconto = 0.00;
    } else if($amb == 2){
        $vldesconto = 0.05;
    } else if($amb == 3){
        $vldesconto = 0.10;
    } else {
        $vldesconto = 0.15;
    }
    //
    $vlpedido = $vlpedido - ($vlpedido * $vldesconto);
    //
    $pedido->setvlpedido($vlpedido);



    $pedido->save();
	$pedido->saveAmbientes();
    //
    $_SESSION["idpedido"] = $pedido->getidpedido();
    //
    send_email(array(
        "to"=>post("desemail"),
        "subject"=>utf8_encode("Or�amento n� ".$_SESSION["idpedido"]."/".date("Y"))
    ), "mascara", array(
        "idpedido"=>$pedido->getidpedido(),
        "year"=>date("Y"),
        "desplano"=>utf8_encode($plano["desplano"]),
        "vlpedido"=>number_format($pedido->getvlpedido(),2,",","."),
        "idpessoa"=>$pessoa->getidpessoa(),
        "despessoa"=>utf8_encode(post("desnome")),
        "desemail"=>post("desemail"),
        "descidade"=>utf8_encode(post("descidade")),
    	"desestado"=>post("desestado"),
        "destelefone"=>post("destelefone"),
        "dtcadastro"=>date("Y-m-d H:i"),
        "ambientes"=>$pedido->getAmbientes(),
        "ip"=>$_SERVER["REMOTE_ADDR"]
    ));
    //
	echo json_encode(array("success"=>true));

} catch(Exception $e){
	echo json_encode(array("success"=>false, "msg"=>utf8_encode($e->getMessage()), "code"=>$e->getCode()));	
}

?>
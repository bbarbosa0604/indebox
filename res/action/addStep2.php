<?php
require_once("../../inc/configuration.php");

try{

    $idpessoa = utf8_encode(post("idpessoa"));
    $desemail = utf8_encode(post("desemail"));
    $destelefone = utf8_encode(post("destelefone"));
    $desrg = utf8_encode(post("desrg"));
    $descpf = utf8_encode(post("descpf"));
    $descep = utf8_encode(post("descep"));
    $desendereco = utf8_encode(post("desendereco"));
    $desnumero = utf8_encode(post("desnumero"));
    $descomplemento = utf8_encode(post("descomplemento"));
    $desbairro = utf8_encode(post("desbairro"));
    $descidade = utf8_encode(post("descidade"));
    $desestado = utf8_encode(post("desestado"));
    $descep_entrega = utf8_encode(post("descep_entrega"));
    $desendereco_entrega = utf8_encode(post("desendereco_entrega"));
    $desnumero_entrega = utf8_encode(post("desnumero_entrega"));
    $descomplemento_entrega = utf8_encode(post("descomplemento_entrega"));
    $desbairro_entrega = utf8_encode(post("desbairro_entrega"));
    $descidade_entrega = utf8_encode(post("descidade_entrega"));
    $desestado_entrega = utf8_encode(post("desestado_entrega"));
    $detalhes = post("detalhes");
    $formapagamento = utf8_encode(post("formapagamento"));
	


    //Valida��es
    if($desemail == ""){
        throw new Exception("Email inv�lido! Por favor, digite um email v�lido", 1);
    }
    if($destelefone == ""){
        throw new Exception("Telefone inv�lido! Por favor, digite um telefone v�lido", 2);
    }
    
    if(!preg_match("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+.([a-zA-Z]{2,4})$^",$desemail)){
        throw new Exception("Email Inv�lido! Digite um email v�lido!", 1);
    }

    if(!preg_match('/^(\(0?\d{2}\)\s?|0?\d{2}[\s.-]?)\d{4,5}[\s.-]?\d{4}$/', $destelefone)){
        throw new Exception("Telefone Inv�lido! Digite um telefone v�lido!", 2);
    }

    if($desrg == ""){
        throw new Exception("RG inv�lido! Por favor, digite um RG v�lido", 3);
    }

    if($descpf == "" || validaCPF($descpf) == false){
        throw new Exception("CPF inv�lido! Por favor, digite um CPF v�lido", 4);
    }

    if($descep == ""){
        throw new Exception("CEP inv�lido! Por favor, digite um CEP v�lido", 5);
    }
    if($desendereco == ""){
        throw new Exception("Endere�o inv�lido! Por favor, digite um endere�o v�lido", 6);
    }
    if($desnumero == ""){
        throw new Exception("Digite um n�mero!", 16);
    }
    if($desbairro == ""){
        throw new Exception("Bairro inv�lido! Por favor, digite um bairro v�lido", 7);
    }
    if($descidade == ""){
        throw new Exception("Cidade inv�lido! Por favor, digite um cidade v�lido", 8);
    }
    if($desestado == ""){
        throw new Exception("Estado inv�lido! Por favor, digite um estado v�lido", 9);
    }
    if($formapagamento == ""){
        throw new Exception("Escolha uma forma de pagamento", 15);   
    }

    $pessoa = new Pessoa(array(
        "idpessoa"=>$idpessoa,
        "descep"=>$descep,
        "desendereco"=>$desendereco,
        "desnumero"=>$desnumero,
        "descomplemento"=>$descomplemento,
        "desbairro"=>$desbairro,
        "descidade"=>$descidade,
        "desestado"=>$desestado,
        "contato"=>array(
            array(
                "descontato"=>$destelefone,
                "idcontatotipo"=>1
            ),
            array(
                "descontato"=>$desemail,
                "idcontatotipo"=>3
            )
        ),
        "documento"=>array(
            array(
                "desdocumento"=>$descpf,
                "iddocumentotipo"=>1
            ),
            array(
                "desdocumento"=>$desrg,
                "iddocumentotipo"=>2
            )
        )
    ));
    $pessoa->saveDocumentos();
    $pessoa->saveContatos();
    //
    $pessoa->saveEndereco();
    //
    if(post("inendereco_entrega") == false){
        if($descep_entrega == ""){
            throw new Exception("CEP inv�lido! Por favor, digite um CEP v�lido", 10);
        }
        if($desendereco_entrega == ""){
            throw new Exception("Endere�o inv�lido! Por favor, digite um endere�o v�lido", 11);
        }
        if($desbairro_entrega == ""){
            throw new Exception("Bairro inv�lido! Por favor, digite um bairro v�lido", 12);
        }
        if($descidade_entrega == ""){
            throw new Exception("Cidade inv�lido! Por favor, digite um cidade v�lido", 13);
        }
        if($desestado_entrega == ""){
            throw new Exception("Estado inv�lido! Por favor, digite um estado v�lido", 14);
        }
        if($desnumero_entrega == ""){
            throw new Exception("Digite um n�mero!", 17);
        }

        $pessoa->setdescep($descep_entrega);
        $pessoa->setdesendereco($desendereco_entrega);
        $pessoa->setdesnumero($desnumero_entrega);
        $pessoa->setdescomplemento($descomplemento_entrega);
        $pessoa->setdesbairro($desbairro_entrega);
        $pessoa->setdescidade($descidade_entrega);
        $pessoa->setdesestado($desestado_entrega);
        $pessoa->saveEndereco();
    }
    $pedido = new Pedido((int)$_SESSION["idpedido"]);
    $pedido->setidendereco($pessoa->getidendereco());
    $pedido->editPedido();
    //
    foreach($detalhes as $val){
        $p = new Pedido(array(
            "idambiente"=>$val["idambiente"],
            "desobservacao"=>utf8_encode($val["desobservacao"])
        ));
        $p->editPedidoAmbiente();
    }
    //
    $pagamento = new Pagamento(array(
        "idpedido"=>$_SESSION["idpedido"],
        "idformapagamento"=>$formapagamento,
        "idstatus"=>2
    ));
    $pagamento->save();
    //
    $_SESSION["idpagamento"] = $pagamento->getidpagamento();
    //
	echo json_encode(array("success"=>true));

} catch(Exception $e){
    echo json_encode(array("success"=>false, "msg"=>utf8_encode($e->getmessage()), "code"=>$e->getcode()));	
}

?>
<?php

require_once("../../inc/configuration.php");
require_once("../../inc/class/PagSeguroLibrary/PagSeguroLibrary.php");

$paymentrequest = new PagSeguroPaymentRequest();

$itens = array(
    Array(
        'id' => '01', // identificador
        'description' => 'Ambiente indebox', // descrição
        'quantity' => 1, // quantidade
        'amount' => 1000.00, // valor unitário
        'weight' => 0 // peso em gramas
    ),
    Array(
        'id' => '02', // identificador
        'description' => 'Ambiente indebox', // descrição
        'quantity' => 1, // quantidade
        'amount' => 1200.00, // valor unitário
        'weight' => 0 // peso em gramas
    )
);
foreach($itens as $data){
    $item = new PagSeguroItem($data);
    /* $paymentRequest deve ser um objeto do tipo PagSeguroPaymentRequest */

    $paymentrequest->addItem($item);
}
//Definindo moeda
$paymentrequest->setCurrency('BRL');

// 1- PAC(Encomenda Normal)
// 2-SEDEX
// 3-NOT_SPECIFIED(Não especificar tipo de frete)
$paymentrequest->setShipping(3);
//Url de redirecionamento
//$paymentrequest->setRedirectURL($redirectURL);// Url de retorno

$credentials = PagSeguroConfig::getAccountCredentials();//credenciais do vendedor

//$compra_id = App_Lib_Compras::insert($produto);
//$paymentrequest->setReference($compra_id);//Referencia;

$url = $paymentrequest->register($credentials);

header("Location: $url");
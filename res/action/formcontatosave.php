<?php
require_once("../../inc/configuration.php");
   
try
{
    $despessoa = post('nome');
    $desemail = post('email');
    $descontato = post('telefone');
    $desmensagem = post('mensagem');
    $desassunto = post('assunto');

    if(!preg_match("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+.([a-zA-Z]{2,4})$^",post("email"))){
        throw new Exception("Email Inv�lido! Digite um email v�lido!", 1);
    }

    if(!preg_match('/^(\(0?\d{2}\)\s?|0?\d{2}[\s.-]?)\d{4,5}[\s.-]?\d{4}$/', post("telefone"))){
        throw new Exception("Telefone Inv�lido! Digite um telefone v�lido!", 2);
    }

    $sql = new Sql();
    $sql->insert("insert tb_faleconosco(desnome,desemail,destelefone,desassunto,desmensagem,dtcadastro) values(?,?,?,?,?,?)",array(
        utf8_decode($despessoa),
        ($desemail),
        utf8_decode($descontato),
        utf8_decode($desassunto),
        utf8_decode($desmensagem),
        date("Y-m-d H:i")
    ));

    $sendgrid = new SendGrid("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");

    $contrato = '<table>

    <tr>

        <td>Nome:</td>

        <td>'.utf8_decode($despessoa).'</td>

    </tr>

    <tr>

        <td>Email:</td>

        <td>'.$desemail.'</td>

    </tr>

    <tr>

        <td>Telefone:</td>

        <td>'.($descontato).'</td>

    </tr>

    <tr>

        <td>Assunto:</td>

        <td>'.$desassunto.'</td>

    </tr>

    <tr>

        <td>Mensagem:</td>

        <td>'.utf8_decode($desmensagem).'</td>

    </tr>

</table>';

    $email = new SendGrid\Email();
    $email
        ->addTo('design@indebox.com.br')
        ->addTo('bruno@fastcode.com.br')    
        ->setFrom('design@indebox.com.br')
        ->setFromName("Indebox Web Site")
        ->setSubject('[IndeBox] Fale Conosco Site')    
        ->setHtml($contrato)
    ;

    $sendgrid->send($email);

    // $idsitecontato = $Contato->saveSiteContato($idpessoa['idpessoa'], 0, $desmensagem, $desassunto, 1, 0, 1);

    echo json_encode(array(
        "success" => true,
        "msg" => "Contato cadastrado com sucesso"
    ));
}
catch (Exception $e)
{
    echo json_encode(array("success"=>false, "msg"=>utf8_encode($e->getMessage()), "code"=>$e->getCode()));	
}
    

    

?>
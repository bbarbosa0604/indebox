<?php
require_once("../../inc/configuration.php");

try{
    $idarquivo = post("idarquivo");
    $idambiente = post("idambiente");
    //
    $pedido = new Pedido(array(
       "idarquivo"=>$idarquivo,
       "idambiente"=>$idambiente
    ));
    $pedido->editArquivo();
    //
    echo json_encode(array("success"=>true));
}
catch(Exception $e){
	echo json_encode(array("success"=>false, "msg"=>$e->getMessage()));	
}

?>
<?php
require_once("../../inc/configuration.php");

$idpedido = post("idpedido");
$file = post("file");
$idarquivo = post("idarquivo");
$uploaddir = "../img/pedido/".$idpedido."/";

$pedido = new Pedido(array(
    "idarquivo"=>$idarquivo
));
$pedido->removeArquivo();

unlink($uploaddir.$file);

echo json_encode(array("success"=>true));

?>
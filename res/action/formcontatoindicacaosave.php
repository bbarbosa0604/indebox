<?php

require_once("../../inc/configuration.php");

$despessoa = post('nome');
$desemail = post('email');
$despessoaamigo = post('nomeamigo');
$desemailamigo = post('emailamigo');
$desmensagem = post('mensagem');

$sql = new Sql();
$sql->insert("insert tb_indicacao(desnome,desemail,desnomeamigo,desemailamigo,desmensagem,dtcadastro) values(?,?,?,?,?,?)",array(
    utf8_decode($despessoa),
    ($desemail),
    utf8_decode($despessoaamigo),
    ($desemailamigo),
    utf8_decode($desmensagem),
    date("Y-m-d H:i")
));

send_email(array(
    "to"=>$desemailamigo,
    "subject"=>"Ola ".utf8_decode($despessoaamigo).", o seu amigo ".utf8_decode($despessoa)." gostaria de indicar a Indebox!"
), "indicacao", array(
    "nome"=>utf8_decode($despessoa),
    "email"=>utf8_decode($desemail),
    "mensagem"=>utf8_decode($desmensagem)
));

echo json_encode(array(
    "success" => true,
    "msg" => "Contato cadastrado com sucesso"
));

?>
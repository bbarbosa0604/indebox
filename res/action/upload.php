<?php
require_once("../../inc/configuration.php");


if($_FILES['files']['type'][0] != "image/jpeg" && $_FILES['files']['type'][0] != "image/png" && $_FILES['files']['type'][0] != "image/gif"){
    echo json_encode(array("success"=>false));
    exit;
}

$idpedido = $_SESSION["idpedido"];
$uploaddir = "../img/pedido/".$idpedido."/";

if(!file_exists($uploaddir)){
    mkdir($uploaddir);
}

$uploadfile = $uploaddir.preg_replace( '/[`^�~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $_FILES['files']['name'][0]));
//
$pedido = new Pedido(array(
    "idpedido"=>$idpedido,
    "desarquivo"=>preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $_FILES['files']['name'][0]))
));
$pedido->saveArquivo();
//
if (move_uploaded_file($_FILES['files']['tmp_name'][0], $uploadfile)) {
    echo json_encode(array("success"=>true, "idpedido"=>$idpedido, "idarquivo"=>$pedido->getidarquivo()));
}

?>
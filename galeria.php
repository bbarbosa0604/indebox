<?php

require_once("inc/configuration.php");

$Sql = new Sql();

$categorias = $Sql->arrays("call Indebox.sp_categorias_list();");

$page = new Page(array(
    "css" => true,
    "data" => array(
        "head_title" => 'Indebox - Galeria',
        "categorias" => $categorias
    )
));

$page->setTpl("galeria");
